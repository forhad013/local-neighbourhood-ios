//
//  ProviderViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class ProviderViewController: UIViewController {

    @IBOutlet var provCollectionView: UICollectionView!
    @IBOutlet weak var providerLabel: UILabel!
    
    var passId : String = ""
    var dictArr : [NSDictionary] = []
    var addressthree : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        providerLabel.isHidden = true
        self.ShowProvider()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func ShowProvider(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            let pstring = "subcategory=\(passId)"
            
            ApiResponse.onResponsePostPhp(url: "Userajax/business", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String

                    if(status == true)
                    {
                        OperationQueue.main.addOperation {
                            
                            self.dictArr = result["data"] as! [NSDictionary]
                            
                            self.provCollectionView.reloadData()
                            
                            LoadingIndicatorView.hide()
                        }
                    }
                   else{
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            self.providerLabel.isHidden = false
                            self.provCollectionView.isHidden = true
                        }
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
}

extension ProviderViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dictArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : ProviderCollectionViewCell = provCollectionView.dequeueReusableCell(withReuseIdentifier: "providercell", for: indexPath) as! ProviderCollectionViewCell
        
        let dict1 = dictArr[indexPath.item]
        
        let addr = Int((dict1["address_status"] as? String)!)
        if (addressthree == addr) {
            
            cell.pAddress.isHidden = true
            cell.pAddressHt.constant = 0
        }
        else
        {
           cell.pAddress.text! = "Address  :\((dict1["address"] as? String)!)"
        }

        cell.pBusinessName.text = dict1["businessname"] as? String
        cell.pName.text = "Provider Name :\(dict1["ownername"] as! String)"
        cell.pEmail.text = "Email    :\(dict1["email"] as! String)"
        cell.pContact.text = "Contact  :\(dict1["phonenumber"] as! String)"
        cell.pWebsite.text = "Website  :\(dict1["weblink"] as! String)"
        cell.pImage.layer.cornerRadius = cell.pImage.frame.size.width/2
        cell.pImage.clipsToBounds = true
        var imageString = dict1["image"] as! String
        imageString = imageString.replacingOccurrences(of: " ", with: "%20")
        cell.pImage.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/\(imageString)"))
        ApiResponse.callRating(Double((dict1["average_rating"] as? String)!)!, starRating: cell.starCollection)
        
        return cell
    }
    
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
      {
        let dict2 = dictArr[indexPath.row]
        let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
        dictProvider.providerdetailDict = dict2
        
            print("dataDict1 ==***********************\(dict2)")
        
        self.present(dictProvider, animated: true, completion: nil)
    }
    
}

class ProviderCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var pImage: UIImageView!
    @IBOutlet weak var pBusinessName: UILabel!
    @IBOutlet weak var pName: UILabel!
    @IBOutlet weak var pEmail: UILabel!
    @IBOutlet weak var pContact: UILabel!     
    @IBOutlet weak var pAddress: UILabel!
    @IBOutlet weak var pWebsite: UILabel!
    @IBOutlet var starCollection: [UIImageView]!
    @IBOutlet weak var pAddressHt: NSLayoutConstraint!
    
}


 
