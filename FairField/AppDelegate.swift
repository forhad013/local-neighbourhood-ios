//
//  AppDelegate.swift
//  FairField
//
//  Created by Prashant Shinde on 6/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import UserNotifications
import FirebaseMessaging
import IQKeyboardManagerSwift
import MapKit
import AudioToolbox
import SwiftyJSON



var lat1 : CLLocationDegrees!
var lng1 : CLLocationDegrees!
let gcmMessageIDKey = "gcm.message_id"

var deligate : NormalPopUpDeligate?

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate    {
    
    var window: UIWindow?
    var user : UserDefaults!
    var storyboard : UIStoryboard!
    var subtitleString : String = ""
    var dataDict : NSDictionary = [:]
    var bodyMessage : String = ""
    var titleString : String = ""
    var locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        FirebaseApp.configure()
        
        
        
        
        application.registerForRemoteNotifications()
        
        
        
        
        
        
        /*   storyboard = UIStoryboard(name: "Main", bundle: nil)
         //  self.currentLocationOfUser()
         user = UserDefaults.standard
         let session = user.object(forKey: "session") as? String
         if (session == "") || (session == nil)
         {
         
         }
         else
         {
         let data1 = user.object(forKey: "userInfo") as! NSDictionary
         let pstring = "user_id=\(data1["user_id"] as! String)"
         self.locationService("Userajax/check_user", pstring: pstring, checkStr: "one")
         }
         
         IQKeyboardManager.shared.enable = true
         */
        
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            let center = UNUserNotificationCenter.current()
            // Request permission to display alerts and play sounds.
            center.requestAuthorization(options: [.alert, .sound])
            { (granted, error) in
                print("granted \(granted)")
                
                if(granted){
                    self.connectToFcm()
                }
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            self.tokenRefreshNotification()
        }
        
        
        
        application.registerForRemoteNotifications()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        
        
    }
    
    
    
    
    func checkNotificationPermission(){
        
        var returnValue: Bool = UserDefaults.standard.bool(forKey: Constant.PREVIOUS_USER)
        
        print("previous user  \(returnValue)")
        
        if(!returnValue){
            let current = UNUserNotificationCenter.current()
            
            current.getNotificationSettings(completionHandler: { (settings) in
                
                print("status : \(settings.authorizationStatus)")
                if settings.authorizationStatus == .notDetermined {
                    
                    
                }
                
                if settings.authorizationStatus == .denied {
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_NEW)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_EVENT)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REWARD)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_BUSINESS)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_ANNOUNCMENT)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_REQUEST)
                    
                    
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_EVENT)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REWARD)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_BUSINESS)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_NEW)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
                    UserDefaults.standard.synchronize()
                }
                
                if settings.authorizationStatus == .authorized {
                    UserDefaults.standard.set(true, forKey: Constant.PREVIOUS_USER)
                    
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_NEW)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_EVENT)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REWARD)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_BUSINESS)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_ANNOUNCMENT)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_REQUEST)
                    
                    
                    
                    
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_EVENT)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REWARD)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_BUSINESS)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_NEW)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
                    UserDefaults.standard.synchronize()
                }
            })
        }
        
        
    }
    
    
    
    
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        
        
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID 1: \(messageID)")
        }
        
        // print("userinfo = \(userInfo)")
        
        
        // sendCustomerNoti(title: "csutome")
        
        UNUserNotificationCenter.current().delegate = self
    }
    
    
    
    func application(_ application: UIApplication, didRegister didRegisterNotificationSettings: UIUserNotificationSettings) {
        //
        print("register")
        //
    }
    
    
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        
        print("register messgae")
        Messaging.messaging().setAPNSToken(deviceToken, type: .sandbox)
        
        
    }
    
    func sendCustomerNoti(title : String){
        print(title)
        
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest(identifier: "TestIdentifier", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        AudioServicesPlaySystemSound(1315);
        
        
        
        
    }
    
    
    func tokenRefreshNotification()
    {
        
        if let refreshedToken = InstanceID.instanceID().token(){
            print("InstanceID token: \(refreshedToken)")
            // self.checkNotificationPermission()
        }
        
        
        
    }
    
    func connectToFcm()
    {
        Messaging.messaging()
        
        Messaging.messaging().delegate = self
        
        self.tokenRefreshNotification()
    }
    
    
    func convertToDictionary(_ text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    /// LOCATION SERVICE ///
    func locationService(_ urlStr : String, pstring : String, checkStr : String){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //  let pstring = "latitude=\(lat1)&longitude=\(lng1)"
            
            ApiResponse.onResponsePostPhp(url: urlStr, parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    _ = result["message"]as! String
                    if(status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                                if checkStr == "one"
                                {
                                    let homeViewController = self.storyboard.instantiateViewController(withIdentifier: "dashboard") as!
                                    DashboardViewController
                                    self.window!.rootViewController = homeViewController
                                    self.window?.makeKeyAndVisible()
                                }
                                else
                                {
                                    _ = result["data"] as! NSDictionary
                                }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation
                            {
                                self.user.set("", forKey: "session")
                                self.user.set(nil, forKey: "userInfo")
                                self.user.synchronize()
                                let loginway = self.user.value(forKey: "loginway") as! String
                                if (loginway == "facebook")
                                {
                                    FBSDKLoginManager().logOut()
                                    FBSDKAccessToken.setCurrent(nil)
                                }
                                let login = self.storyboard?.instantiateViewController(withIdentifier: "signin") as! SignInViewController
                                self.window!.rootViewController = login
                                self.window?.makeKeyAndVisible()
                        }
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        LoadingIndicatorView.hide()
                    }
                    else {
                        LoadingIndicatorView.hide()
                    }
                }
            })
        }
        else
        {
        }
    }
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        //   self.locationService()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        //   self.locationService()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.connectToFcm()
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // self.locationService()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}




extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        user = UserDefaults.standard
        print(userInfo)
        
        if let user_id_from_not = userInfo["user_id"] {
            
            let data1 = user.object(forKey: "userInfo") as! NSDictionary
            let  user_id = data1["user_id"] as! String
            
            let user_id_from_noti = userInfo["user_id"] as! String
            
            print(user_id)
            
            
            
            //let  user_id = "2417"
            
            
            if(user_id_from_noti == user_id){
                print("same")
            }else{
                completionHandler([.alert, .badge, .sound])
                AudioServicesPlaySystemSound(1315);
            }
            
        }else{
            completionHandler([.alert, .badge, .sound])
            AudioServicesPlaySystemSound(1315);
        }
        
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        let userInfo = response.notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID 2 : \(messageID)")
        }
        if let type1 = userInfo["type"] {
            
            // Print full message.
            print("clicked \(type1)")
            
            let type = type1 as! String
            let data = userInfo["data"] as! String
            
            let dataJson = convertToJson(stringdata: data)
            
            if (type == "new_business") {
                
                let owner_id = userInfo["owner_id"]
                
                let ownerData = dataJson.dictionaryObject!["owner"] as! NSDictionary
                
                businessPage(owner_data: ownerData)
                
            }
            else if (type == "review_request")
            {
                let ownerData = dataJson.dictionaryObject!["owner"] as! NSDictionary
                
                businessPage(owner_data: ownerData)
                
            }
            else if (type == "new_review") {
                let ownerData = dataJson.dictionaryObject!["owner"] as! NSDictionary
                
                businessPage(owner_data: ownerData)
                
            }
            else if (type == "new_reward")
            {
                openRewardPage()
            }
            else if (type == "new_event") {
                openNewEventPage()
            }
            else if (type == "new_announcement")
            {
                
                let title = userInfo["title"] as! String
                let body = userInfo["body"] as! String
                let image = userInfo["image"] as! String
                
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let dashboard = storyBoard.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
                dashboard.openPopUp(title: title,body: body,imagelink: image)
                
                self.window?.rootViewController = dashboard
                self.window?.makeKeyAndVisible()
                
                
            }
            else if (type == "new_coupon")
            {
                openCouponPage()
            }
            completionHandler()
        }
        
        
        
    }
    
    
    func convetToDictionary(stringData :  String) -> NSDictionary{
        
        var dictonary:NSDictionary?
        
        if let data12 = stringData.data(using: String.Encoding.utf8) {
            
            do {
                dictonary = try JSONSerialization.jsonObject(with: data12, options: []) as? [String:AnyObject] as! NSDictionary
                
                
                
            } catch let error as NSError {
                print(error)
            }
        }
        return dictonary!
    }
    
    
    func convertToJson(stringdata : String) -> JSON {
        
        
        
        if let jsonData = stringdata.data(using: .utf8) {
            if let json = try? JSON(data: jsonData) {
                return json
            }
        }
        return nil
    }
    
  
    
    
    func businessPage(owner_data : NSDictionary){
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let provider = storyBoard.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
        print("dataDict1 ==***********************\(owner_data)")
        provider.providerdetailDict = owner_data
        provider.strOne = "one"
        
        self.window?.rootViewController = provider
        self.window?.makeKeyAndVisible()
        
        
        
        
    }
    
    
    func stringToJson(notificationString : String){
        
        let data = notificationString.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
                
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    
    func openBusinessListing(){
        OperationQueue.main.addOperation{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let presentViewController = storyBoard.instantiateViewController(withIdentifier: "business") as! BusinessViewController
            self.window?.rootViewController = presentViewController
            self.window?.makeKeyAndVisible()
        }
        
        
        
        
    }
    
    func openRewardPage(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let provider = storyBoard.instantiateViewController(withIdentifier: "rewardsviewcontroller") as! RewardsViewController
        self.window?.rootViewController = provider
        self.window?.makeKeyAndVisible()
        
    }
    
    func openCouponPage(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let provider = storyBoard.instantiateViewController(withIdentifier: "promotion") as! PromotionViewController
        self.window?.rootViewController = provider
        self.window?.makeKeyAndVisible()
    }
    
    
    func openNewEventPage(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let local = storyBoard.instantiateViewController(withIdentifier: "localevents") as! LocalEventsVC
        self.window?.rootViewController = local
        self.window?.makeKeyAndVisible()
    }
    
    
    func openHome(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let dashboard = storyBoard.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
        self.window?.rootViewController = dashboard
        self.window?.makeKeyAndVisible()
    }
}
protocol NormalPopUpDeligate {
    
    func  openPopUp(title: String, body : String , imagelink : String)
}






