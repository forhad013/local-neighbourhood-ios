//
//  ViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import ReachabilitySwift
import FBSDKCoreKit
import FBSDKLoginKit

var reach : Reachability = Reachability()!




class ViewController: UIViewController {

    var window: UIWindow?
    var user : UserDefaults!
 
    var subtitleString : String = ""
    var dataDict : NSDictionary = [:]
    var bodyMessage : String = ""
    var titleString : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
       // storyboard = UIStoryboard(name: "Main", bundle: nil)
        //  self.currentLocationOfUser()
        user = UserDefaults.standard
        let session = user.object(forKey: "session") as? String
        if (session == "") || (session == nil)
        {
            
        }
        else
        {
            let data1 = user.object(forKey: "userInfo") as! NSDictionary
            let pstring = "user_id=\(data1["user_id"] as! String)"
            self.locationService("Userajax/check_user", pstring: pstring, checkStr: "one")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func howItWorks(_ sender: Any) {
    }

    /// LOCATION SERVICE ///
    func locationService(_ urlStr : String, pstring : String, checkStr : String){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //  let pstring = "latitude=\(lat1)&longitude=\(lng1)"
            
            ApiResponse.onResponsePostPhp(url: urlStr, parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    _ = result["message"]as! String
                    if(status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                                if checkStr == "one"
                                {
                       
                                    
                                    let business = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
                                    self.present(business, animated: true, completion: nil)
                                }
                                else
                                {
                                    _ = result["data"] as! NSDictionary
                                }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation
                            {
                                self.user.set("", forKey: "session")
                                self.user.set(nil, forKey: "userInfo")
                                self.user.synchronize()
                                let loginway = self.user.value(forKey: "loginway") as! String
                                if (loginway == "facebook")
                                {
                                    FBSDKLoginManager().logOut()
                                    FBSDKAccessToken.setCurrent(nil)
                                }
                                let login = self.storyboard?.instantiateViewController(withIdentifier: "signin") as! SignInViewController
                                self.window!.rootViewController = login
                                self.window?.makeKeyAndVisible()
                        }
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        LoadingIndicatorView.hide()
                    }
                    else {
                        LoadingIndicatorView.hide()
                    }
                }
            })
        }
        else
        {
        }
    }
    
    
    }


