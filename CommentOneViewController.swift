//
//  CommentOneViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 8/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class CommentOneViewController: UIViewController {
  
    @IBOutlet weak var commentoneTableView: UITableView!
   
    var commentOneArr : [NSDictionary] = []
    var eventid : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showComment()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func showComment(){
        let params = "event_id=\(self.eventid)"
        
        print("params = \(params)")
        
        ApiResponse.onResponsePostPhp(url: "Userajax/show_comments", parms: params, completion: { (result, error) in
            
            print("show comment = \(result)")
            if (error == "") {
                
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                print("result = \(result)")
                
                if (status == true) {
                    
                    OperationQueue.main.addOperation
                    {
                        LoadingIndicatorView.hide()
                        
                        //  ApiResponse.alert(title: "Done", message: message, controller: self)
                        self.commentOneArr  = result["data"] as! [NSDictionary]
                        
                        if(self.commentOneArr.count != 0) {
                            self.commentoneTableView.reloadData()
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
        
    }
    
    @IBAction func bactBtn1(_ sender: Any) {
          self.dismiss(animated: false, completion: nil)
    }
    
}

class CommentOneCell : UITableViewCell {
 
    @IBOutlet weak var cmtImgButton: UIButton!
    @IBOutlet weak var cmtName: UILabel!
    @IBOutlet weak var cmtDescription: UILabel!

}

extension CommentOneViewController : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentOneArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CommentOneCell = commentoneTableView.dequeueReusableCell(withIdentifier: "commentonecell")as! CommentOneCell
        let commDict = commentOneArr[indexPath.row]
        
        cell.cmtName.text!  = (commDict["user_name"] as? String)!
        
        cell.cmtDescription.text!  = (commDict["comment"] as? String)!
        
        //  self.imageDict = commDict["images"] as! [String]
       cell.layer.borderColor =  UIColor.init(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1.0).cgColor
        cell.layer.borderWidth = 4
        cell.clipsToBounds = true
        cell.cmtImgButton.addTarget(self, action: #selector(imagedetail(_:)), for: .touchUpInside)
        return cell
    }
    
     @objc func imagedetail(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:commentoneTableView)
        let indexPath1 = commentoneTableView.indexPathForRow(at: point)
        let dictNoti1 = self.commentOneArr[(indexPath1?.item)!]
        let showImage = self.storyboard?.instantiateViewController(withIdentifier: "imageviewcontroller") as! ImageViewController
        showImage.commentDict = dictNoti1
        showImage.imgStr = "commentStr1"
        self.present(showImage, animated: true, completion: nil)
    }
    
     
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        
        let vw = UIView.init(frame: CGRect(x : 0, y : 0, width : commentoneTableView.frame.width, height : 10))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
    
    
}
