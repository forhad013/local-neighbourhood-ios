//
//  RewardCommentVC.swift
//  Local Neighbourhood App
//
//  Created by KpStar on 5/28/18.
//  Copyright © 2018 Prashant Shinde. All rights reserved.
//

import UIKit
import SDWebImage

class RewardCommentVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var titleImg: UIImageView!
    @IBOutlet weak var descTxt: UITextView!
    @IBOutlet weak var enteredLbl: UILabel!
    @IBOutlet weak var showmoreBtn: UIButton!
    @IBOutlet weak var myCommentTxt: UITextView!
    @IBOutlet weak var submitBtn: KGHighLightedButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var galleryBtn: UIButton!
    @IBOutlet weak var myCommentImg: UIImageView!
    @IBOutlet weak var enterBtn: KGHighLightedButton!
    
    
    var imagePicker : UIImagePickerController? = UIImagePickerController()
    var reward_id = Int()
    var user_id = String()
    var myImage : UIImage?
    var sortedComments : [[String: Any]] = [[:]]
    var participants = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myCommentTxt.delegate = self
        enterBtn.isHidden = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.myCommentTxt.resignFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        return numberOfChars <= 1000;
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let user_detail = UserDefaults.standard.dictionary(forKey: "userInfo")
        user_id = user_detail!["user_id"] as! String
        
        view_reward()
    }
    
    func view_reward() {
        
        let params = "reward_id=\(String(reward_id))&user_id=\(user_id))"
        
        LoadingIndicatorView.show()
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            ApiResponse.onResponsePostEnteredPhp(url: "userajax/is_user_entered_reward", parms: params, completion: { (result, error) in
                if (error == "") {
                    
                    OperationQueue.main.addOperation
                        {
                            //LoadingIndicatorView.hide()
                            if result == "YES" {
                                self.enterBtn.isEnabled = false
                                self.enterBtn.alpha = 0.3
                                self.enterBtn.setTitle("Entered", for: .normal)
                            }
                            self.enterBtn.isHidden = false
                    }
                } else {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    self.enterBtn.isHidden = true
                }
            })
            
            ApiResponse.onResponsePostPhp(url: "userajax/view_reward", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            
                            self.participants = result["total_participants"] as? Int ?? 0
                            if self.participants < 2 {
                                self.enteredLbl.text = String(self.participants) + "  Entry"
                            } else {
                                self.enteredLbl.text = String(self.participants) + "  Entries"
                            }
                            let dict = result["reward"] as? [String : Any] ?? [:]
                            let imgUrl = dict["image"] as? String ?? ""
                            let can_comment = dict["can_comment"] as? String ?? "0"
                            let can_enter = dict["can_enter"] as? String ?? "0"
                            if can_comment == "1" {
                                self.myCommentTxt.isHidden = false
                                self.galleryBtn.isHidden = false
                                self.cameraBtn.isHidden = false
                                self.myCommentImg.isHidden = false
                                self.showmoreBtn.isHidden = false
                                self.submitBtn.isHidden = false
                            } else {
                                self.myCommentTxt.isHidden = true
                                self.galleryBtn.isHidden = true
                                self.cameraBtn.isHidden = true
                                self.myCommentImg.isHidden = true
                                self.submitBtn.isHidden = true
                                self.showmoreBtn.isHidden = true
                            }
                            
                            if can_enter == "1" {
                                self.enterBtn.isHidden = false
                                self.enteredLbl.isHidden = false
                            } else {
                                self.enterBtn.isHidden = true
                                self.enteredLbl.isHidden = true
                            }
                            if imgUrl != "" {
                                
                                self.titleImg.sd_setImage(with: URL(string : "\(Constant.IMAGE_URL_NEW)rewards/\(imgUrl)"), placeholderImage: UIImage(named: "logo"), options: .refreshCached, completed: nil)
                            }
                            self.titleLbl.text = dict["title"] as? String ?? ""
                            self.descTxt.text = dict["description"] as? String ?? ""
                            
                            let comments = result["comments"] as! [[String:Any]]
                            self.sortedComments = comments.sorted(by: { (a, b) -> Bool in
                                let first = a["created_at"] as! String
                                let second = b["created_at"] as! String
                                return first < second
                            })
                    }
                } else {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    self.enterBtn.isHidden = true
                }
            })
        }
    }
    
    @IBAction func enterBtnTapped(_ sender: Any) {
        
        let data = UserDefaults.standard.dictionary(forKey: "userInfo")
        enterBtn.isEnabled = false
        let user_id = data!["user_id"] as! String
        let reward_id = self.reward_id
        let paramsone = "reward_id=\(String(reward_id))&user_id=\(user_id)"
        ApiResponse.onResponsePostPhp(url: "userajax/add_reward_participant", parms: paramsone, completion: { (result, error) in
            
            if error == "" {
                OperationQueue.main.addOperation {
                    self.enterBtn.isEnabled = false
                    self.enterBtn.alpha = 0.3
                    self.participants += 1
                    self.enteredLbl.text = String(self.participants) + "  Entries"
                    self.enterBtn.setTitle("Entered", for: .normal)
                }
            }
        })
    }
    
    @IBAction func pictureBtnTapped(_ sender: UIButton) {
        
        imagePicker?.delegate = self
        if sender == cameraBtn {
            
            openCamera()
        } else {
            
            openGallery()
        }
    }
    
    public func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            imagePicker?.sourceType = UIImagePickerControllerSourceType.camera
            self.present(imagePicker!, animated: true, completion: nil)
        }
    }
    
    public func openGallery() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            imagePicker?.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(imagePicker!, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.myImage = image ?? nil
        let width = self.view.frame.width - 40
        let ratio = (image?.size.height)!/(image?.size.width)!
        image = image?.scaleImage(newSize: CGSize(width: width, height: width*ratio))
        picker.dismiss(animated: true, completion: nil)
        self.myCommentImg.image = image
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        
        if (sender as! UIButton) == showmoreBtn {
            
            let desVC = storyboard?.instantiateViewController(withIdentifier: "rewardstable") as! RewardTableViewController
            //            desVC.comments = self.sortedComments
            desVC.reward_id = self.reward_id
            self.present(desVC, animated: true, completion: nil)
        } else {
            LoadingIndicatorView.show()
            
            let user_detail = UserDefaults.standard.dictionary(forKey: "userInfo")
            let user_id = user_detail!["user_id"] as! String
            
            let params : [String : String] = [
                "reward_id" : String(self.reward_id),
                "user_id" : user_id,
                "comment" : self.myCommentTxt.text
            ]
            
            
            var data: NSData? = nil
            if let image = self.myImage {
                let width = self.view.frame.width - 40
                let ratio = (image.size.height)/(image.size.width)
                let mImage = image.scaleImage(newSize: CGSize(width: width, height: width*ratio))
                
                data = UIImageJPEGRepresentation(mImage, 1.0)! as NSData
            }
            
            if self.myCommentTxt.text == "" && data == nil {
                ApiResponse.alert(title: "", message: "You must submit a comment or a photo", controller: self)
                return
            }
            //
            //            if self.myCommentTxt.text == "" {
            //               self.myCommentTxt.text = " "
            //            }
            //
            if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
                ApiResponse.onResponsePostCommentPhp(url: "Userajax/add_reward_comment", parms: params, data: data as Data? ?? nil, completion: { (result, error) in
                    
                    if (error == "") {
                        
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                
                                if (result["status"] as? Int) != 1 {
                                    ApiResponse.alert(title: "Something went wrong", message: "Fill all entry", controller: self)
                                } else {
                                    self.myCommentImg.image = nil
                                    self.myCommentTxt.text = ""
                                    self.scrollView.contentSize.height = self.submitBtn.frame.origin.y + self.submitBtn.frame.height
                                }
                        }
                    } else {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                })
            }
        }
    }
    
    @IBAction func onBackPressed(_ sender: UIBarButtonItem) {
        
        let desVC = storyboard?.instantiateViewController(withIdentifier: "rewardsviewcontroller") as! RewardsViewController
        self.present(desVC, animated: true, completion: nil)
    }
}

extension UIImage {
    func scaleImage(newSize: CGSize) -> UIImage {
        //UIGraphicsBeginImageContext(newSize);
        // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
        // Pass 1.0 to force exact pixel size.
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage;
    }
    
    func scaleImage(newHeight: CGFloat) -> UIImage {
        let scaleFactor = newHeight / self.size.height
        let newSize = CGSize(width: size.width * scaleFactor, height: size.height * scaleFactor)
        return scaleImage(newSize: newSize)
    }
    
    func fixImageOrientation() -> UIImage {
        
        guard imageOrientation != .up else {
            return self
        }
        
        var transform = CGAffineTransform.identity
        
        switch imageOrientation {
            
        case .down, .downMirrored:
            
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            
            
        case .left, .leftMirrored:
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2)
            
        case .right, .rightMirrored:
            
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: -(CGFloat.pi / 2))
            
        default:
            break
        }
        
        switch imageOrientation {
            
        case .upMirrored, .downMirrored:
            
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            
        case .leftMirrored, .rightMirrored:
            
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
            
        default:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: self.cgImage!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
            
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
}

