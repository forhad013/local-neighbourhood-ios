//
//  RewardTableViewController.swift
//  Local Neighbourhood App
//
//  Created by KpStar on 5/28/18.
//  Copyright © 2018 Prashant Shinde. All rights reserved.
//

import UIKit
import SDWebImage

class RewardTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var comments : [[String: Any]] = [[:]]
    var reward_id : Int? = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view_reward()
    }
    
    func view_reward() {
        
        let user_detail = UserDefaults.standard.dictionary(forKey: "userInfo")
        let user_id = user_detail!["user_id"] as! String
        let params = "reward_id=\(String(reward_id!))&user_id=\(user_id))"
        
        LoadingIndicatorView.show()
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            ApiResponse.onResponsePostPhp(url: "userajax/view_reward", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            
                            self.comments = [[:]]
                            self.comments = result["comments"] as! [[String:Any]]
                            print(self.comments)
                            self.comments = self.comments.sorted(by: { (a, b) -> Bool in
                                let first = a["created_at"] as! String
                                let second = b["created_at"] as! String
                                return first < second
                            })
                            self.tableView.reloadData()
                    }
                } else {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
            })
        }
    }
    
    @IBAction func onBackPressed(_ sender: UIBarButtonItem) {
        
        let desVC = storyboard?.instantiateViewController(withIdentifier: "comment") as! RewardCommentVC
        desVC.reward_id = self.reward_id!
        self.present(desVC, animated: true, completion: nil)
    }
    
}

extension RewardTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "rewardCell") as! RewardOneCell
        cell.contentView.layer.cornerRadius = 20.0
        cell.commentTxt.text = self.comments[indexPath.section]["comment"] as? String ?? ""
        cell.usernameLbl.text = self.comments[indexPath.section]["first_name"] as? String ?? ""
        
        var date_str = self.comments[indexPath.section]["created_at"] as? String ?? "2018-01-01 00:00:00"
        let dateformat = DateFormatter()
        dateformat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateformat.date(from: date_str)
        
        dateformat.dateFormat = "MM/dd/yyyy"
        date_str = dateformat.string(from: date!)
        
        
        cell.dateLbl.text = date_str
        let imageUrl = self.comments[indexPath.section]["image"] as? String ?? ""
        if imageUrl == "<null>" || imageUrl == "" {
            cell.imgView?.frame.size.height = 0
            cell.imgView?.image = nil
        } else {
            cell.imgView?.isHidden = false
            let url = URL(string: "\(Constant.IMAGE_URL_NEW)rewards/comments/\(imageUrl)")
            cell.imgView.frame.size.height = 0
            cell.imgView?.sd_setImage(with: url, placeholderImage: UIImage(named: "logo_gray"), options: SDWebImageOptions(rawValue: 0), completed: {(mImage, error, cacheType, imageUrl) in
                
                if error == nil {
                    let ratio = (mImage?.size.height)!/(mImage?.size.width)!
                    let width = cell.commentTxt.frame.size.width
                    var resizedImage = mImage?.fixImageOrientation()
                    resizedImage = resizedImage?.scaleImage(newSize: CGSize.init(width: width, height: width*ratio))
                    cell.imgView.image = resizedImage
                }
            })
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
