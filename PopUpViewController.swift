//
//  PopUpViewController.swift
//  Loycard
//
//  Created by Forhad on 1/23/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import UIKit





class PopUpViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    
    
    
    var token : Int = Int()
    
    var cardNumber : String = String()
    var titleText : String = String()
    var imagelink : String = String()
    var body : String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.mainView.layer.cornerRadius = 8
        self.mainView.clipsToBounds = true
        
        self.showAnimate()
        
        
        titleLabel.text = titleText
        
        
        subtitleLabel.text = body
        
    
        
        
        if(imagelink != nil){
            let img = "\(Constant.IMAGE_URL_NEW)notifications/\(imagelink)"
       iconImage.sd_setImage(with: URL(string : img))
        }else{
            iconImage.image = UIImage.init(named:"fairfield_icon.png")
        }
    }
    
    
    @IBAction func okBtnPressed(_ sender: Any) {
        
        //  dismiss(animated: true, completion: nil)
        removeAnimate()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    
    
    
}
