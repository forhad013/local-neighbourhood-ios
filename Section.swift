//
//  Section.swift
//  FairField
//
//  Created by Prashant Shinde on 7/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import Foundation


struct Section {
    var genere : String!
  //  var movies : [String]!
    var expanded : Bool!
    
    init(genere : String   , expanded : Bool ) {
        self.genere = genere
      //  self.movies = movies
        self.expanded = expanded
    }
    
}
