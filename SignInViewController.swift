//
//  SignInViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseMessaging

class SignInViewController: UIViewController , FBSDKLoginButtonDelegate, UITextFieldDelegate {
    
    @IBOutlet var checkImg1: UIImageView!
    @IBOutlet var checkImg: UIImageView!
    @IBOutlet var fbButton: FBSDKLoginButton!
    @IBOutlet var emailText: UITextField!
    @IBOutlet var passwordText: UITextField!
    

    
    var userDef : UserDefaults!
    var newFrame : CGRect!
    var count : Bool!
    var keyExists : Bool!
    var s1 : String = ""
    
    
    @IBOutlet var popView: UIView!
    @IBOutlet var transparentView: UIView!
    @IBOutlet weak var termsBtn: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!

    var checkOne : Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.transparentView.isHidden = true;
         self.popView.isHidden = true;
        btnConfirm.isHidden = false

        self.tokenRefreshNotification()
     
        userDef = UserDefaults.standard
        
        let textFArray : [UITextField] = [emailText, passwordText]
        for tf in textFArray
        {
            tf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 12, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            emailText.attributedPlaceholder = NSAttributedString(string:"Email",
                                                                 attributes:[kCTForegroundColorAttributeName as NSAttributedStringKey: UIColor.white])
            passwordText.attributedPlaceholder = NSAttributedString(string:"Password",
                                                                    attributes:[kCTForegroundColorAttributeName as NSAttributedStringKey: UIColor.white])
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (ApiResponse.validateEmail(emailText.text!) == true)
        {
            checkImg.image = UIImage(named : "checked.png")
            if ((passwordText.text?.count)! >= 5)
            {
                checkImg1.image = UIImage(named : "checked.png")
                
            }
            else
            {
                checkImg1.image = UIImage(named : "cancel (5).png")
            }
        }
        else
        {
          checkImg.image = UIImage(named : "cancel (5).png")
        }

        if ((passwordText.text?.count)! >= 5)
        {
            checkImg1.image = UIImage(named : "checked.png")
            if (ApiResponse.validateEmail(emailText.text!) == true)
            {
                checkImg.image = UIImage(named : "checked.png")
            }
            else
            {
                checkImg.image = UIImage(named : "cancel (5).png")
            }
        }
        else
        {
            checkImg1.image = UIImage(named : "cancel (5).png")
        }
       return true
    }
  
 
    @IBAction func openSignup(_ sender: Any) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "signup") as! SignUpViewController
        self.present(signUp, animated: true, completion: nil)
         
    }
    
    @IBAction func onSignin(_ sender: Any) {
         print("\(emailText.text!)")
        if (emailText.text == "") || (passwordText.text == "") {
            ApiResponse.alert(title: "Empty", message: "Please fill all the fields", controller: self)
        }
        else if (ApiResponse.validateEmail(emailText.text!) == false) {
            ApiResponse.alert(title: "Invalid Email", message: "Please enter valid email id", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
             

            LoadingIndicatorView.show("Loading....")
            let params = "email=\(emailText.text!)&password=\(passwordText.text!)&from_where=0"
            print("params = \(params)")
            
            ApiResponse.onResponsePostPhp(url: "Userajax/login", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    print("login result = \(result)")
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                   
                    if (status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                            LoadingIndicatorView.hide()
                            let datadict = result["data"] as! NSDictionary
                            self.userDef.set(datadict, forKey: "userInfo")
                            self.userDef.set("login", forKey: "session")
                            self.userDef.set("email", forKey: "loginway")
                            self.userDef.synchronize()
                            let userid = datadict["user_id"] as! String
                         //   Messaging.messaging().subscribe(toTopic: userid)
                            let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
                            self.present(dashboard, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                let alert = UIAlertController(title: "Oops", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                                { action -> Void in
                                    
                                    if(message == "Email not verified."){
                                        
                                        let verifyMail = self.storyboard?.instantiateViewController(withIdentifier: "verifyemailvc") as! VerifyEmailVC
                                        
                                        verifyMail.emailId = self.emailText.text!
                                        self.present(verifyMail, animated: true, completion: nil)
                                        
                                    }
                                    
                                    
                                    
                                })
                                self.present(alert, animated: true, completion: nil)
                        }
                       
                        
                        
                        
                        
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    @IBAction func term(_ sender: Any) {
        
        self.transparentView.isHidden = false;
        self.popView.isHidden = false;
        self.view.bringSubview(toFront: self.transparentView)
        self.view.bringSubview(toFront: self.popView)
        termsBtn.setBackgroundImage(UIImage(named : "check-box-empty.png"), for: .normal)
        checkOne = false


    }
    @IBAction func signupWithFb(_ sender: Any) {
        print("signupWithFb")
        fbButton.readPermissions = ["public_profile", "email", "user_friends"]
        fbButton.delegate = self
//        self.transparentView.isHidden = false;
//        self.popView.isHidden = false;
//        self.view.bringSubview(toFront: self.transparentView)
//        self.view.bringSubview(toFront: self.popView)


//        UIView.animate(withDuration: 0.5, animations: {
//            self.transparentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//            self.popView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//            self.transparentView.alpha = 1.0;
//            self.popView.alpha = 1.0;
//
//        }, completion:{(finished : Bool)  in
//            if (finished)
//            {
////                self.popView.removeFromSuperview()
////                self.transparentView.removeFromSuperview()
//                self.view.addSubview(self.transparentView)
//                self.view.addSubview(self.popView)
//
//
//            }
//        });

      
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logout")
    }

    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        print("signupWithFb")

        print("User Logged In")

        if ((error) != nil)
        {
            // Process error
            print("process error\(error)")
        }
        else if result.isCancelled
        {
            // Handle cancellations
        }
        else
        {
            if result.grantedPermissions.contains("email")
            {
 
                print("do wrok")
                self.returnUserData()
            }
        }
    }
    
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"first_name, last_name , email, picture.type(large) , gender  " ])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(String(describing: error))")
            }
            else
            {
                print("fetched user: \(String(describing: result ))")
                //let userdef = UserDefaults.standard
                //let token =  userdef.value(forKey: "devicetoken")
                let result1 = result as AnyObject
                
               // print("tokennnn = \(String(describing: token))")
                

                let deviceid = UIDevice.current.identifierForVendor!.uuidString
                
                let fullName = "\(result1["first_name"] as! String) \(result1["last_name"] as! String)"
                
                let postString123 = "first_name=\(fullName)&email=\(result1["email"] as! String)&device_token=device123456token&device_type=IOS&device_id=\(deviceid)&from_where=1&social_id=\(result1["id"] as! String)&contact="
                // &contact=\(self.result1["contact"] as! String)
                
                print("postString123 \(postString123)" )
                self.fbjson(pstring: postString123)
            }
        })
    }
    
    
    func tokenRefreshNotification()
    {
        if let refreshedToken = Messaging.messaging().fcmToken{
            print("Sign in InstanceID token: \(refreshedToken)")
            // self.checkNotificationPermission()
        }
        
        
        
    }
    
    
    func fbjson( pstring : String)
    {
        ApiResponse.onResponsePostPhp(url: "Userajax/facebook_login", parms: pstring, completion: {(result , error) in
            
            if(error == ""){
                
                print("fb login rezsult = \(result)")
                let status = result["status"]as! Bool
                let message = result["message"]as! String
                
                if(status == true){
                    OperationQueue.main.addOperation {
                        
                        let datadict = result["data"] as! NSDictionary
                        self.userDef.set(datadict, forKey: "userInfo")
                        self.userDef.set("login", forKey: "session")
                        self.userDef.set("facebook", forKey: "loginway")
                        self.userDef.synchronize()
                        
                        let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
                        self.present(dashboard, animated: true, completion: nil)
                    }
                }
                else{
                    ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                }
                
            }
            else{
                if(error == Constant.Status_Not_200){
                    ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                }
            }
        })
    }
    

    
    @IBAction func forgotPassword(_ sender: Any) {
        
        let forgotPwd = self.storyboard?.instantiateViewController(withIdentifier: "forgot") as! ForgotPasswordVC
        self.present(forgotPwd, animated: true, completion: nil)
        
    }
    @IBAction func checkBtn(_ sender: Any) {
        //            termsBtn.setBackgroundImage(BoxOn , for: .normal)
        //            termsBtn.setBackgroundImage( BoxOff , for: .normal)
        if (checkOne == false)
        {
            termsBtn.setBackgroundImage(UIImage(named : "CheckBoxFill.png"), for: .normal)
            checkOne = true
        }
        else {
            
            termsBtn.setBackgroundImage(UIImage(named : "check-box-empty.png"), for: .normal)
            checkOne = false
        }
    }
    @IBAction func btnConfirm(_ sender: Any) {
        if (checkOne == true)
        {
//            UIView.animate(withDuration: 0.5, animations: {
//                self.transparentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//                self.popView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//                self.transparentView.alpha = 0.0;
//                self.popView.alpha = 0.0;
//
//            }, completion:{(finished : Bool)  in
//                if (finished)
//                {
//                    self.popView.removeFromSuperview()
//                    self.transparentView.removeFromSuperview()
//
//                }
//            });
            self.transparentView.isHidden = true;
            self.popView.isHidden = true;
            btnConfirm.isHidden = true

//                    fbButton.readPermissions = ["public_profile", "email", "user_friends"]
//                    fbButton.delegate = self

        }
        else
        {
            ApiResponse.alert(title: "", message: "Please agree terms and conditions.", controller: self)
            
            
        }
        
    }
    @IBAction func btnCancel(_ sender: Any) {
//        let signIn = self.storyboard?.instantiateViewController(withIdentifier: "signin") as! SignInViewController
//        self.present(signIn, animated: true, completion: nil)
        
//        self.transparentView.alpha = 0.0;
//        self.popView.alpha = 0.0;
        
        self.transparentView.isHidden = true;
        self.popView.isHidden = true;
        btnConfirm.isHidden = false

//        self.popView.removeFromSuperview()
//        self.transparentView.removeFromSuperview()


//        UIView.animate(withDuration: 0.5, animations: {
//            self.transparentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//            self.popView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//            self.transparentView.alpha = 0.0;
//            self.popView.alpha = 0.0;
//
//        }, completion:{(finished : Bool)  in
//            if (finished)
//            {
//                self.popView.removeFromSuperview()
//                self.transparentView.removeFromSuperview()
//
//            }
//        });

        
    }
    
    
    @IBAction func termsAneconditionBtm(_ sender: Any) {
        let web = self.storyboard?.instantiateViewController(withIdentifier: "webviewvc")as! WebViewVC
        self.present(web, animated: true, completion: nil)
    }
    

    
}
