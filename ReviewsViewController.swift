//
//  ReviewsViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 8/4/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import FirebaseMessaging
import ReachabilitySwift

class ReviewsViewController: UIViewController {
    
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet weak var listTableView2: UITableView!
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var imageViewForAds: UIImageView!
    
    
    var reviewArr : [NSDictionary] = []
    var listArray2 : [String] = []
    var reviewRating1 : Int!
    var advertisementArr1 : [NSDictionary] = []
    var scrollingTimer = Timer()
    var userDef : UserDefaults!
    var user_id : String = ""
    var ownerId2 : String = ""
    var idStr : String = ""
    var strOne : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"
        self.labelOne.isHidden = true
        
       
        DispatchQueue.main.async
            {
         
            self.listArray2 = ["New Businesses", "New Reviews",   "Requested Reviews"]
            self.listTableView2.isHidden = true
            self.reviewTableView.reloadData()
            self.showAdvertisement1()
            self.addGestureToAdsImageView()
        }
        
    }
    
    
    func addGestureToAdsImageView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(adsImageViewAction))
        self.imageViewForAds.isUserInteractionEnabled = true
        self.imageViewForAds.tag = -1
        self.imageViewForAds.addGestureRecognizer(tapGesture)
    }
    
    private var adsImageIndex = 0
    
    @objc func startTimer(theTimer : Timer){
        //        UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseOut, animations: {
        //            self.addCollectionViewThree.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int , section:0), at: .centeredHorizontally, animated: false)
        //        }, completion: nil)
        setupForAdsImages()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
           self.showReview()
    }
    
    func stopTimerTest() {
        
        scrollingTimer.invalidate()
    }
    
    func setupForAdsImages() {
        if adsImageIndex >= advertisementArr1.count {
            adsImageIndex = 0
        }
        let dict1 = advertisementArr1[adsImageIndex]
        adView(userid: self.user_id, bannerid: dict1.value(forKey: "id")! as! String, ownerid: dict1.value(forKey: "owner_id")! as! String)
        self.imageViewForAds.tag = adsImageIndex
        adsImageIndex = adsImageIndex + 1
        let imageString = dict1["image"] as! String
       // print("imageString = \(imageString)")
        imageViewForAds.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
        
        
    }
    
    @objc func adsImageViewAction(sender: UITapGestureRecognizer!) {
        print("adsImageView clicked \((sender.view?.tag)!)")
        if (sender.view?.tag)! != -1 {
            let dict2 = advertisementArr1[(sender.view?.tag)!]
            self.ownerId2 = (dict2["owner_id"] as? String)!
            print("ownerId ======******\(ownerId2)")
            adClick(userid: self.user_id, bannerid: dict2.value(forKey: "id")! as! String, ownerid: dict2.value(forKey: "owner_id")! as! String)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func showList(_ sender: Any) {
       
        if listTableView2.isHidden {
            listTableView2.isHidden = false
        } else {
            listTableView2.isHidden = true
        }
        
    }
    
    func showReview()
    {
        LoadingIndicatorView.show()
        let params = ""
        ApiResponse.onResponsePostPhp(url: "Userajax/new_reviews", parms: params, completion: { (result, error) in
            if (error == "")
            {
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                if (status == true)
                {
                     print("Reviews result = \(result)")
                     OperationQueue.main.addOperation
                        {
                        LoadingIndicatorView.hide()
                        self.reviewArr = result["data"] as! [NSDictionary]
                            self.reviewTableView.reloadData()
                            
                            if self.reviewArr.count != 0
                            {
                                self.labelOne.isHidden = true
                                self.reviewTableView.reloadData()
                            }
                            else
                            {
                                self.reviewTableView.isHidden = true
                                self.labelOne.isHidden = false
                            }

                    }
                }
                else
                {
                  //  ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
    }
    
    @IBAction func onBackReview(_ sender: Any) {
        if (strOne == "one")
        {
            let dash = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
            self.present(dash, animated: false, completion: nil)
        }
        else
        {
            stopTimerTest()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func adClick(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/banner_click", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                self.addDetailTwo()
                                
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    func adView(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/save_page_viewer", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
     
    
    func showAdvertisement1(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let pstring = ""
            ApiResponse.onResponsePostPhp(url: "Userajax/banners", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                            self.advertisementArr1 = result["data"] as! [NSDictionary]
                            if self.advertisementArr1.count != 0
                            {
//                                self.addCollectionView.reloadData()
                                self.setupForAdsImages()
                                self.scrollingTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: nil, repeats: true)
                                self.scrollingTimer.fire()
                            }
                            else
                            {
//                            self.addCollectionView.isHidden = true
                            }
                        }
                    }
                    else
                    {
                  //      ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                    
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    var newsArr: [NSDictionary] = []
}

class ReviewTableViewCell : UITableViewCell
{
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet var reviewRatings: [UIImageView]!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var reviewDescription: UILabel!
    @IBOutlet weak var imgBtn: UIButton!
    @IBOutlet weak var reviewDescriptionHt: NSLayoutConstraint!
    @IBOutlet weak var businessNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
}

extension ReviewsViewController : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return 1
        var count:Int?
        
        if tableView == self.reviewTableView
        {
            count = reviewArr.count
        }
        else
        {
            count =  listArray2.count
        }
        
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.reviewTableView
        {
            let cell : ReviewTableViewCell = reviewTableView.dequeueReusableCell(withIdentifier: "reviewtableviewcell") as! ReviewTableViewCell
            
            let reviewDict  = reviewArr[indexPath.row]
            let userid = reviewDict["user_id"] as! String
            if (user_id == userid)
            {
                cell.editBtn.isHidden = false
                cell.deleteBtn.isHidden = false
                
                
                cell.deleteBtn.tag = Int((reviewDict["id"]  as? String)!)!
                cell.editBtn.tag = indexPath.row
                
                cell.deleteBtn.addTarget(self, action: #selector(deleteReview(_:)), for: .touchUpInside)
                cell.editBtn.addTarget(self, action: #selector(editDetail(_:)), for: .touchUpInside)
            }
            else
            {
                cell.editBtn.isHidden = true
                cell.deleteBtn.isHidden = true
            }
            
            idStr = reviewDict["id"] as! String
            cell.name.text = reviewDict["title"] as? String
            let ht1 = ApiResponse.calculateHeightForString((reviewDict["review"] as? String)!)
            cell.reviewDescriptionHt.constant = ht1 + 15
            cell.reviewDescription.numberOfLines = 0
            cell.reviewDescription.text = reviewDict["review"] as? String
            cell.businessNameLabel.text = reviewDict["businessname"] as? String
            cell.userNameLabel.text = reviewDict["user_name"] as? String
            
            ApiResponse.callRating(Double((reviewDict["rating"] as? String)!)!, starRating: cell.reviewRatings)
            
            if ((reviewDict["images"] as! NSArray).count == 0){
                cell.imgBtn.isHidden = true
            }
            else{
            
            cell.imgBtn.isHidden = false
            cell.imgBtn.layer.cornerRadius = cell.imgBtn.frame.size.width/2
            cell.imgBtn.clipsToBounds = true
            cell.imgBtn.addTarget(self, action: #selector(imageDetail1(_:)), for: .touchUpInside)
            }
            cell.layer.borderColor =  UIColor.init(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1.0).cgColor
            cell.layer.borderWidth = 4
            cell.clipsToBounds = true
            
            return cell
        }
        else
        {
            let cell : UITableViewCell = listTableView2.dequeueReusableCell(withIdentifier: "listcell2")!
            
            cell.textLabel?.text = listArray2[indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            
            let itemSize = CGSize(width : 15, height : 15);
            UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
            let imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
            cell.imageView?.image?.draw(in: imageRect)
            cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return cell
        }
        
    }
    
    @objc func imageDetail1(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:reviewTableView)
        let indexPath1 = reviewTableView.indexPathForRow(at: point)
        let dictNoti1 = self.reviewArr[(indexPath1?.item)!]
        let showImage = self.storyboard?.instantiateViewController(withIdentifier: "imageviewcontroller") as! ImageViewController
        showImage.commentDict = dictNoti1
        showImage.imgStr = "reviewImg"
        self.present(showImage, animated: true, completion: nil)
    }
    
    @objc func editDetail(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:reviewTableView)
        let indexPath1 = reviewTableView.indexPathForRow(at: point)
        let dictNoti = self.reviewArr[(indexPath1?.item)!]
        let edit = self.storyboard?.instantiateViewController(withIdentifier: "feedbackvw") as! FeedbackViewController
        edit.editDictOne = dictNoti
        edit.editStr = "edit"
        self.present(edit, animated: true, completion: nil)
    }

    
    @objc func deleteReview(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:reviewTableView)
        self.idStr  = String(sender.tag)
        let indexPath1 = reviewTableView.indexPathForRow(at: point)
        let dictNoti1 = self.reviewArr[(indexPath1?.item)!]
        self.showDeleteAlert()
        
    }
    
    func showDeleteAlert(){
        let alert = UIAlertController(title: "Deleting the review.", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default,  handler: { action in
            
            self.DeleteReview()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel,  handler: { action in
            
            alert.dismiss(animated: true, completion: nil)
            
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    

    func DeleteReview()
    {
        
        let params = "review_id=\(self.idStr)"
        print(params)
        ApiResponse.onResponsePostPhp(url: "Userajax/delete_rating", parms: params, completion: { (result, error) in
            if (error == "")
            {
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                print("result = \(result)")
    
                if (status == true)
                {
                    OperationQueue.main.addOperation
                        {
                            
                            self.showReview()
                        
                            ApiResponse.alertWithLoading(title: "Done", message: message, controller: self)
                            
                    }
                }
                else
                {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                    }
                    //   ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else
            {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == reviewTableView
        {
            let resourceDict  = reviewArr[indexPath.item]
            let ht1 = ApiResponse.calculateHeightForString((resourceDict["review"] as? String)!)
            return 123 + ht1
        } else
        {
            return 31
        }
        
    }

    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let vw = UIView.init(frame: CGRect(x : 0, y : 0, width : reviewTableView.frame.width, height : 10))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.listTableView2 {
            switch indexPath.row {
            case 0:
                print("Business Info")
                let feed = self.storyboard?.instantiateViewController(withIdentifier: "newsfeed") as! NewsFeedVC
                feed.searchone = "search1"
                self.present(feed, animated: true, completion: nil)
                
                break
                
            case 1:
                print("View Reviews")
                let feed = self.storyboard?.instantiateViewController(withIdentifier: "reviewsviewcontroller") as! ReviewsViewController
                self.present(feed, animated: true, completion: nil)
                
                break
                
            case 2:
                print("Review Request")
                let feed = self.storyboard?.instantiateViewController(withIdentifier: "reviewrequestvc") as! ReviewRequestVC
                self.present(feed, animated: true, completion: nil)
                
                break
                
            default :
                break
            }
            listTableView2.isHidden = true
        }
        else if tableView == reviewTableView {
            self.getLocal(index: indexPath.row)
        }
    }
    
    func getLocal(index: Int){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = ""
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/new_business_owner", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("local data == \(result)")
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            LoadingIndicatorView.hide()
                            self.newsArr = result["data"] as! [NSDictionary]
                            if self.newsArr.count > 0 {
                                print("Array is \(self.newsArr)")
                                //reviewArr[1]["owner_id"]!
                                
                                for dictionary in self.newsArr {
                                    if dictionary["owner_id"] as! String == self.reviewArr[index]["owner_id"] as! String {
                                        let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                                        dictProvider.providerdetailDict = dictionary
                                        self.present(dictProvider, animated: true, completion: nil)
                                        break
                                    }
                                }
                                
                            }
                            else {
                                
                            }
                            
                        }
                    }
                    else{
                        LoadingIndicatorView.hide()
                        //  ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
}

extension ReviewsViewController {//} : UICollectionViewDataSource , UICollectionViewDelegate {
    

    func addDetailTwo(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = "owner_id=\(self.ownerId2)"
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/get_business_owner", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                 //   print("ressassss = \(result)")
                    if(status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                            let dict5 = result["data"] as! NSDictionary
                            let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                            dictProvider.providerdetailDict = dict5
                            self.present(dictProvider, animated: true, completion: nil)
                            LoadingIndicatorView.hide()
                         // ApiResponse.alert(title: "OK", message: message, controller: self)
                        }
                    }
                    else{
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else
                {
                    if(error == Constant.Status_Not_200){
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
}
class AddCell : UICollectionViewCell
{
    @IBOutlet weak var addImage: UIImageView!
}


