//
//  PromotionViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import MDCSwipeToChoose

class PromotionViewController: UIViewController , MDCSwipeToChooseDelegate {
    
    @IBOutlet var swipeView: MDCSwipeToChooseView!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var couponLabel: UILabel!
    
    var imagearr = [ImageCard]()
    var namearr : [String] = []
    var discriptionarr : [String] = []
    var imgArray : [UIImage] = []
    var idx : Int = 0
    var ch : Int = 0
    var emojiOptionsOverlay: EmojiOptionsOverlay!
    var user_id : String = ""
    var userDef : UserDefaults!
    var couponArr : [NSDictionary] = []
    var couponIdArr : [NSDictionary] = []
    var viewCouponArr : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // imagearr = [UIImage(named : "coupon-code.png")!, UIImage(named : "coupon-code.png")!, UIImage(named : "coupon-code.png")!, UIImage(named : "coupon-code.png")!, UIImage(named : "coupon-code.png")!]
        
        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"
        couponLabel.isHidden = true
        DispatchQueue.main.async {
            self.ShowCoupons()
        }
    }
    
    
    @IBAction func btnHelp(_sender: Any)
    {
        
        let alert = UIAlertController(title: nil, message: "Swipe right to save the coupon and swipe left to discard. To contact the business and redeem your coupon go to your Saved Coupons click the coupon you want to redeem and you will see the Business Info button on the bottom right.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true)

    }
    @IBAction func couponButton(_ sender: Any) {
        let save = self.storyboard?.instantiateViewController(withIdentifier: "savedcoupons") as! SavedCouponsVC
        self.present(save, animated: true, completion: nil)
    }
    
    func showCard(){
        
        dynamicAnimator = UIDynamicAnimator(referenceView: self.view)
        setUpDummyUI()
        
        // 1. create a deck of cards
        // 20 cards for demonstrational purposes - once the cards run out, just re-run the project to start over
        // of course, you could always add new cards to self.cards and call layoutCards() again
        for i in  1...imgArray.count
        {
            let card = ImageCard(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 60, height: self.view.frame.height * 0.5))
            print("cardddd = \(card.subviews)")
            let imgv = card.viewWithTag(11) as! UIImageView
            let titleTextLabel = card.viewWithTag(99999) as! UILabel
            let descriptionTextLabel = card.viewWithTag(999999) as! UILabel
            titleTextLabel.text = "\(namearr[i-1])"
            descriptionTextLabel.text = "\(discriptionarr[i-1])"
            imgv.image = imgArray[i-1]
            imagearr.append(card)
            namearr.append("Title-\(i)")
            discriptionarr.append("Title-\(i)")
        }
        
        print("cardsss count = \(imagearr.count)")
        
        // 2. layout the first 4 cards for the user
        layoutCards()
        
        // 3. set up the (non-interactive) emoji options overlay
        emojiOptionsOverlay = EmojiOptionsOverlay(frame: self.view.frame)
        self.view.addSubview(emojiOptionsOverlay)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    let cardAttributes: [(downscale: CGFloat, alpha: CGFloat)] = [(1, 1), (0.92, 0.8), (0.84, 0.6), (0.76, 0.4)]
    let cardInteritemSpacing: CGFloat = 15
    
    func layoutCards() {
        // frontmost card (first card of the deck)
        let firstCard = imagearr[0]
        self.view.addSubview(firstCard)
        firstCard.layer.zPosition = CGFloat(imagearr.count)
        firstCard.center = self.view.center
        firstCard.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleCardPan)))
        
        // the next 3 cards in the deck
        for i in 1...3 {
            if i > (imagearr.count - 1) { continue }
            
            let card = imagearr[i]
            
            card.layer.zPosition = CGFloat(imagearr.count - i)
            // here we're just getting some hand-picked vales from cardAttributes (an array of tuples)
            // which will tell us the attributes of each card in the 4 cards visible to the user
            let downscale = cardAttributes[i].downscale
            let alpha = cardAttributes[i].alpha
            card.transform = CGAffineTransform(scaleX: downscale, y: downscale)
            card.alpha = alpha
            
            // position each card so there's a set space (cardInteritemSpacing) between each card, to give it a fanned out look
            card.center.x = self.view.center.x
            card.frame.origin.y = imagearr[0].frame.origin.y - (CGFloat(i) * cardInteritemSpacing)
            // workaround: scale causes heights to skew so compensate for it with some tweaking
            if i == 3 {
                card.frame.origin.y += 1.5
            }
            
            self.view.addSubview(card)
        }
        
        // make sure that the first card in the deck is at the front
        self.view.bringSubview(toFront: imagearr[0])
        titleLable.text = namearr[0]
        descriptionLabel.text = discriptionarr[0]
    }
    
    func showNextCard()
    {
        let animationDuration: TimeInterval = 0.2
        // 1. animate each card to move forward one by one
        for i in 1...3 {
            if i > (imagearr.count - 1) { continue }
            let card = imagearr[i]
            
            titleLable.text = namearr[i]
             descriptionLabel.text = discriptionarr[i]
            let newDownscale = cardAttributes[i - 1].downscale
            let newAlpha = cardAttributes[i - 1].alpha
            UIView.animate(withDuration: animationDuration, delay: (TimeInterval(i - 1) * (animationDuration / 2)), usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: [], animations: {
                card.transform = CGAffineTransform(scaleX: newDownscale, y: newDownscale)
                card.alpha = newAlpha
                if i == 1 {
                    card.center = self.view.center
                } else {
                    card.center.x = self.view.center.x
                    card.frame.origin.y = self.imagearr[1].frame.origin.y - (CGFloat(i - 1) * self.cardInteritemSpacing)
                }
            }, completion: { (_) in
                if i == 1 {
                    card.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handleCardPan)))
                }
            })
            
        }
        
        // 2. add a new card (now the 4th card in the deck) to the very back
        if 4 > (imagearr.count - 1) {
            if imagearr.count != 1 {
                self.view.bringSubview(toFront: imagearr[1])
                titleLable.text = namearr[1]
                 descriptionLabel.text = discriptionarr[1]
            }
            else {
                titleLable.isHidden = true
                descriptionLabel.isHidden = true
            }
            return
        }
        let newCard = imagearr[4]
        newCard.layer.zPosition = CGFloat(imagearr.count - 4)
        let downscale = cardAttributes[3].downscale
        let alpha = cardAttributes[3].alpha
        
        // initial state of new card
        newCard.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        newCard.alpha = 0
        newCard.center.x = self.view.center.x
        newCard.frame.origin.y = imagearr[1].frame.origin.y - (4 * cardInteritemSpacing)
        self.view.addSubview(newCard)
        titleLable.text = namearr[4]
        descriptionLabel.text = discriptionarr[4]
        
        // animate to end state of new card
        UIView.animate(withDuration: animationDuration, delay: (3 * (animationDuration / 2)), usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: [], animations: {
            newCard.transform = CGAffineTransform(scaleX: downscale, y: downscale)
            newCard.alpha = alpha
            newCard.center.x = self.view.center.x
            newCard.frame.origin.y = self.imagearr[1].frame.origin.y - (3 * self.cardInteritemSpacing) + 1.5
        }, completion: { (_) in
            
        })
        // first card needs to be in the front for proper interactivity
        self.view.bringSubview(toFront: self.imagearr[1])
        titleLable.text = namearr[1]
         descriptionLabel.text = discriptionarr[1]
    }
    
    func removeOldFrontCard() {
        imagearr[0].removeFromSuperview()
        imagearr.remove(at: 0)
        namearr.remove(at: 0)
        discriptionarr.remove(at: 0)
        self.couponArr.remove(at: 0)
    }
    
    var dynamicAnimator: UIDynamicAnimator!
    var cardAttachmentBehavior: UIAttachmentBehavior!
    
     @objc func handleCardPan(sender: UIPanGestureRecognizer) {
        // if we're in the process of hiding a card, don't let the user interace with the cards yet
        if cardIsHiding { return }
        // change this to your discretion - it represents how far the user must pan up or down to change the option
        let optionLength: CGFloat = 60
        // distance user must pan right or left to trigger an option
        let requiredOffsetFromCenter: CGFloat = 15
        
        let panLocationInView = sender.location(in: view)
        let panLocationInCard = sender.location(in: imagearr[0])
        switch sender.state {
        case .began:
            dynamicAnimator.removeAllBehaviors()
            let offset = UIOffsetMake(panLocationInCard.x - imagearr[0].bounds.midX, panLocationInCard.y - imagearr[0].bounds.midY);
            // card is attached to center
            cardAttachmentBehavior = UIAttachmentBehavior(item: imagearr[0], offsetFromCenter: offset, attachedToAnchor: panLocationInView)
            dynamicAnimator.addBehavior(cardAttachmentBehavior)
        case .changed:
            cardAttachmentBehavior.anchorPoint = panLocationInView
            if imagearr[0].center.x > (self.view.center.x + requiredOffsetFromCenter) {
                
                print("like imagessssssssssssss")
                
                let ddict = couponArr[0] 
                let coupon_id = ddict["id"] as! String
                let owner_id = ddict["owner_id"] as! String

                let idDict = ["\(owner_id)":"\(coupon_id)"]
                self.couponIdArr.append(idDict as NSDictionary)
                self.viewCouponArr.append(idDict as NSDictionary)
                
                if imagearr[0].center.y < (self.view.center.y - optionLength) {
                    imagearr[0].showOptionLabel(option: .like1)
                    emojiOptionsOverlay.showEmoji(for: .like1)
                    
                    if imagearr[0].center.y < (self.view.center.y - optionLength - optionLength) {
                        emojiOptionsOverlay.updateHeartEmoji(isFilled: true, isFocused: true)
                    } else {
                        emojiOptionsOverlay.updateHeartEmoji(isFilled: true, isFocused: false)
                    }
                    
                } else if imagearr[0].center.y > (self.view.center.y + optionLength) {
                    imagearr[0].showOptionLabel(option: .like3)
                    emojiOptionsOverlay.showEmoji(for: .like3)
                    emojiOptionsOverlay.updateHeartEmoji(isFilled: false, isFocused: false)
                } else {
                    imagearr[0].showOptionLabel(option: .like2)
                    emojiOptionsOverlay.showEmoji(for: .like2)
                    emojiOptionsOverlay.updateHeartEmoji(isFilled: false, isFocused: false)
                }
                LikeCoupons()
            } else if imagearr[0].center.x < (self.view.center.x - requiredOffsetFromCenter) {
                
                emojiOptionsOverlay.updateHeartEmoji(isFilled: false, isFocused: false)
                
                print("dislike imagessssssssssssss")
                let ddict = couponArr[0]
                let coupon_id = ddict["id"] as! String
                let owner_id = ddict["owner_id"] as! String
                
                let idDict = ["\(owner_id)":"\(coupon_id)"]
                
                print("idDict ==== \(idDict)")
                self.viewCouponArr.append(idDict as NSDictionary)
                
                if imagearr[0].center.y < (self.view.center.y - optionLength) {
                    imagearr[0].showOptionLabel(option: .dislike1)
                    emojiOptionsOverlay.showEmoji(for: .dislike1)
                } else if imagearr[0].center.y > (self.view.center.y + optionLength) {
                    imagearr[0].showOptionLabel(option: .dislike3)
                    emojiOptionsOverlay.showEmoji(for: .dislike3)
                } else {
                    imagearr[0].showOptionLabel(option: .dislike2)
                    emojiOptionsOverlay.showEmoji(for: .dislike2)
                }
            } else {
                imagearr[0].hideOptionLabel()
                emojiOptionsOverlay.hideFaceEmojis()
            }
            
        case .ended:
            
            dynamicAnimator.removeAllBehaviors()
            
            if emojiOptionsOverlay.heartIsFocused {
                // animate card to get "swallowed" by heart
                
                let currentAngle = CGFloat(atan2(Double(imagearr[0].transform.b), Double(imagearr[0].transform.a)))
                
                let heartCenter = emojiOptionsOverlay.heartEmoji.center
                var newTransform = CGAffineTransform.identity
                newTransform = newTransform.scaledBy(x: 0.05, y: 0.05)
                newTransform = newTransform.rotated(by: currentAngle)
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseOut], animations: {
                    self.imagearr[0].center = heartCenter
                    self.imagearr[0].transform = newTransform
                    self.imagearr[0].alpha = 0.5
                }, completion: { (_) in
                    self.emojiOptionsOverlay.updateHeartEmoji(isFilled: false, isFocused: false)
                    self.removeOldFrontCard()
                })
                emojiOptionsOverlay.hideFaceEmojis()
                showNextCard()
                
            } else {
                emojiOptionsOverlay.hideFaceEmojis()
                emojiOptionsOverlay.updateHeartEmoji(isFilled: false, isFocused: false)
                
                if !(imagearr[0].center.x > (self.view.center.x + requiredOffsetFromCenter) || imagearr[0].center.x < (self.view.center.x - requiredOffsetFromCenter)) {
                    // snap to center
                    let snapBehavior = UISnapBehavior(item: imagearr[0], snapTo: self.view.center)
                    dynamicAnimator.addBehavior(snapBehavior)
                } else {
                    
                    let velocity = sender.velocity(in: self.view)
                    let pushBehavior = UIPushBehavior(items: [imagearr[0]], mode: .instantaneous)
                    pushBehavior.pushDirection = CGVector(dx: velocity.x/10, dy: velocity.y/10)
                    pushBehavior.magnitude = 175
                    dynamicAnimator.addBehavior(pushBehavior)
                    // spin after throwing
                    var angular = CGFloat.pi / 2 // angular velocity of spin
                    
                    let currentAngle: Double = atan2(Double(imagearr[0].transform.b), Double(imagearr[0].transform.a))
                    
                    if currentAngle > 0 {
                        angular = angular * 1
                    } else {
                        angular = angular * -1
                    }
                    let itemBehavior = UIDynamicItemBehavior(items: [imagearr[0]])
                    itemBehavior.friction = 0.2
                    itemBehavior.allowsRotation = true
                    itemBehavior.addAngularVelocity(CGFloat(angular), for: imagearr[0])
                    dynamicAnimator.addBehavior(itemBehavior)
                    showNextCard()
                    hideFrontCard()
                }
            }
        default:
            break
        }
    }
    
    var cardIsHiding = false
    
    func hideFrontCard()
    {
        if #available(iOS 10.0, *) {
            var cardRemoveTimer: Timer? = nil
            cardRemoveTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [weak self] (_) in
                guard self != nil else { return }
                if !(self!.view.bounds.contains(self!.imagearr[0].center)) {
                    cardRemoveTimer!.invalidate()
                    self?.cardIsHiding = true
                    UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn], animations: {
                        self?.imagearr[0].alpha = 0.0
                    }, completion: { (_) in
                        self?.removeOldFrontCard()
                        self?.cardIsHiding = false
                    })
                }
            })
        } else {
            // fallback for earlier versions
            UIView.animate(withDuration: 0.2, delay: 1.5, options: [.curveEaseIn], animations: {
                self.imagearr[0].alpha = 0.0
            }, completion: { (_) in
                self.removeOldFrontCard()
            })
        }
    }
    
    
    @IBAction func onBack(_ sender: Any) {
       // self.LikeCoupons()
        self.dismiss(animated: false, completion: nil)
        let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
        
        self.present(dashboard, animated: true, completion: nil)
    }
    
    func ShowCoupons() {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")

            let pstring = "user_id=\(user_id)"
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/coupon_list", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                   print("coupons = \(result)")
                    
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            LoadingIndicatorView.hide()
                            
                            let data1 = result["data"] as! [NSDictionary]
                            for dd in data1 {
                        
                                
                                 let img = "\(Constant.IMAGE_URL_NEW)coupon/\(dd["image"] as! String)"
                              //  let img = "https://www.localneighborhoodapp.com/images/coupon/QWxjbyBDb3Vwb24uUE5H_1524061278"
                                let imgurl = URL(string : img)
                                 let imageData:NSData = NSData(contentsOf: imgurl!)!
                                 let image = UIImage(data: imageData as Data)
                                
                                self.imgArray.append(image!)
                               //   self.imagearr.append(card)
                              //  print("self.imgArray.append(img) ====\(self.imgArray.append(img))")
                                self.namearr.append(dd["coupon_name"] as! String)
                                self.discriptionarr.append(dd["description"] as! String)
                                self.couponArr.append(dd)
                            }
                            DispatchQueue.main.async
                            {
                                if (self.imgArray.count != 0)  {  //self.imagearr.count
                                     self.showCard()
                                    self.couponLabel.isHidden = true
                                }
                                else
                                {
                                    OperationQueue.main.addOperation
                                        {
                                            self.swipeView.isHidden = true
                                            self.descriptionLabel.isHidden = true
                                            self.titleLable.isHidden = true
                                            self.couponLabel.isHidden = false
                                            
                                    }
                                }
                            }
                        }
                    }
                    else{
                        //LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        // LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        // LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    // * REMOVE DUPLICACY AND REARRANGE ** //
    
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    func LikeCoupons()
    {
        self.couponIdArr = uniq(source: self.couponIdArr)
        self.viewCouponArr = uniq(source: self.viewCouponArr)
        
        var jsonDict1 : NSString = ""
        var jsonDict2 : NSString = ""
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: couponIdArr, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            jsonDict1 = NSString(data: jsonData,
                                   encoding: String.Encoding.ascii.rawValue)!
            // here "decoded" is of type `Any`, decoded from JSON data
        }
        catch {
            print(error.localizedDescription)
        }
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: viewCouponArr, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            jsonDict2 = NSString(data: jsonData,
                                 encoding: String.Encoding.ascii.rawValue)!
            // here "decoded" is of type `Any`, decoded from JSON data
        }
        catch {
            print(error.localizedDescription)
        }

//        print("couponssss arr = \(couponIdArr)---\(viewCouponArr)")
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
           // LoadingIndicatorView.show("Loading....")
            let pstring = "coupon_id=\(jsonDict1)&user_id=\(user_id)&view_coupon=\(jsonDict2)"
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/like_coupon_by_user", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    print("ressassss = \(result)")
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            
                          //  let data1 = result["data"] as! [NSDictionary]
                        }
                    }
                    else{
                       // LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                    
                }
                else{
                    if(error == Constant.Status_Not_200){
                       // LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                       // LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
}

extension PromotionViewController {
    /// Hide status bar
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    /// Dummy UI
    func setUpDummyUI() {
        // menu icon
        let menuIconImageView = UIImageView(image: UIImage(named: "menu_icon"))
        menuIconImageView.contentMode = .scaleAspectFit
        menuIconImageView.frame = CGRect(x: 35, y: 30, width: 35, height: 30)
        menuIconImageView.isUserInteractionEnabled = false
        self.view.addSubview(menuIconImageView)
        
        // title label
        let titleLabel = UILabel()
      //  titleLabel.text = "How do you like\nthis one?"
        titleLabel.numberOfLines = 2
        titleLabel.font = UIFont(name: "AvenirNext-Bold", size: 19)
        titleLabel.textColor = UIColor(red: 83/255, green: 98/255, blue: 196/255, alpha: 1.0)
        titleLabel.textAlignment = .center
        titleLabel.frame = CGRect(x: (self.view.frame.width / 2) - 90, y: 17, width: 180, height: 60)
        self.view.addSubview(titleLabel)
               // <- ☹️
        let frownArrowImageView = UIImageView(image: UIImage(named: "frown_arrow"))
        frownArrowImageView.contentMode = .scaleAspectFit
        frownArrowImageView.frame = CGRect(x: (self.view.frame.width / 2) - 140, y: self.view.frame.height - 70, width: 80, height: 50)
        frownArrowImageView.isUserInteractionEnabled = false
        self.view.addSubview(frownArrowImageView)
        
        // 🙂 ->
        let smileArrowImageView = UIImageView(image: UIImage(named: "smile_arrow"))
        smileArrowImageView.contentMode = .scaleAspectFit
        smileArrowImageView.frame = CGRect(x: (self.view.frame.width / 2) + 60, y: self.view.frame.height - 70, width: 80, height: 50)
        smileArrowImageView.backgroundColor = UIColor.clear
        smileArrowImageView.isUserInteractionEnabled = false
        self.view.addSubview(smileArrowImageView)
    }
}

extension ViewController: MDCSwipeToChooseDelegate {
    
    // This is called when a user didn't fully swipe left or right.
    func viewDidCancelSwipe(_ view: UIView) -> Void{
        print("Couldn't decide, huh?")
    }
    
    // Sent before a choice is made. Cancel the choice by returning `false`. Otherwise return `true`.
    func view(_ view: UIView, shouldBeChosenWith: MDCSwipeDirection) -> Bool {
        if shouldBeChosenWith == .left {
            return true
        } else {
            // Snap the view back and cancel the choice.
            UIView.animate(withDuration: 0.16, animations: { () -> Void in
                view.transform = CGAffineTransform.identity
                view.center = view.superview!.center
            })
            return false
        }
    }
    
    
    // This is called when a user swipes the view fully left or right.
    func view(_ view: UIView, wasChosenWith: MDCSwipeDirection) -> Void {
        if wasChosenWith == .left {
            print("Photo deleted!")
        } else {
            print("Photo saved!")
        }
    }
}



