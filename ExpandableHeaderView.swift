//
//  ExpandableHeaderView.swift
//  FairField
//
//  Created by Prashant Shinde on 7/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

protocol ExpandableHeaderViewDelegate {
    func toggleSection(header : ExpandableHeaderView , section : Int )
}
class ExpandableHeaderView: UITableViewHeaderFooterView {
    var delegate : ExpandableHeaderViewDelegate?
    var section : Int!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.addGestureRecognizer(UITapGestureRecognizer(target : self , action : #selector(selectHeaderAction)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func selectHeaderAction(gestureRecognizer : UITapGestureRecognizer){
    let cell = gestureRecognizer.view as! ExpandableHeaderView
        delegate?.toggleSection(header: self, section: cell.section)
    }
    
    func costumInit (title : String , section : Int , delegate : ExpandableHeaderView){
    self.textLabel?.text = title
        self.section = section
    self.delegate = delegate as? ExpandableHeaderViewDelegate
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.textLabel?.textColor = UIColor.white
        self.contentView.backgroundColor = UIColor.darkGray
    }

}
