//
//  LocalEvViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class LocalEvViewController: UIViewController , UITextViewDelegate , UIImagePickerControllerDelegate ,  UINavigationControllerDelegate{
    
    @IBOutlet var bgImage: UIImageView!
    @IBOutlet var titleText: UILabel!
    @IBOutlet var descText: UILabel!
    @IBOutlet var addressText: UILabel!
    @IBOutlet var contactText: UILabel!
    @IBOutlet var likeBtn: KGHighLightedButton!
    @IBOutlet var postBtn: UIButton!
    @IBOutlet var commentTxt: UITextView!
    @IBOutlet var disLikeBtn: KGHighLightedButton!
    @IBOutlet var gallary: UIButton!
    @IBOutlet var camera: UIButton!
    @IBOutlet var eventImgCollectionVw: UICollectionView!
    @IBOutlet var commentTableViewCell: UITableView!
    @IBOutlet weak var commentHeightConst: NSLayoutConstraint!
    
    var eventDetail : NSDictionary = [:]
    var eventDetailOne : NSDictionary = [:]
    var Check1 : Bool = false
    var Check2 : Bool = false
    var splitedStr : [String] = []
    var s1 : String = ""
    var likeWhite = UIImage(named: "Like White.png") as UIImage?
    var likeBlue = UIImage(named: "Like Blue.png")as UIImage?
    var unlikeWhite = UIImage(named: "DisLike White.png") as UIImage?
    var unlikeBlue = UIImage(named: "DisLike Blue.png")as UIImage?
    var picker : UIImagePickerController? = UIImagePickerController()
    var newFrame : CGRect!
    var imageArray : [UIImage] = []
    var commentDict : [NSDictionary] = []
    var event_id : String = ""
    var user_id : String = ""
    var userDef : UserDefaults!
    let lab2 =  UILabel.init()
    
    var dataArray : [NSData] = []
    @IBOutlet weak var imageViewForAds: UIImageView!
    
    var advertisementArr : [NSDictionary] = []
    var scrollingTimer = Timer()
    var ownerId : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commentTxt.layer.borderWidth = 1.0
        commentTxt.layer.borderColor =  UIColor.init(red: 35/255.0, green: 118/255.0, blue: 188/255.0, alpha: 1.0).cgColor
        commentTxt.delegate = self
        print("eventDetail = \(eventDetailOne)")
        
        if s1 == "EventOne"
        {
            self.addressText.text! = (eventDetailOne["address"] as? String)!
            self.titleText.text! = (eventDetailOne["title"] as? String)!
            self.descText.text! = (eventDetailOne["description"] as? String)!
            event_id = (eventDetailOne["id"] as? String)!
            print("event_id === \(event_id)")
            let imageString1 = eventDetailOne["image"] as! String
            //print("imageString = \(imageString1)")
            bgImage.sd_setImage(with: URL(string : "\(Constant.IMAGE_URL_NEW)events/\(imageString1)"))
                print("img: \(Constant.IMAGE_URL_NEW)events/\(imageString1)")
            let separators1 = CharacterSet(charactersIn: ",")
            let divLine1 = (eventDetailOne["contact"] as! String)
            self.splitedStr = divLine1.components(separatedBy: separators1)
            self.contactText.text = divLine1.replacingOccurrences(of: ",", with: "\n")
            
            self.userDef = UserDefaults.standard
            let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
            self.user_id = "\(data1["user_id"] as! String)"
            
            self.showComment()
            
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            self.eventImgCollectionVw.collectionViewLayout = layout
            
        }
        else
        {
            self.addressText.text! = (eventDetail["address"] as? String)!
            self.titleText.text! = (eventDetail["title"] as? String)!
            self.descText.text! = (eventDetail["description"] as? String)!
            event_id = (eventDetail["id"] as? String)!
            print("event_id === \(event_id)")
            let imageString1 = eventDetail["image"] as! String
            //print("imageString = \(imageString1)")
            bgImage.sd_setImage(with: URL(string : "\(Constant.IMAGE_URL_NEW)events/\(imageString1)"))
            
            
            print("img: \(Constant.IMAGE_URL_NEW)events/\(imageString1)")
            let separators1 = CharacterSet(charactersIn: ",")
            let divLine1 = (eventDetail["contact"] as! String)
            self.splitedStr = divLine1.components(separatedBy: separators1)
            self.contactText.text = divLine1.replacingOccurrences(of: ",", with: "\n")
            
            
            
            self.userDef = UserDefaults.standard
            let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
            self.user_id = "\(data1["user_id"] as! String)"
            
            self.showComment()
            
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            self.eventImgCollectionVw.collectionViewLayout = layout
        }
        //        commentTxt.text = "Write Something"
        //        commentTxt.textColor = UIColor.lightGray
        
        DispatchQueue.main.async {
            self.showAdvertisement()
        }
        addGestureToAdsImageView()
        
    }
    
    func addGestureToAdsImageView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(adsImageViewAction))
        self.imageViewForAds.isUserInteractionEnabled = true
        self.imageViewForAds.tag = -1
        self.imageViewForAds.addGestureRecognizer(tapGesture)
    }
    
    private var adsImageIndex = 0
    
    @objc func startTimer(theTimer : Timer){
        //        UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseOut, animations: {
        //            self.addCollectionViewThree.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int , section:0), at: .centeredHorizontally, animated: false)
        //        }, completion: nil)
        setupForAdsImages()
    }
    
    func stopTimerTest() {
        
        scrollingTimer.invalidate()
    }
    
    
    func setupForAdsImages() {
        if adsImageIndex >= advertisementArr.count {
            adsImageIndex = 0
        }
        let dict1 = advertisementArr[adsImageIndex]
        adView(userid: self.user_id, bannerid: dict1.value(forKey: "id")! as! String, ownerid: dict1.value(forKey: "owner_id")! as! String)
        
        self.imageViewForAds.tag = adsImageIndex
        adsImageIndex = adsImageIndex + 1
        let imageString = dict1["image"] as! String
        // print("imageString = \(imageString)")
        imageViewForAds.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
        
        
    }
    
    @objc func adsImageViewAction(sender: UITapGestureRecognizer!) {
        print("adsImageView clicked \((sender.view?.tag)!)")
        if (sender.view?.tag)! != -1 {
            let dict2 = advertisementArr[(sender.view?.tag)!]
            adClick(userid: self.user_id, bannerid: dict2.value(forKey: "id")! as! String, ownerid: dict2.value(forKey: "owner_id")! as! String)
            self.ownerId = (dict2["owner_id"] as? String)!
            
        }
    }
    
    func adClick(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/banner_click", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                self.addDetailOne()
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    func adView(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/save_page_viewer", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    func showAdvertisement()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let pstring = ""
            
            ApiResponse.onResponsePostPhp(url: "Userajax/banners", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            self.advertisementArr = result["data"] as! [NSDictionary]
                            self.setupForAdsImages()
                            self.scrollingTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: nil, repeats: true)
                            self.scrollingTimer.fire()
                            //                            self.advertisementCollectionView.reloadData()
                        }
                    }
                    else{
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        // ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        //ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            // ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    func addDetailOne()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let pstring = "owner_id=\(self.ownerId)"
            
            ApiResponse.onResponsePostPhp(url: "Userajax/get_business_owner", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                
                                let dict5 = result["data"] as! NSDictionary
                                
                                let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                                dictProvider.providerdetailDict = dict5
                                self.present(dictProvider, animated: true, completion: nil)
                        }
                    }
                    else{
                        ApiResponse.alert(title: "", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200)
                    {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            ApiResponse.alert(title: "No internet connection", message: "Please check your internet", controller: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.newFrame = self.view.frame
        
        newFrame.origin.y = self.view.bounds.origin.y - 150
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                
                self.view.frame = self.newFrame
        })
    }
    
    private func textFieldDidEndEditing(_ textField: UITextField)
    {
        newFrame.origin.y = 0
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                self.view.frame = self.newFrame
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func onBackEvent(_ sender: Any) {
        stopTimerTest()
        self.dismiss(animated: false, completion: nil )
    }
    
    @IBAction func like(_ sender: Any) {
        //        if (Check1 == false)
        //        {
        //            likeBtn.setBackgroundImage(likeBlue, for: .normal)
        //            Check1 = true
        //            disLikeBtn.setBackgroundImage(unlikeWhite, for: .normal)
        //        }
        //        else
        //        {
        //
        //            likeBtn.setBackgroundImage(likeWhite, for: .normal)
        //            Check1 = false
        //        }
        
        likeBtn.setBackgroundImage(likeBlue, for: .normal)
        disLikeBtn.setBackgroundImage(unlikeWhite, for: .normal)
    }
    
    @IBAction func unLike(_ sender: Any) {
        
        //        if (Check2 == false)
        //        {
        //            disLikeBtn.setBackgroundImage(unlikeBlue, for: .normal)
        //            Check2 = true
        //            likeBtn.setBackgroundImage(likeWhite, for: .normal)
        //        }
        //        else {
        //
        //            disLikeBtn.setBackgroundImage(unlikeWhite, for: .normal)
        //            Check2 = false
        //        }
        
        disLikeBtn.setBackgroundImage(unlikeBlue, for: .normal)
        likeBtn.setBackgroundImage(likeWhite, for: .normal)
    }
    
    @IBAction func cameraBtn(_ sender: Any) {
        
        picker?.delegate = self
        self.openCamera()
        
    }
    
    @IBAction func gallaryBtn(_ sender: Any) {
        
        picker?.delegate = self
        self.openGallary()
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            self.present(picker!, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(picker!, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        var tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        tempImage = ApiResponse.resize(tempImage!, maxHt: 400, maxWd: 300)
        imageArray.append(tempImage!)
        picker.dismiss(animated: true, completion: nil)
        self.eventImgCollectionVw.reloadData()
    }
    
    func postComment(){
        
        let newUrl = Constant.BASE_URL+"Userajax/add_comment"
        
        let url = NSURL(string: newUrl)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var body = Data()
        let fname = "test.png"
        let mimetype = "image/png"
        let parameters : [String : String] = ["event_id" : "\(self.event_id)" , "comment" : "\(commentTxt.text!)" , "user_id" : "\(self.user_id)"]
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        var i : Int = 0
        for imgg in imageArray {
            let image_data = UIImagePNGRepresentation(imgg)
            if(image_data == nil)
            {
                return
            }
            //define the data post parameter
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"test\(i)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("hi\r\n".data(using: String.Encoding.utf8)!)
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"images[]\"; filename=\"\(fname)\(i)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(image_data!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            i = i + 1
        }
        
        request.httpBody = body as Data
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            DispatchQueue.main.async {
                guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                    print("error")
                    return
                }
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("datataattaatat = \(dataString!)")
                LoadingIndicatorView.hide()
                ApiResponse.alert(title: "", message: "Comments successfully." , controller: self)
            }
        }
        task.resume()
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    @IBAction func postAction(_ sender: Any) {
        LoadingIndicatorView.show()
        self.postComment()
        self.commentTxt.text = ""
        
        likeBtn.setBackgroundImage(likeWhite, for: .normal)
        disLikeBtn.setBackgroundImage(unlikeWhite, for: .normal)
        imageArray = []
        eventImgCollectionVw.reloadData()
    }
    
    func showComment(){
        
        let params = "event_id=\(self.event_id)"
        ApiResponse.onResponsePostPhp(url: "Userajax/show_comments", parms: params, completion: { (result, error) in
            if (error == "")
            {
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                print("result = \(result)")
                if (status == true) {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            self.commentDict  = result["data"] as! [NSDictionary]
                            if(self.commentDict.count != 0) {
                                self.commentHeightConst.constant = 121
                                self.commentTableViewCell.reloadData()
                            }
                    }
                }
                else {
                    OperationQueue.main.addOperation
                        {
                            self.commentHeightConst.constant = 0
                    }
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
        
    }
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}// extension for impage uploading

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

// File Type ----------

extension LocalEvViewController : UICollectionViewDelegate , UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : EventImgCollectionViewCell = eventImgCollectionVw.dequeueReusableCell(withReuseIdentifier: "imagecv12", for: indexPath) as! EventImgCollectionViewCell
        cell.eventImg.image = imageArray[indexPath.row]
        
        return cell
    }
}

class EventImgCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet var eventImg: UIImageView!
}

class CommentTabelViewCell : UITableViewCell
{
    @IBOutlet var userName: UILabel!
    @IBOutlet weak var imgBtn1: KGHighLightedButton!
    @IBOutlet var userComment: UILabel!
    
    
}
class ShowMoreCell : UITableViewCell
{
    @IBOutlet var showBtn: KGHighLightedButton!
    
}

extension LocalEvViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(section == 0) {
            if (commentDict.count == 0) {
                
                return 0
            }
            else {
                return 1
            }
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0) {
            
            let cell : CommentTabelViewCell = commentTableViewCell.dequeueReusableCell(withIdentifier: "commenttabelviewcell", for: indexPath) as! CommentTabelViewCell
            let dict1 = commentDict[indexPath.row]
            print("dict1 == \(dict1)")
            cell.userName.text! = (dict1["user_name"]as? String)!
            cell.userComment.text! = (dict1["comment"]as? String)!
            let imageArr = dict1["images"] as! [String]
            if (imageArr.count != 0) {
                cell.imgBtn1.addTarget(self, action: #selector(imagedetail1(_:)), for: .touchUpInside)
                
                
            }
            return cell
        }
        else {
            let cell : ShowMoreCell = commentTableViewCell.dequeueReusableCell(withIdentifier: "showmorecell") as! ShowMoreCell
            
            if (commentDict.count != 0) {
                
                cell.showBtn.addTarget(self, action: #selector(showMore(_:)), for: .touchUpInside)
            }
            
            return cell
        }
        
    }
    
    @objc func showMore(_ sender: UIButton!)
    {
        let commentDict1 = self.storyboard?.instantiateViewController(withIdentifier: "commentoneviewcontroller") as! CommentOneViewController
        // commentDict1.commentOneArr = commentDict
        commentDict1.eventid = event_id
        self.present(commentDict1, animated: true, completion: nil)
    }
    
    
    @objc func imagedetail1(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:commentTableViewCell)
        let indexPath1 = commentTableViewCell.indexPathForRow(at: point)
        let dictNoti1 = self.commentDict[(indexPath1?.item)!]
        let showImage = self.storyboard?.instantiateViewController(withIdentifier: "imageviewcontroller") as! ImageViewController
        showImage.commentDict = dictNoti1
        showImage.imgStr = "imgStr3"
        self.present(showImage, animated: true, completion: nil)
    }
    
}




