//
//  DashboardViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseMessaging
import UserNotifications
import Alamofire

class DashboardViewController: UIViewController , NormalPopUpDeligate  {
    func openPopUp(title: String, body: String, imagelink: String) {
        openPopUpDialoge(title: title,body: body,imagelink: imagelink)
    }
    
    
    @IBOutlet var listTable: UITableView!
     var userDef : UserDefaults!
    var listArray : [String] = []
    var imageArray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //   let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //   let helloWorldTimer = Timer.scheduledTimer(timeInterval: 120.0, target: self, selector: #selector(appDelegate.locationService), userInfo: nil, repeats: true)
        
        listTable.separatorStyle = .none
        listTable.layer.borderColor = UIColor.black.cgColor
        listTable.layer.borderWidth = 1.0
        listArray = ["Saved Events", "Saved Coupons",   "Edit Profile", "Change Password" , "Push Notifications" , "Logout"]
        imageArray = ["Saved Events 01 (5).png", "Saved Coupons 01 (5).png",  "Edit Profile (5).png", "Change Password (5).png" , "Notification (5).png" , "logout.png"]
        
        
        
        checkNotificationPermission()
        
        
//        NotificationCenter.default.addObserver(self,selector: #selector(notificationNewBusiness(notification:)),name: NSNotification.Name(rawValue: "new_business"),object: nil)
        
    }
    
    
    
 
    
    
    func checkNotificationPermission(){
        
        var returnValue: Bool = UserDefaults.standard.bool(forKey: Constant.PREVIOUS_USER)
        
        print("previous user  \(returnValue)")
        
        if(!returnValue){
            let current = UNUserNotificationCenter.current()
            
            current.getNotificationSettings(completionHandler: { (settings) in
                
           //     print("status : \(settings.authorizationStatus)")
                if settings.authorizationStatus == .notDetermined {
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_NEW)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_EVENT)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REWARD)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_BUSINESS)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_ANNOUNCMENT)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_REQUEST)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_COUPON)
                    
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_EVENT)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REWARD)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_BUSINESS)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_NEW)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_COUPON)
                    UserDefaults.standard.synchronize()
                    
                }
                
                if settings.authorizationStatus == .denied {
                    
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_NEW)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_EVENT)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REWARD)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_BUSINESS)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_ANNOUNCMENT)
                    Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_REQUEST)
                     Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_COUPON)
                    
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_COUPON)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_EVENT)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REWARD)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_BUSINESS)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_NEW)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
                    UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
                    UserDefaults.standard.synchronize()
                }
                
                if settings.authorizationStatus == .authorized {
                    
                    if Messaging.messaging().fcmToken != nil {
                    UserDefaults.standard.set(true, forKey: Constant.PREVIOUS_USER)
             
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_NEW)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_EVENT)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REWARD)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_BUSINESS)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_ANNOUNCMENT)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_REQUEST)
                    Messaging.messaging().subscribe(toTopic: Constant.TOPICS_COUPON)
                        
                    
                    
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_COUPON)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_EVENT)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REWARD)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_BUSINESS)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_NEW)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
                    UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
                    UserDefaults.standard.synchronize()
                    }else{
                        self.checkNotificationPermission()
                    }
                }
            })
        }
        
        
    }
    
    
    func openPopUpDialoge(title: String, body : String , imagelink : String){
        
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUp") as! PopUpViewController
        popOverVC.titleText = title
        popOverVC.body = body
        popOverVC.imagelink = imagelink
        
        if(!popOverVC.isViewLoaded){
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
        
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        listTable.isHidden = true
        
        notificationCheck()
        
    }
    
    @IBAction func openList(_ sender: Any) {
        
        if listTable.isHidden {
            listTable.isHidden = false
        } else {
            listTable.isHidden = true
        }
    }
    
    @IBAction func openImportant(_ sender: Any) {
        
        let imp = self.storyboard?.instantiateViewController(withIdentifier: "important") as! ImportantViewController
        self.present(imp, animated: true, completion: nil)
    }
    
    @IBAction func openBusiness(_ sender: Any) {
        
        let business = self.storyboard?.instantiateViewController(withIdentifier: "business") as! BusinessViewController
        self.present(business, animated: true, completion: nil)
        
    }
    
    @IBAction func openLocalEvents(_ sender: Any) {
        
        let local = self.storyboard?.instantiateViewController(withIdentifier: "localevents") as! LocalEventsVC
        self.present(local, animated: true, completion: nil)
    }
    
    @IBAction func openPromotionSwipe(_ sender: Any) {
        
        let promo = self.storyboard?.instantiateViewController(withIdentifier: "promotion") as! PromotionViewController
        self.present(promo, animated: true, completion: nil)
    }
    
    @IBAction func openFbGroup(_ sender: Any) {
        
        let fbgroup = self.storyboard?.instantiateViewController(withIdentifier: "facebookvc") as! FaceBookVC
        self.present(fbgroup, animated: true, completion: nil)
    }
    
    @IBAction func openNewsFeed(_ sender: Any) {
        
        let feed = self.storyboard?.instantiateViewController(withIdentifier: "newsfeed") as! NewsFeedVC
        feed.searchone = "searchData1"
        self.present(feed, animated: true, completion: nil)
        
    }
    
    @IBAction func supportBtn(_ sender: Any) {
        let support = self.storyboard?.instantiateViewController(withIdentifier: "supportviewcontroller") as! SupportViewController
        self.present(support, animated: true, completion: nil)
    }
    
    
    @IBAction func notificationBtn(_ sender: Any) {
        let notification = self.storyboard?.instantiateViewController(withIdentifier: "notification") as! NotificationViewController
        self.present(notification, animated: true, completion: nil)
    }
    
    @IBAction func openForum(_ sender: Any) {
        let rewards = self.storyboard?.instantiateViewController(withIdentifier: "rewardsviewcontroller") as! RewardsViewController
        self.present(rewards, animated: true, completion: nil)
        
    }
    
    @IBAction func openCrimeNews(_ sender: Any) {
        let crime = self.storyboard?.instantiateViewController(withIdentifier: "crimenewsvc") as! CrimeNewsVC
        self.present(crime, animated: true, completion: nil)
        
    }
    
    @IBAction func shareBtn(_ sender: Any) {
        
        let activityVC = UIActivityViewController(activityItems: ["https://itunes.apple.com/us/app/cfm-fixer/id1232203486?mt=8"] , applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func searchBtn(_ sender: Any) {
        let feed1 = self.storyboard?.instantiateViewController(withIdentifier: "newsfeed") as! NewsFeedVC
        feed1.searchone = "searchData"
        self.present(feed1, animated: true, completion: nil)
    }
    
}

extension DashboardViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = listTable.dequeueReusableCell(withIdentifier: "listcell")!
        cell.textLabel?.text = listArray[indexPath.row]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.imageView?.image = UIImage(named:imageArray[indexPath.row])
        let itemSize = CGSize(width : 15, height : 15);
        UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
        let imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
        cell.imageView?.image?.draw(in: imageRect)
        cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return cell
    }
    
    
    
    func notificationCheck()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
        
            
            let URL = Constant.BASE_URL + "Userajax/check_announcement";
            
            self.userDef = UserDefaults.standard
            let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
             var user_id = "\(data1["user_id"] as! String)"
            
            
           // user_id = "89"
            print(user_id)
            
            let params : [String : String] = ["user_id" : "\(user_id)"]
              Alamofire.request(URL,method:.post,parameters:params).responseJSON {
              
                response in
                if response.result.isSuccess{
                     let result = response.value as! NSDictionary
                    
                    print(result)
     
              
           
                    if  let type = result["type"]{

                  
                            let title = result["title"] as! String
                            let body = result["body"] as! String
                            let image = result["image"] as! String

                     OperationQueue.main.addOperation
                            {
                                self.openPopUp(title: title, body: body, imagelink: image)
                        }
                    }
                  
                 

 
                }
                else {
                  //  ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
            }
        }
        else {
           // ApiResponse.alert(title: "No internet connection", message: "Please check your internet", controller: self)
        }
    }
    
    func stringToJson(notificationString : String){
        
        let data = notificationString.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
                
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row
        {
        case 0:
            let events = self.storyboard?.instantiateViewController(withIdentifier: "savedevents") as! SavedEventsVC
            self.present(events, animated: true, completion: nil)
            break
            
        case 1:
            let coupon = self.storyboard?.instantiateViewController(withIdentifier: "savedcoupons") as! SavedCouponsVC
            self.present(coupon, animated: true, completion: nil)
            break
            
        case 2:
            let edit = self.storyboard?.instantiateViewController(withIdentifier: "edit") as! EditProfileVC
            self.present(edit, animated: true, completion: nil)
            break
            
        case 3:
            let change = self.storyboard?.instantiateViewController(withIdentifier: "change") as! ChangePasswordVC
            self.present(change, animated: true, completion: nil)
            break
            
        case 4:
            let setting = self.storyboard?.instantiateViewController(withIdentifier: "settingviewcontroller") as! SettingViewController
            self.present(setting, animated: true, completion: nil)
            break
            
            
        case 5:
            let alert = UIAlertController(title: "Logout", message: "Are you sure, you want to logout", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
                action in
                
                let user = UserDefaults.standard
                user.set("", forKey: "session")
                user.synchronize()
                
                if(user.value(forKey: "loginway") != nil){
                    let loginway = user.value(forKey: "loginway") as! String
                    if (loginway == "facebook") {
                        FBSDKLoginManager().logOut()
                        FBSDKAccessToken.setCurrent(nil)
                    }
                }
                
                let data1 = user.value(forKey: "userInfo") as! NSDictionary
                let user_id = "\(data1["user_id"] as! String)"
                Messaging.messaging().unsubscribe(fromTopic: user_id)
                
                let viewVc = self.storyboard?.instantiateViewController(withIdentifier: "view") as! ViewController
                self.present(viewVc, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.destructive, handler:nil))
            self.present(alert, animated: true, completion: nil)
            
            break
            
        default :
            break
        }
        listTable.isHidden = true
    }
}

