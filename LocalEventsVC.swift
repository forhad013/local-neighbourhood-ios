//
//  LocalEventsVC.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SDWebImage

class LocalEventsVC: UIViewController {
    
    
    @IBOutlet var localCollectionView: UICollectionView!
    @IBOutlet weak var addCollectionViewThree: UICollectionView!
    @IBOutlet weak var eventLabel: UILabel!
    @IBOutlet weak var imageViewForAdvertisement: UIImageView!
    
    var user_id : String = ""
    var userDef : UserDefaults!
    var eventDict : [NSDictionary] = []
    var Check : Bool = false
    var eventId : String!
    var BoxOn = UIImage(named: "dislike.png") as UIImage?
    var BoxOff = UIImage(named: "like.png")as UIImage?
    var advertisementArr3 : [NSDictionary] = []
    var scrollingTimer3 = Timer()
    var ownerId4 : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"
             self.getLocal()
             self.showAdvertisement3()
        addGestureToAdsImageView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        eventLabel.isHidden = true
    }
    
    func adClick(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/banner_click", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                self.addDetailFour()
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    func adView(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/save_page_viewer", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    
    func showAdvertisement3(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = ""
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/banners", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss = \(result)")
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            self.advertisementArr3 = result["data"] as! [NSDictionary]
                            if self.advertisementArr3.count != 0
                            {
//                            self.addCollectionViewThree.reloadData()
                                self.setupForAdsImages()
                                self.scrollingTimer3 = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(LocalEventsVC.startTimer(theTimer:)), userInfo: nil, repeats: true)
                                self.scrollingTimer3.fire()
                            }
                            else
                            {
                            
                            }
                            
                            LoadingIndicatorView.hide()
                            // ApiResponse.alert(title: "OK", message: message, controller: self)
                        }
                    }
                    else
                    {
                        self.addCollectionViewThree.isHidden = true
                        self.eventLabel.isHidden = false
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else
                    {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    func addGestureToAdsImageView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(adsImageViewAction))
        self.imageViewForAdvertisement.isUserInteractionEnabled = true
        self.imageViewForAdvertisement.tag = -1
        self.imageViewForAdvertisement.addGestureRecognizer(tapGesture)
    }
    
    private var adsImageIndex = 0
    
    @objc func startTimer(theTimer : Timer){
//        UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseOut, animations: {
//            self.addCollectionViewThree.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int , section:0), at: .centeredHorizontally, animated: false)
//        }, completion: nil)
        setupForAdsImages()
    }
    
    func stopTimerTest() {
        scrollingTimer3.invalidate()
    }
    
    func setupForAdsImages() {
        if adsImageIndex >= advertisementArr3.count {
            adsImageIndex = 0
        }
        let dict1 = advertisementArr3[adsImageIndex]
        adView(userid: self.user_id, bannerid: dict1.value(forKey: "id")! as! String, ownerid: dict1.value(forKey: "owner_id")! as! String)
        self.imageViewForAdvertisement.tag = adsImageIndex
        adsImageIndex = adsImageIndex + 1
        let imageString = dict1["image"] as! String
        //print("imageString = \(imageString)")
        imageViewForAdvertisement.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
        
        
    }
    @objc  
    func adsImageViewAction(sender: UITapGestureRecognizer!) {
        print("adsImageView clicked \((sender.view?.tag)!)")
        if (sender.view?.tag)! != -1 {
            let dict2 = advertisementArr3[(sender.view?.tag)!]
            self.ownerId4 = (dict2["owner_id"] as? String)!
            adClick(userid: self.user_id, bannerid: dict2.value(forKey: "id")! as! String, ownerid: dict2.value(forKey: "owner_id")! as! String)
            print("ownerId ======******\(ownerId4)")
            
        }
    }
    
     func getLocal(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = "user_id=\(user_id)"
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/show_local_event", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    print("ressassss = \(result)")
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            self.eventDict = result["data"] as! [NSDictionary]
                            if (self.eventDict.count != 0) {
                                self.eventLabel.isHidden = true
                                self.localCollectionView.isHidden = false
                                self.localCollectionView.reloadData()
                            }
                            else {
                               self.eventLabel.isHidden = false
                               self.localCollectionView.isHidden = true
                            }
                            
                            LoadingIndicatorView.hide()
                            // ApiResponse.alert(title: "OK", message: message, controller: self)
                        }
                        
                    }
                    else{
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                        
                    }
                    
                }
                else{
                    if(error == Constant.Status_Not_200){
                         LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                         LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        stopTimerTest()
        self.dismiss(animated: false, completion: nil)
        let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
        
        self.present(dashboard, animated: true, completion: nil)
    }
    
    
    
}

extension LocalEventsVC : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == localCollectionView
        {
         return self.eventDict.count
        }
        else
        {
        return advertisementArr3.count
        }
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == localCollectionView
        {
            let cell : LocalCollectionViewCell = localCollectionView.dequeueReusableCell(withReuseIdentifier: "localcell", for: indexPath) as! LocalCollectionViewCell
            
            let localDict = eventDict[indexPath.item]
            
            
            
            cell.titleLabel.text = localDict["title"] as? String
            let imageString = localDict["image"] as! String
            //print("imageString = \(imageString)")
            
            let urlString = "\(Constant.IMAGE_URL_NEW)events/\(imageString)"
            cell.bgimage.sd_setImage(with: URL(string :urlString))
                            print("img event: \(urlString)")
            print(localDict["event_date"] as? String)
            cell.dateLabel.text = localDict["event_date"] as? String
            cell.locationLabel.text = localDict["address"] as? String
            cell.likeBtn.addTarget(self, action: #selector(likeEvent(_:)), for: .touchUpInside)
            cell.intrestedLabel.text = localDict["interested"] as? String
            
            return cell

        }
        else {
            
            let cell : AddCellThree = addCollectionViewThree.dequeueReusableCell(withReuseIdentifier: "addcellthree", for: indexPath) as! AddCellThree
            
            let dict1 = advertisementArr3[indexPath.item]
            let imageString = dict1["image"] as! String
            //print("imageString = https://www.localneighborhoodapp.com/images/banner/\(imageString)")
            cell.addImg3.sd_setImage(with: URL(string : "\(Constant.IMAGE_URL_NEW)banner/\(imageString)"))
            var rowIndex = indexPath.item
            let numberofRecords : Int = self.advertisementArr3.count - 1
            if (rowIndex < numberofRecords)
            {
                rowIndex = (rowIndex + 1)
            }
            else
            {
                rowIndex = 0
                
            }
            
            return cell
            
        }
        
           }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == localCollectionView {
            let event2 = eventDict[indexPath.item]
            let events = self.storyboard?.instantiateViewController(withIdentifier: "events2") as! LocalEvViewController
            events.eventDetail = event2
            events.s1 = "Event"
            self.present(events, animated: true, completion: nil)

        }
        else {
            let dict2 = advertisementArr3[indexPath.row]
            self.ownerId4 = (dict2["owner_id"] as? String)!
            print("ownerId ======******\(ownerId4)")
            self.addDetailFour()

        }
        
           }
    
    func addDetailFour(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = "owner_id=\(self.ownerId4)"
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/get_business_owner", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss = \(result)")
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            let dict5 = result["data"] as! NSDictionary
                            
                            let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                            dictProvider.providerdetailDict = dict5
                            self.present(dictProvider, animated: true, completion: nil)
                            
                            
                            LoadingIndicatorView.hide()
                            // ApiResponse.alert(title: "OK", message: message, controller: self)
                        }
                        
                    }
                    else{
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                        
                    }
                    
                }
                else{
                    if(error == Constant.Status_Not_200){
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    
    @objc func likeEvent(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:localCollectionView)
        let indexPath1 = localCollectionView.indexPathForItem(at: point)
        
        let dict = eventDict[(indexPath1?.row)!]
        let eventidddd = dict["id"] as! String
        
        if (Check == false)
        {
             sender.setBackgroundImage(BoxOff, for: .normal)
            Check = true
            self.likeAndDislike(1, eventid: eventidddd)
        }
        else {
            
            sender.setBackgroundImage(BoxOn, for: .normal)
            Check = false
            self.likeAndDislike(0, eventid: eventidddd)
        }
    }
    
    func likeAndDislike(_ likeValue : Int, eventid : String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            // LoadingIndicatorView.show("Loading....")
            
            
            let pstring = "event_id=\(eventid)&event_like=\(likeValue)&user_id=\(user_id)"
           
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/eventlikes_by_user", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    print("ressassss = \(result)")
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            // LoadingIndicatorView.hide()
                            
                            //self.localCollectionView.reloadData()
                            //ApiResponse.alert(title: "", message: message, controller: self)
                            
                            
                        }
                        
                    }
                    else{
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                    
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
       
}

class LocalCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet var likeBtn: UIButton!
    @IBOutlet var bgimage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var intrestedLabel: UILabel!
    
}

class AddCellThree : UICollectionViewCell {

    @IBOutlet weak var addImg3: UIImageView!

}



