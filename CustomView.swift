//
//  CustomView.swift
//  Yomuzoku
//
//  Created by Prashant Shinde on 5/30/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
    
@IBDesignable
open class KGHighLightedField: UITextField {
    
    @IBInspectable
     public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
     public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
     public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
//    @IBInspectable var paddingLeft: CGFloat = 0
//    @IBInspectable var paddingRight: CGFloat = 0
//    
//    override open func textRect(forBounds : CGRect) -> CGRect {
//        print("rectttt = \(CGRect(x : bounds.origin.x + paddingLeft, y : bounds.origin.y, width : bounds.size.width - paddingLeft - paddingRight, height : bounds.size.height))")
//        return CGRect(x : bounds.origin.x + paddingLeft, y : bounds.origin.y, width : bounds.size.width - paddingLeft - paddingRight, height : bounds.size.height)
//    }
//    
//    override open func editingRect(forBounds : CGRect) -> CGRect {
//        return textRect(forBounds: bounds)
//    }
    
    @IBInspectable var placeHolderColor : UIColor = UIColor.lightGray{
        didSet {
            setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
        }
    }
    
}

@IBDesignable
open class KGHighLightedButton: UIButton {
    
//    @IBInspectable
//    public var borderColor: UIColor = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor.cgColor
//        }
//    }
//    
//    @IBInspectable
//    public var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}

@IBDesignable
open class CustomUIView: UIView {
    
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}


