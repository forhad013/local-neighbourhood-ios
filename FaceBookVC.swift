//
//  FaceBookVC.swift
//  FairField
//
//  Created by Prashant Shinde on 9/12/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class FaceBookVC: UIViewController, UIWebViewDelegate {

    
    @IBOutlet weak var fbWebView: UIWebView!
    @IBOutlet weak var imageViewForAds: UIImageView!
    
    var advertisementArr : [NSDictionary] = []
    var scrollingTimer = Timer()
    var ownerId : String = ""
    var userid: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dic = UserDefaults.standard.value(forKey: "userInfo") as! NSDictionary
        userid = dic.value(forKey: "user_id")! as! String


        fbWebView.delegate = self
        
        let url = NSURL (string: "https://www.facebook.com/groups/20947417401/");
        let request = NSURLRequest(url: url! as URL);
        fbWebView.loadRequest(request as URLRequest);
        
        DispatchQueue.main.async {
            self.showAdvertisement()
        }
        addGestureToAdsImageView()
    }
    
    func addGestureToAdsImageView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(adsImageViewAction))
        self.imageViewForAds.isUserInteractionEnabled = true
        self.imageViewForAds.tag = -1
        self.imageViewForAds.addGestureRecognizer(tapGesture)
    }
    
    private var adsImageIndex = 0
    
    @objc func startTimer(theTimer : Timer){
        //        UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseOut, animations: {
        //            self.addCollectionViewThree.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int , section:0), at: .centeredHorizontally, animated: false)
        //        }, completion: nil)
        setupForAdsImages()
    }
    
    func stopTimerTest() {
        
        scrollingTimer.invalidate()
    }
    
    func setupForAdsImages() {
        if adsImageIndex >= advertisementArr.count {
            adsImageIndex = 0
        }
        let dict1 = advertisementArr[adsImageIndex]
        adView(userid: userid, bannerid: dict1.value(forKey: "id")! as! String, ownerid: dict1.value(forKey: "owner_id")! as! String)
        self.imageViewForAds.tag = adsImageIndex
        adsImageIndex = adsImageIndex + 1
        let imageString = dict1["image"] as! String
        //print("imageString = \(imageString)")
        imageViewForAds.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
        
        
    }
    
    @objc func adsImageViewAction(sender: UITapGestureRecognizer!) {
        print("adsImageView clicked \((sender.view?.tag)!)")
        if (sender.view?.tag)! != -1 {
            let dict2 = advertisementArr[(sender.view?.tag)!]
            self.ownerId = (dict2["owner_id"] as? String)!
            adClick(userid: userid, bannerid: dict2.value(forKey: "id")! as! String, ownerid: dict2.value(forKey: "owner_id")! as! String)
        }
    }
    
    func adClick(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/banner_click", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                self.addDetailOne()

                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    func adView(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/save_page_viewer", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    
    func showAdvertisement()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let pstring = ""
            
            ApiResponse.onResponsePostPhp(url: "Userajax/banners", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            self.advertisementArr = result["data"] as! [NSDictionary]
                            self.setupForAdsImages()
                            self.scrollingTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: nil, repeats: true)
                            self.scrollingTimer.fire()
                            //                            self.advertisementCollectionView.reloadData()
                        }
                    }
                    else{
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        // ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        //ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            // ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    func addDetailOne()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let pstring = "owner_id=\(self.ownerId)"
            
            ApiResponse.onResponsePostPhp(url: "Userajax/get_business_owner", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                
                                let dict5 = result["data"] as! NSDictionary
                                
                                let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                                dictProvider.providerdetailDict = dict5
                                self.present(dictProvider, animated: true, completion: nil)
                        }
                    }
                    else{
                        ApiResponse.alert(title: "", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200)
                    {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            ApiResponse.alert(title: "No internet connection", message: "Please check your internet", controller: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    /// ** WEB VIEW DELEGATE ** ///
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingIndicatorView.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingIndicatorView.hide()
    }
    
    @IBAction func backAction(_ sender: Any) {
        stopTimerTest()
        self.dismiss(animated: false, completion: nil)
    }

}
