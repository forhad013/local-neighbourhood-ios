//
//  ForgotPasswordVC.swift
//  FairField
//
//  Created by Prashant Shinde on 6/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController , UITextFieldDelegate{
    
    @IBOutlet var emailText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let textfield : [UITextField] = [  emailText]
        
        
        let placeholder = ["Email"]
        var i = 0
        
        for tf in textfield
        {
            tf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 5, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:placeholder[i],
                                                          attributes:[kCTForegroundColorAttributeName as NSAttributedStringKey: UIColor.white])
            i += 1
            
        }

        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSendEmail(_ sender: Any) {
        if(emailText.text == ""){
            ApiResponse.alert(title: "Empty", message: "Please enter Email", controller: self)
        }
        else if(ApiResponse.validateEmail(emailText.text!) == false){
            ApiResponse.alert(title: "Invalid Email", message: "Please enter valid email id", controller: self)
        }
        else if (reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN) {
            
            let params = "email=\(emailText.text!)"
            ApiResponse.onResponsePostPhp(url: "Userajax/forgot_password", parms: params, completion: { (result, error) in
                print("params = \(params)")
                
                if (error == "") {
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    print("result = \(result)")
                    
                    if (status == true) {
                        ApiResponse.alert(title: "Done", message: message, controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
            
        }
        
        
    }
    
    func getPassword(){}
    
    @IBAction func onBack(_ sender: Any) {
    }
    
}
