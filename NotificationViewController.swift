
import UIKit
import SwiftyJSON
import Alamofire
 


class NotificationViewController: UIViewController {
    
    @IBOutlet var notificationTable: UITableView!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var imageViewForAds: UIImageView!
    
    var notiDict : [NSDictionary] = []
    var user_id : String = ""
    var userDef : UserDefaults!
    var flagStr : String!
    var advertisementArr4 : [NSDictionary] = []
    var scrollingTimer4 = Timer()
    var ownerId5 : String = ""
    let swiftColor = UIColor.init(red: 8, green: 143, blue: 198)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationLabel.isHidden = true
        
        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"
        self.showAdvertisement3()
        addGestureToAdsImageView()
        self.showNotification()
        
        
        // Do any additional setup after loading the view.
    }
    
    func addGestureToAdsImageView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(adsImageViewAction))
        self.imageViewForAds.isUserInteractionEnabled = true
        self.imageViewForAds.tag = -1
        self.imageViewForAds.addGestureRecognizer(tapGesture)
    }
    
    private var adsImageIndex = 0
    
    @objc func startTimer(theTimer : Timer){
        //        UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseOut, animations: {
        //            self.addCollectionViewThree.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int , section:0), at: .centeredHorizontally, animated: false)
        //        }, completion: nil)
        setupForAdsImages()
    }
    func stopTimerTest() {
        scrollingTimer4.invalidate()
        
    }
    
    func setupForAdsImages() {
        if adsImageIndex >= advertisementArr4.count {
            adsImageIndex = 0
        }
        let dict1 = advertisementArr4[adsImageIndex]
        adView(userid: self.user_id, bannerid: dict1.value(forKey: "id")! as! String , ownerid: dict1.value(forKey: "owner_id")! as! String)
        self.imageViewForAds.tag = adsImageIndex
        adsImageIndex = adsImageIndex + 1
        let imageString = dict1["image"] as! String
        //print("imageString = \(imageString)")
        imageViewForAds.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
        
        
    }
    
    @objc func adsImageViewAction(sender: UITapGestureRecognizer!) {
        print("adsImageView clicked \((sender.view?.tag)!)")
        if (sender.view?.tag)! != -1 {
            let dict2 = advertisementArr4[(sender.view?.tag)!]
            self.ownerId5 = (dict2["owner_id"] as? String)!
            print("ownerId ======******\(ownerId5)")
            adClick(userid: self.user_id, bannerid: dict2.value(forKey: "id")! as! String, ownerid: dict2.value(forKey: "owner_id")! as! String)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBack(_ sender: Any) {
        stopTimerTest()
        self.dismiss(animated: false, completion: nil)
    }
    
    
    func showNotification(){
        
        let url = "\(Constant.BASE_URL)Userajax/all_notification"
       // LoadingIndicatorView.show()
        let params = "user_id=\(user_id)"
        
        
        
        ApiResponse.onResponsePostPhp(url: "Userajax/all_notification", parms: params, completion: { (result, error) in
            
            if (error == "") {
                
                //               let result = response.value as! NSDictionary
                
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                if (status == true) {
                    
                    print("Reviews result = \(result)")
                    
                    OperationQueue.main.addOperation {
                        LoadingIndicatorView.hide()
                        self.notiDict = result["data"] as! [NSDictionary]
                        self.notificationTable.reloadData()
                    }
                }
                else {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            self.notificationTable.isHidden = true
                            self.notificationLabel.isHidden = false
                    }
                    
                }
            }
            else {
                
                ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                
            }
 
        })
 
 
 
    
}
        
        func convertToJson(stringdata : String) -> JSON {
            
            
            
            if let jsonData = stringdata.data(using: .utf8) {
                if let json = try? JSON(data: jsonData) {
                    return json
                }
            }
            return nil
        }
        
        
        func adClick(userid: String, bannerid: String, ownerid:String)
        {
            if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
            {
                //LoadingIndicatorView.show()
                let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
                ApiResponse.onResponsePostPhp(url: "userajax/banner_click", parms: pstring, completion: {(result , error) in
                    if(error == "")
                    {
                        let status = result["status"]as! Bool
                        let message = result["message"]as! String
                        
                        if(status == true){
                            OperationQueue.main.addOperation
                                {
                                    
                                    self.addDetailFive()
                            }
                        }
                        else{
                            ApiResponse.alert(title: "Oops!", message: message, controller: self)
                        }
                    }
                    else{
                        if(error == Constant.Status_Not_200) {
                            ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                        }
                        else {
                            ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                        }
                    }
                })
            }
            else
            {
                ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
            }
        }
        
        func adView(userid: String, bannerid: String, ownerid:String)
        {
            if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
            {
                //LoadingIndicatorView.show()
                let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
                ApiResponse.onResponsePostPhp(url: "userajax/save_page_viewer", parms: pstring, completion: {(result , error) in
                    if(error == "")
                    {
                        let status = result["status"]as! Bool
                        let message = result["message"]as! String
                        
                        if(status == true){
                            OperationQueue.main.addOperation
                                {
                                    // self.importantArr = result["data"] as! [NSDictionary]
                                    //self.importantTable.reloadData()
                                    //  LoadingIndicatorView.hide()
                            }
                        }
                        else{
                            ApiResponse.alert(title: "Oops!", message: message, controller: self)
                        }
                    }
                    else{
                        if(error == Constant.Status_Not_200) {
                            ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                        }
                        else {
                            ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                        }
                    }
                })
            }
            else
            {
                ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
            }
        }
        
        
        
    }

        

    
    
 extension NotificationViewController {// : UICollectionViewDataSource , UICollectionViewDelegate {
    
    //    func numberOfSections(in collectionView: UICollectionView) -> Int {
    //        return 1
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //        return self.advertisementArr4.count
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    //        let cell : AdvertisementCellOne = advertisementCV.dequeueReusableCell(withReuseIdentifier: "advertisementcellone", for: indexPath) as! AdvertisementCellOne
    //        let dict1 = advertisementArr4[indexPath.item]
    //        let imageString = dict1["image"] as! String
    //        print("imageString = https://www.localneighborhoodapp.com/images/banner/\(imageString)")
    //        cell.addImageThree.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
    //        var rowIndex = indexPath.row
    //        let numberofRecords : Int = self.advertisementArr4.count - 1
    //        if (rowIndex < numberofRecords)
    //        {
    //            rowIndex = (rowIndex + 1)
    //        }
    //
    //        else
    //        {
    //            rowIndex = 0
    //        }
    //        scrollingTimer4 = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(NotificationViewController.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)
    //        return cell
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    //    {
    //        let dict2 = advertisementArr4[indexPath.row]
    //        self.ownerId5 = (dict2["owner_id"] as? String)!
    //        print("ownerId ======******\(ownerId5)")
    //        self.addDetailFive()
    //    }
    
    func showAdvertisement3(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //  LoadingIndicatorView.show("Loading....")
            let pstring = ""
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/banners", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss = \(result)")
                    if(status == true){
                        //    LoadingIndicatorView.hide()
                        OperationQueue.main.addOperation {
                            
                            self.advertisementArr4 = result["data"] as! [NSDictionary]
                            self.setupForAdsImages()
                            self.scrollingTimer4 = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: nil, repeats: true)
                            self.scrollingTimer4.fire()
                            
                            //    LoadingIndicatorView.hide()
                            // ApiResponse.alert(title: "OK", message: message, controller: self)
                        }
                        
                    }
                    else{
                        LoadingIndicatorView.hide()
                        //  ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                        
                    }
                    
                }
                else
                {
                    if(error == Constant.Status_Not_200)
                    {
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else
                    {
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    
    
    func addDetailFive(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = "owner_id=\(self.ownerId5)"
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/get_business_owner", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss = \(result)")
                    if(status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                var dict5 = result["data"] as! NSDictionary
                                let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                                dictProvider.providerdetailDict = dict5
                                self.present(dictProvider, animated: true, completion: nil)
                                
                                // ApiResponse.alert(title: "OK", message: message, controller: self)
                        }
                    }
                    else
                    {
                        LoadingIndicatorView.hide()
                        //ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                    
                }
                else{
                    if(error == Constant.Status_Not_200)
                    {
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else
                    {
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    func openPopUpDialoge(title: String, body : String , imagelink : String){
        
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUp") as! PopUpViewController
        popOverVC.titleText = title
        popOverVC.body = body
        popOverVC.imagelink = imagelink
        
        if(!popOverVC.isViewLoaded){
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            
        }
    }
}
    
    extension NotificationViewController : UITableViewDataSource, UITableViewDelegate {
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return notiDict.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell : NotificationTableViewCell = notificationTable.dequeueReusableCell(withIdentifier: "notificationcell")as! NotificationTableViewCell
            let dictData = notiDict[indexPath.row]
            
            
            
            
            self.flagStr = dictData["type"] as? String
            
            
            cell.layer.borderColor =  UIColor.init(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1.0).cgColor
            cell.layer.borderWidth = 4
            cell.clipsToBounds = true
            //          let data =  dictData["data"] as! String
            //      let jsonData = convertToJson(stringdata: data)
            
            
            
            
            if(self.flagStr! == "new_business")
            {
                //   cell.titleLabel.text! = "Received email from\((dictData["flag"]as? String)!) business owner. Check Inbox - Email"
                //  let jsonData = convertToJson(stringdata: data)
                
                let ownerData = dictData["owner_data"] as! NSDictionary
                
                let businessName = (ownerData["businessname"]as? String)!
                let text = "\(businessName) just registered"
                let rangeOfColoredString = (text as NSString).range(of: businessName)
                
                // Create the attributedString.
                let attributedString = NSMutableAttributedString(string:text)
                attributedString.setAttributes([NSAttributedStringKey.foregroundColor: swiftColor],
                                               range: rangeOfColoredString)
                cell.titleLabel.attributedText = attributedString
            }
                
                
            else if (self.flagStr! == "review_request")
            {
                let ownerData = dictData["owner_data"] as! NSDictionary
                
                
                let businessName = (ownerData["businessname"]as? String)!
                let text = "A Review is requested for \(businessName) please share your experience!"
                let rangeOfColoredString = (text as NSString).range(of: businessName)
                
                // Create the attributedString.
                let attributedString = NSMutableAttributedString(string:text)
                attributedString.setAttributes([NSAttributedStringKey.foregroundColor: swiftColor],
                                               range: rangeOfColoredString)
                cell.titleLabel.attributedText = attributedString
            }
                
            else if (self.flagStr! == "new_review")
            {
                let ownerData = dictData["owner_data"] as! NSDictionary
                
                
                let businessName = (ownerData["businessname"]as? String)!
                let text = "\(businessName) received a new review"
                let rangeOfColoredString = (text as NSString).range(of: businessName)
                
                // Create the attributedString.
                let attributedString = NSMutableAttributedString(string:text)
                attributedString.setAttributes([NSAttributedStringKey.foregroundColor: swiftColor],
                                               range: rangeOfColoredString)
                cell.titleLabel.attributedText = attributedString
            }
            else if (self.flagStr! == "new_coupon")
            {
                let ownerData = dictData["owner_data"] as! NSDictionary
                
                let businessName = (ownerData["businessname"]as? String)!
                let text = "New coupon from \(businessName)"
                let rangeOfColoredString = (text as NSString).range(of: businessName)
                
                
                
                
                // Create the attributedString.
                let attributedString = NSMutableAttributedString(string:text)
                attributedString.setAttributes([NSAttributedStringKey.foregroundColor: swiftColor],
                                               range: rangeOfColoredString)
                cell.titleLabel.attributedText = attributedString
            }
            else if (self.flagStr! == "new_event")
            {
                
                let text = "New Event "
                
                cell.titleLabel.text = text
            }
            else if (self.flagStr! == "new_announcement")
            {
                
                let text = "New Announement"
                
                cell.titleLabel.text = text
            }
            else if (self.flagStr! == "new_reward")
            {
                
                let text = "New reward"
                
                cell.titleLabel.text = text
            }
            
            
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let dataDict1 = notiDict[indexPath.row]
            
            //owner_data
            let flagStr1 = dataDict1["type"] as? String
            
            if(flagStr1! == "new_business")
            {
                //  print("d1 == \(d1)")
                let d1 = dataDict1["owner_data"] as! NSDictionary
                
                
                let provider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                print("dataDict1 ==***********************\(d1)")
                provider.providerdetailDict = d1
                provider.reviewRating = 0
                print("dataDict1 == \(dataDict1)")
               // provider.strOne = "one"
                self.present(provider, animated: true, completion: nil)
            }
                
                
            else if (flagStr1! == "review_request")
            {
                //   print("d1 == \(d1)")
                let d1 = dataDict1["owner_data"] as! NSDictionary
                let feedBack = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                print("dataDict1 ==***************************** \(dataDict1)")
                feedBack.providerdetailDict = d1
               // feedBack.strOne = "one"
                self.present(feedBack, animated: true, completion: nil)
            }
                
            else if (flagStr1! == "new_review")
            {
                let d1 = dataDict1["owner_data"] as! NSDictionary
                let review = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                print("dataDict1 ==***************************** \(dataDict1)")
                review.providerdetailDict = d1
                //review.strOne = "one"
                self.present(review, animated: true, completion: nil)
            }
            else if (flagStr1! == "new_event")
            {
                
                let review = self.storyboard?.instantiateViewController(withIdentifier: "localevents") as! LocalEventsVC
                
                self.present(review, animated: true, completion: nil)
            }
            else if (flagStr1! == "new_reward")
            {
                
                let review = self.storyboard?.instantiateViewController(withIdentifier: "rewardsviewcontroller") as! RewardsViewController
                
                self.present(review, animated: true, completion: nil)
            }
            else if (flagStr1! == "new_coupon")
            {
                let review = self.storyboard?.instantiateViewController(withIdentifier: "promotion") as! PromotionViewController
                
                self.present(review, animated: true, completion: nil)
            }
            else if (flagStr1! == "new_announcement")
            {
                let title = dataDict1["title"] as! String
                let body = dataDict1["body"] as! String
                let image = dataDict1["image"] as! String
                
               self.openPopUpDialoge(title: title,body: body,imagelink: image)
                
                
            }
//            else
//            {
//                print("\(String(describing: flagStr1!))")
//                ApiResponse.alert(title: "", message: "please check your mail", controller: self)
//            }
            
        }
        
    }
    


    class NotificationTableViewCell : UITableViewCell
    {
        @IBOutlet var titleLabel: UILabel!
    }
    
    class AdvertisementCellOne : UICollectionViewCell
    {
        @IBOutlet weak var addImageThree: UIImageView!
    }
    







