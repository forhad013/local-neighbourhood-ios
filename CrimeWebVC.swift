//
//  CrimeWebVC.swift
//  FairField
//
//  Created by Prashant Shinde on 9/12/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class CrimeWebVC: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webViewOne: UIWebView!
    var webStr : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webViewOne.delegate = self
        
        if webStr == "One" {
            let url = NSURL (string: "http://www.topix.com/crime/cypress-tx")
            let request = NSURLRequest(url: url! as URL)
            webViewOne.loadRequest(request as URLRequest)
            webViewOne.scalesPageToFit = true

        }
        else if (webStr == "Two")
        {
            let url = NSURL (string: "http://www.broadcastify.com/listen/feed/1364")
            let request = NSURLRequest(url: url! as URL)
            webViewOne.loadRequest(request as URLRequest)
            webViewOne.scalesPageToFit = true

        } else
        {
            let url = NSURL (string: "http://www.broadcastify.com/listen/ctid/2623")
            let request = NSURLRequest(url: url! as URL)
            webViewOne.loadRequest(request as URLRequest)
            webViewOne.scalesPageToFit = true

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// ** WEB VIEW DELEGATE ** ///
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingIndicatorView.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingIndicatorView.hide()
    }

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    
    
    

}
