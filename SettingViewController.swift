//
//  SettingViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 8/30/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import FirebaseMessaging
import UserNotifications

class SettingViewController: UIViewController {
    
    @IBAction func questionMarkBtn(_ sender: Any) {
        
        ApiResponse.alert(title: "Info", message: "Notifications for new giveaways!", controller: self)
    }
    
    @IBAction func specialAnnoucementInfoBtn(_ sender: Any) {
        
         ApiResponse.alert(title: "", message: "Notifications for new giveaways!", controller: self)
    }
    @IBOutlet weak var selectAllSwitch: UISwitch!
    @IBOutlet weak var newReviewsSw: UISwitch!
    @IBOutlet weak var requestReviewSw: UISwitch!
    @IBOutlet weak var businessListingSw: UISwitch!
    @IBOutlet weak var newEventSw: UISwitch!
    @IBOutlet weak var annoucmentSw: UISwitch!
    @IBOutlet weak var newRewardSw: UISwitch!
    
    @IBOutlet weak var newCouponSw: UISwitch!
    
    var newRewardBool: Bool = true
    var announcmentBool: Bool = true
    var newEventBool: Bool = true
    var newBusinessBool: Bool = true
    var requestReviewBool: Bool = true
    var newReviewBool: Bool = true
    var newCouponBool: Bool = true
    var userDef : UserDefaults!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        requestReviewBool = UserDefaults.standard.bool(forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
        announcmentBool = UserDefaults.standard.bool(forKey: Constant.NOTIFICATION_ANNOUNCMENT)
        newReviewBool = UserDefaults.standard.bool(forKey: Constant.NOTIFICATION_REVIEW_NEW)
        newBusinessBool = UserDefaults.standard.bool(forKey: Constant.NOTIFICATION_BUSINESS)
        newRewardBool = UserDefaults.standard.bool(forKey: Constant.NOTIFICATION_REWARD)
        newEventBool = UserDefaults.standard.bool(forKey: Constant.NOTIFICATION_EVENT)
        newCouponBool = UserDefaults.standard.bool(forKey: Constant.NOTIFICATION_COUPON)
        
        
        if(announcmentBool){
            annoucmentSw.setOn(true, animated: false)
        } else {
            annoucmentSw.setOn(false, animated: false)
        }
        if(newReviewBool){
            newReviewsSw.setOn(true, animated: false)
        } else {
            newReviewsSw.setOn(false, animated: false)
        }
        if(newBusinessBool){
            businessListingSw.setOn(true, animated: false)
        } else {
            businessListingSw.setOn(false, animated: false)
        }
        if(newRewardBool){
            newRewardSw.setOn(true, animated: false)
        } else {
            newRewardSw.setOn(false, animated: false)
        }
        if(newEventBool){
            newEventSw.setOn(true, animated: false)
        } else {
            newEventSw.setOn(false, animated: false)
        }
        if(requestReviewBool){
            requestReviewSw.setOn(true, animated: false)
        } else {
            requestReviewSw.setOn(false, animated: false)
        }
        
        if(newCouponBool){
            newCouponSw.setOn(true, animated: false)
        } else {
            newCouponSw.setOn(false, animated: false)
        }
        
        
        let application = UIApplication.shared
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        self.promptUserToRegisterPushNotifications()
        
        showPompt()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func showPompt(){
        
        let current = UNUserNotificationCenter.current()
        
        current.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .notDetermined {
                // Notification permission has not been asked yet, go for it!
                
                
                
            }
            
            if settings.authorizationStatus == .denied {
                let alertController = UIAlertController (title: "Notification is disabled", message: "Go to Settings?", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
            
            if settings.authorizationStatus == .authorized {
                // Notification permission was already granted
                print("yes")
            }
        })
    }
    
    
    
    
    
    
    func changeUserDefault(){
        if(newRewardSw.isOn){
            Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REWARD)
            UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REWARD)
        }else{
            Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REWARD)
            UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REWARD)
        }
        
        if(annoucmentSw.isOn){
            Messaging.messaging().subscribe(toTopic: Constant.TOPICS_ANNOUNCMENT)
            UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
        }else{
            Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_ANNOUNCMENT)
            UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
        }
        
        if(newEventSw.isOn){
            Messaging.messaging().subscribe(toTopic: Constant.TOPICS_EVENT)
            UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_EVENT)
        }else{
            Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_EVENT)
            UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_EVENT)
        }
        
        
        if(businessListingSw.isOn){
            Messaging.messaging().subscribe(toTopic: Constant.TOPICS_BUSINESS)
            UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_BUSINESS)
        }else{
            Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_BUSINESS)
            UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_BUSINESS)
        }
        
        if(requestReviewSw.isOn){
            Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_REQUEST)
            UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
        }else{
            Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_REQUEST)
            UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
        }
        
        if(newReviewsSw.isOn){
            Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_NEW)
            UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_NEW)
        }else{
            Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_NEW)
            UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_NEW)
        }
        
        
        if(newCouponSw.isOn){
            Messaging.messaging().subscribe(toTopic: Constant.TOPICS_COUPON)
            UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_COUPON)
        }else{
            Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_COUPON)
            UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_COUPON)
        }
        
    }
    
    @IBAction func switchPressed(_ sender: Any) {
      //  print((sender as AnyObject).tag)
        
        
        if((sender as AnyObject).tag == 6){
            
            if selectAllSwitch.isOn
            {
                enableAll(status: true)
            }
            else
            {
                enableAll(status: false)
                
            }
            
            self.changeUserDefault()
        }else if((sender as AnyObject).tag == 0){
            
            if(newRewardSw.isOn){
                Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REWARD)
                UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REWARD)
            }else{
                Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REWARD)
                UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REWARD)
            }
        }else if((sender as AnyObject).tag == 1){
            
            if(annoucmentSw.isOn){
                Messaging.messaging().subscribe(toTopic: Constant.TOPICS_ANNOUNCMENT)
                UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
            }else{
                Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_ANNOUNCMENT)
                UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
            }
        }else if((sender as AnyObject).tag == 2){
            
            if(newEventSw.isOn){
                Messaging.messaging().subscribe(toTopic: Constant.TOPICS_EVENT)
                UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_EVENT)
            }else{
                Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_EVENT)
                UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_EVENT)
            }
        }else if((sender as AnyObject).tag == 3){
            
            
            if(businessListingSw.isOn){
                Messaging.messaging().subscribe(toTopic: Constant.TOPICS_BUSINESS)
                UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_BUSINESS)
            }else{
                Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_BUSINESS)
                UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_BUSINESS)
            }
        }else if((sender as AnyObject).tag == 4){
            
            if(requestReviewSw.isOn){
                Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_REQUEST)
                UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
            }else{
                Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_REQUEST)
                UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
            }
        }else if((sender as AnyObject).tag == 5){
            
            if(newReviewsSw.isOn){
                Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_NEW)
                UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_NEW)
            }else{
                Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_NEW)
                UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_NEW)
            }
        }else if((sender as AnyObject).tag == 7){
            
            if(newCouponSw.isOn){
                Messaging.messaging().subscribe(toTopic: Constant.TOPICS_COUPON)
                UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_COUPON)
            }else{
                Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_COUPON)
                UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_COUPON)
            }
        }
    }
    
    
    func enableAll(status : Bool){
        
        newEventSw.setOn(status, animated: false)
        newRewardSw.setOn(status, animated: false)
        annoucmentSw.setOn(status, animated: false)
        businessListingSw.setOn(status, animated: false)
        requestReviewSw.setOn(status, animated: false)
        newReviewsSw.setOn(status, animated: false)
        newCouponSw.setOn(status, animated: false)
        // newEventSw.isEnabled = status
        
    }
    
    /*
     override func viewWillDisappear(_ animated: Bool) {
     
     
     if(newEventSw.isOn){
     Messaging.messaging().subscribe(toTopic: Constant.TOPICS_EVENT)
     UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_EVENT)
     }else{
     Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_EVENT)
     UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_EVENT)
     }
     
     if(newRewardSw.isOn){
     Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REWARD)
     UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REWARD)
     }else{
     Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REWARD)
     UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REWARD)
     }
     if(annoucmentSw.isOn){
     Messaging.messaging().subscribe(toTopic: Constant.TOPICS_ANNOUNCMENT)
     UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
     }else{
     Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_ANNOUNCMENT)
     UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_ANNOUNCMENT)
     }
     
     if(businessListingSw.isOn){
     Messaging.messaging().subscribe(toTopic: Constant.TOPICS_BUSINESS)
     UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_BUSINESS)
     }else{
     Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_BUSINESS)
     UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_BUSINESS)
     }
     
     if(requestReviewSw.isOn){
     Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_REQUEST)
     UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
     }else{
     Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_REQUEST)
     UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_REQUEST)
     }
     if(newReviewsSw.isOn){
     Messaging.messaging().subscribe(toTopic: Constant.TOPICS_REVIEW_NEW)
     UserDefaults.standard.set(true, forKey: Constant.NOTIFICATION_REVIEW_NEW)
     }else{
     Messaging.messaging().unsubscribe(fromTopic: Constant.TOPICS_REVIEW_NEW)
     UserDefaults.standard.set(false, forKey: Constant.NOTIFICATION_REVIEW_NEW)
     }
     
     
     }
     */
    
    
//    @IBAction func onoffSwitch(_ sender: Any)
//    {
//        userDef = UserDefaults.standard
//        let data1 = userDef.object(forKey: "userInfo") as! NSDictionary
//        let user_id = "\(data1["user_id"] as! String)"
//        
//        if selectAllSwitch.isOn
//        {
//            Messaging.messaging().subscribe(toTopic: user_id)
//        }
//        else
//        {
//            Messaging.messaging().unsubscribe(fromTopic: user_id)
//        }
//    }
    
    @IBAction func backActionBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

