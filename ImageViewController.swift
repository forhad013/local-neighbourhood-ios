//
//  ImageViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 8/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    
    
    @IBOutlet weak var imagecollectionView: UICollectionView!
    
    var commentDict : NSDictionary = [:]
    var imageArr : [String] = []
    var imgStr : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("imageArr == \(commentDict)")
        //    print("imgStr ================================ \(self.imgStr!)")
        imageArr = commentDict["images"] as! [String]
        imagecollectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
}

class ImageCell : UICollectionViewCell {
    
    @IBOutlet weak var showImage: UIImageView!
}

extension ImageViewController : UICollectionViewDataSource , UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if(self.imgStr! == "reviewImg"){
            let cell : ImageCell = imagecollectionView.dequeueReusableCell(withReuseIdentifier: "imagecell", for: indexPath)as! ImageCell
            
            let str = imageArr[indexPath.item]
            cell.showImage.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/assets/rating_review/\(str)"))
            return cell
        }
        else if(self.imgStr! == "commentStr"){
            let cell : ImageCell = imagecollectionView.dequeueReusableCell(withReuseIdentifier: "imagecell", for: indexPath)as! ImageCell
            
            let str = imageArr[indexPath.item]
            cell.showImage.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/assets/rating_review/\(str)"))
            return cell
            
        }
        else if(self.imgStr! == "commentStr1"){
            let cell : ImageCell = imagecollectionView.dequeueReusableCell(withReuseIdentifier: "imagecell", for: indexPath)as! ImageCell
            
            let str = imageArr[indexPath.item]
            cell.showImage.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/assets/rating_review/\(str)"))
            return cell
            
        }
        else {
            let cell : ImageCell = imagecollectionView.dequeueReusableCell(withReuseIdentifier: "imagecell", for: indexPath)as! ImageCell
            
            let str = imageArr[indexPath.item]
            cell.showImage.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/assets/events/\(str)"))
            return cell
        }
        
    }
    
}
