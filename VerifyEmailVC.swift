//
//  VerifyEmailVC.swift
//  FairField
//
//  Created by Prashant Shinde on 7/3/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class VerifyEmailVC: UIViewController , UITextFieldDelegate{
    
    @IBOutlet var codeText: UITextField!
    
    var emailId : String = ""
    var newFrame : CGRect!
    var userDef : UserDefaults!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let placeholder = ["Enter Code"]
        
        codeText.delegate = self
        let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 5, height: codeText.frame.size.height))
        //Adding the padding to the second textField
        codeText.leftView = paddingForFirst
        codeText.leftViewMode = UITextFieldViewMode .always
        
        codeText.attributedPlaceholder = NSAttributedString(string:placeholder[0],
                                                            attributes:[kCTForegroundColorAttributeName as NSAttributedStringKey: UIColor.white])
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.newFrame = self.view.frame
        
        newFrame.origin.y = self.view.bounds.origin.y - 110
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                
                self.view.frame = self.newFrame
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        newFrame.origin.y = 0
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                self.view.frame = self.newFrame
        })
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    @IBAction func submitAction(_ sender: Any) {
        
        if (codeText.text == "")  {
            ApiResponse.alert(title: "Empty", message: "Please enter verify code", controller: self)
        }
        else {
            
            LoadingIndicatorView.show("Loading...")
            
            let params = "code=\(codeText.text!)&email=\(self.emailId)"
            
            print("paramsparamsparams\(params)")
            
            LoadingIndicatorView.hide()
            
            ApiResponse.onResponsePostPhp(url: "Userajax/verifycode", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    print("verify code result = \(result)")
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    
                    if (status == true) {
                        
                        OperationQueue.main.addOperation {
                            
                            print("result = @ \(result)")
                            
                            let data = result["data"] as! NSDictionary
                            let userDef = UserDefaults.standard
                            userDef.set(data, forKey: "userInfo")
                            userDef.set("login", forKey: "session")

                            
                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel) {
                                action -> Void in
                                
                                let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
                                self.present(dashboard, animated: true, completion: nil)
                            }
                                
                                )
                            self.present(alert , animated: true ,completion: nil)

                           
                            
                            
                            
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        
        
    }
    
    @IBAction func resentcodeAction(_ sender: Any) {
        
        
        LoadingIndicatorView.show("Loading...")
        
        let params = "email=\(self.emailId)"
        
        print("paramsparamsparams\(params)")
        
        LoadingIndicatorView.hide()
        
        ApiResponse.onResponsePostPhp(url: "Userajax/resendcode", parms: params, completion: { (result, error) in
            
            if (error == "") {
                print("verify code result = \(result)")
                
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                
                if (status == true) {
                    
                    OperationQueue.main.addOperation {
                        
                        print("result = @ \(result)")
                        
                        ApiResponse.alert(title: "OK", message: message, controller: self)
                        
                    }
                }
                else {
                    ApiResponse.alert(title: "Oops!", message: message, controller: self)
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
        
        
        
    }
    
}
