//
//  RewardOneCell.swift
//  Local Neighbourhood App
//
//  Created by KpStar on 6/9/18.
//  Copyright © 2018 Prashant Shinde. All rights reserved.
//

import UIKit

class RewardOneCell: UITableViewCell {
    
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var commentTxt: UITextView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
