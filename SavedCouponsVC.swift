//
//  SavedCouponsVC.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class SavedCouponsVC: UIViewController {
    
    @IBOutlet var couponsCollectionView: UICollectionView!
    @IBOutlet weak var saveCouponLabel: UILabel!
    @IBOutlet weak var imageViewForAds: UIImageView!
    
    private var images: [UIImage] = []
    private var texts: [String] = []
    
     var Coupon_ownerid: String? = nil

    var user_id : String = ""
    var userDef : UserDefaults!
    var couponDict : [NSDictionary] = []
    var deleteCouponDict : [NSDictionary] = []
    var couponId : String!
    var advertisementArr13 : [NSDictionary] = []
    var scrollingTimer13 = Timer()
    var ownerId13 : String = ""
    var deleteID : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"
        self.saveCoupons()
        self.showAdvertisement2()
        addGestureToAdsImageView()
    }
    
    func addGestureToAdsImageView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(adsImageViewAction))
        self.imageViewForAds.isUserInteractionEnabled = true
        self.imageViewForAds.tag = -1
        self.imageViewForAds.addGestureRecognizer(tapGesture)
    }
    
    private var adsImageIndex = 0
    
    @objc func startTimer(theTimer : Timer){
        //        UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseOut, animations: {
        //            self.addCollectionViewThree.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int , section:0), at: .centeredHorizontally, animated: false)
        //        }, completion: nil)
        setupForAdsImages()
    }
    
    func stopTimerTest() {
        scrollingTimer13.invalidate()
        
    }
    
    func setupForAdsImages() {
        if adsImageIndex >= advertisementArr13.count {
            adsImageIndex = 0
        }
        let dict1 = advertisementArr13[adsImageIndex]
        adView(userid: self.user_id, bannerid: dict1.value(forKey: "id")! as! String , ownerid: dict1.value(forKey: "owner_id")! as! String)
        self.imageViewForAds.tag = adsImageIndex
        adsImageIndex = adsImageIndex + 1
        let imageString = dict1["image"] as! String
        //print("imageString = \(imageString)")
        imageViewForAds.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
        
        
    }
    
    @objc func adsImageViewAction(sender: UITapGestureRecognizer!) {
        print("adsImageView clicked \((sender.view?.tag)!)")
        if (sender.view?.tag)! != -1 {
            let dict2 = advertisementArr13[(sender.view?.tag)!]
            self.ownerId13 = (dict2["owner_id"] as? String)!
            print("ownerId ======******\(ownerId13)")
            adClick(userid: self.user_id, bannerid: dict2.value(forKey: "id")! as! String , ownerid: dict2.value(forKey: "owner_id")! as! String)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBack(_ sender: Any) {
        stopTimerTest()
        self.dismiss(animated: false, completion: nil)
    }
    
    func adClick(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/banner_click", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                
                                self.addDetailTwo()
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    func adView(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/save_page_viewer", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                // self.importantArr = result["data"] as! [NSDictionary]
                                //self.importantTable.reloadData()
                                //  LoadingIndicatorView.hide()
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    
    func showAdvertisement2(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            
            let pstring = ""
            ApiResponse.onResponsePostPhp(url: "Userajax/banners", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss = \(result)")
                    if(status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                            self.advertisementArr13 = result["data"] as! [NSDictionary]
                            print("self.advertisementArr1  == \(self.advertisementArr13)")
                            self.setupForAdsImages()
                            self.scrollingTimer13 = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: nil, repeats: true)
                            self.scrollingTimer13.fire()
                            if self.couponDict.count != 0
                            {
                                self.couponsCollectionView.reloadData()
                            }
                            else
                            {}
                        }
                    }
                    else{}
                }
                else
                {
                    if(error == Constant.Status_Not_200)
                    {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else
                    {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }

    func saveCoupons()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let pstring = "user_id=\(self.user_id)"
            ApiResponse.onResponsePostPhp(url: "Userajax/like_coupon_list", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    if(status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                            self.couponsCollectionView.reloadData()
                            self.couponDict = result["data"] as! [NSDictionary]
                            print("OK=====",self.couponDict)
                                print("Finish=====",self.couponDict)

                            if self.couponDict.count != 0
                            {
                                self.saveCouponLabel.isHidden = true
                                self.couponsCollectionView.reloadData()
                                
                                
                                
                            }
                            else
                            {}
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation
                            {
                            LoadingIndicatorView.hide()
                            self.couponsCollectionView.isHidden = true
                            self.saveCouponLabel.isHidden = false
                        }
                    }
                }
                else
                {
                    if(error == Constant.Status_Not_200)
                    {
                      //  ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else
                    {
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
}

extension SavedCouponsVC : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == couponsCollectionView
        {
        return couponDict.count
        }
        else
        {
        return advertisementArr13.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
//        if collectionView ==  couponsCollectionView
//        {
        let cell : CouponCollectionViewCell = couponsCollectionView.dequeueReusableCell(withReuseIdentifier: "couponscell", for: indexPath) as! CouponCollectionViewCell
        let saveCoupon = couponDict[indexPath.item]
        let imageString = saveCoupon["image"] as? String
        cell.couponImg.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/coupon/\(imageString!)"))
        cell.couponName.text = saveCoupon["coupon_name"] as? String
        cell.couponTitle.text = saveCoupon["description"] as? String
        self.couponId = saveCoupon["id"] as? String
 
        //cell.removeCouponBtn.tag = Int((saveCoupon["id"] as? NSNumber)!)
        cell.removeCouponBtn.tag = Int((saveCoupon["id"]  as? String)!)!

        cell.removeCouponBtn.addTarget(self, action: #selector(deleteCoupon(_:)), for: .touchUpInside)
        return cell
//        }
//        else
//        {
//            let cell : AddBannerThreeCell = advertisementcvseven.dequeueReusableCell(withReuseIdentifier: "addbannerthreecell", for: indexPath) as! AddBannerThreeCell
//            let dict1 = advertisementArr13[indexPath.item]
//            let imageString = dict1["image"] as! String
//            cell.bannerImsgeone.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
//            var rowIndex = indexPath.item
//            let numberofRecords : Int = self.advertisementArr13.count - 1
//            if (rowIndex < numberofRecords)
//            {
//                rowIndex = (rowIndex + 1)
//            }
//            else
//            {
//                rowIndex = 0
//            }
//            scrollingTimer13 = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(SavedCouponsVC.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)
//            return cell
//        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == couponsCollectionView {
            print("hello world")
            
            let saveCoupon = couponDict[indexPath.item]
            let imageString = saveCoupon["image"] as? String
//            cell.couponImg.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/coupon/\(imageString!)"))
//            cell.couponName.text = saveCoupon["coupon_name"] as? String
//            cell.couponTitle.text = saveCoupon["description"] as? String
            
            let showSavedCoupon = self.storyboard?.instantiateViewController(withIdentifier: "ShowSavedCouponViewController") as! ShowSavedCouponViewController
            showSavedCoupon.imageString = "https://www.localneighborhoodapp.com/images/coupon/\(imageString!)"
            showSavedCoupon.descriptionString = saveCoupon["description"] as? String
            showSavedCoupon.labelString = saveCoupon["coupon_name"] as? String
            showSavedCoupon.currentID = saveCoupon["owner_id"] as? String
            Coupon_ownerid = saveCoupon["owner_id"] as? String
            //print("Coupon_ownerid ======******"\(Coupon_ownerid))
             print("ownerId ======******\(String(describing: Coupon_ownerid))")

            self.present(showSavedCoupon, animated: true, completion: nil)
        }
        else {
            let dict2 = advertisementArr13[indexPath.row]
            self.ownerId13 = (dict2["owner_id"] as? String)!
            print("ownerId ======******\(ownerId13)")
            self.addDetailTwo()
        }
    }
    
        func addDetailTwo(){
            if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
            {
                LoadingIndicatorView.show("Loading....")
                let pstring = "owner_id=\(self.ownerId13)"
                
                print("\(pstring)")
                ApiResponse.onResponsePostPhp(url: "Userajax/get_business_owner", parms: pstring, completion: {(result , error) in
                    if(error == ""){
                        
                        
                        let status = result["status"]as! Bool
                        let message = result["message"]as! String
                       // print("ressassss = \(result)")
                        if(status == true){
                            OperationQueue.main.addOperation {
                                
                                var dict5 = result["data"] as! NSDictionary
                                
                                let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                                dictProvider.providerdetailDict = dict5
                                self.present(dictProvider, animated: true, completion: nil)
                                
                                
                                LoadingIndicatorView.hide()
                                // ApiResponse.alert(title: "OK", message: message, controller: self)
                            }
                            
                        }
                        else{
                            LoadingIndicatorView.hide()
                            ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                            
                        }
                        
                    }
                    else{
                        if(error == Constant.Status_Not_200){
                            LoadingIndicatorView.hide()
                            ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                        }
                        else {
                            LoadingIndicatorView.hide()
                            ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                        }
                    }
                    
                })
                
            }
            else
            {
                //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
            }
        }

    
    @objc func deleteCoupon(_ sender: UIButton!)
    {
//        let point : CGPoint = sender.convert(CGPoint.zero, to:couponsCollectionView)
//        let indexPath1 = couponsCollectionView.indexPathForItem(at: point)
//        let dictNoti = couponDict[(indexPath1?.item)!]
        deleteID = sender.tag
        self.deleteCoupon()
        
    }
    
    func deleteCoupon(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = "coupon_id=\(deleteID)&user_id=\(self.user_id)"
            //let pstring = "coupon_id=\(self.couponId!)&user_id=\(user_id)"

            
            print("DELETE\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/remove_coupon", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    print("ressassss = \(result)")
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            
                            LoadingIndicatorView.hide()
                            let data = result["data"] as! String
                           // ApiResponse.alert(title: "OK", message: message, controller: self)
                            self.saveCoupons()
                            
                        }
                        
                    }
                    else{
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                        
                    }
                    
                }
                else{
                    if(error == Constant.Status_Not_200){
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        LoadingIndicatorView.hide()
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }

   

    }
    


class AddBannerThreeCell : UICollectionViewCell {

    @IBOutlet weak var bannerImsgeone: UIImageView!
}

class CouponCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var removeCouponBtn: KGHighLightedButton!
    @IBOutlet weak var couponImg: UIImageView!
    @IBOutlet weak var couponName: UILabel!
    @IBOutlet weak var couponTitle: UILabel!
    
    
}
//@IBOutlet weak var couponImage: UIImageView!
