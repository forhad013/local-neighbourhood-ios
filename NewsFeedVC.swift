//
//  NewsFeedVC.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class NewsFeedVC: UIViewController , UISearchBarDelegate {

    @IBOutlet weak var newBusinessTV: UITableView!
    @IBOutlet weak var listTable1: UITableView!
    @IBOutlet weak var navgationView: UIView!
    @IBOutlet weak var searchData: UISearchBar!
    @IBOutlet weak var imageViewForAds: UIImageView!
    @IBOutlet weak var lblNoData: UILabel!

   // @IBOutlet weak var businessLabel: UILabel!
    
    var newsArr : [NSDictionary] = []
    var listArray1 : [String] = []
    var searchone : String!
    var userDef : UserDefaults!
    var searching : Bool = false
    var filtered : [String] = []
    var data : [String] = []
    var arr : [String] = []
    var advertisementArr2 : [NSDictionary] = []
    var scrollingTimer2 = Timer()
    var ownerId1 : String = ""
    var addressone : Int = 0
    var webLink : String = ""
    var userid: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dic = UserDefaults.standard.value(forKey: "userInfo") as! NSDictionary
        userid = dic.value(forKey: "user_id")! as! String
        lblNoData.isHidden = true
        if(self.searchone! == "searchData")
        {
            navgationView.isHidden = false
            
            listTable1.separatorStyle = .none
            data = arr
        }
        else {
            listArray1 = ["New Businesses", "New Reviews",   "Requested Reviews"]
            navgationView.isHidden = true
        }
        listTable1.isHidden = true
        searchData.delegate = self
        
        DispatchQueue.main.async {
            self.getLocal()
            self.showAdvertisement2()
        }
        addGestureToAdsImageView()
    }
    
    func addGestureToAdsImageView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(adsImageViewAction))
        self.imageViewForAds.isUserInteractionEnabled = true
        self.imageViewForAds.tag = -1
        self.imageViewForAds.addGestureRecognizer(tapGesture)
    }
    
    private var adsImageIndex = 0
    
    @objc func startTimer(theTimer : Timer){
        //        UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseOut, animations: {
        //            self.addCollectionViewThree.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int , section:0), at: .centeredHorizontally, animated: false)
        //        }, completion: nil)
        setupForAdsImages()
    }
    
    func stopTimerTest() {
        
        scrollingTimer2.invalidate()
    }
    
    func setupForAdsImages() {
        if adsImageIndex >= advertisementArr2.count {
            adsImageIndex = 0
        }
        let dict1 = advertisementArr2[adsImageIndex]
        adView(userid: userid, bannerid: dict1.value(forKey: "id")! as! String, ownerid: dict1.value(forKey: "owner_id")! as! String)
        self.imageViewForAds.tag = adsImageIndex
        adsImageIndex = adsImageIndex + 1
        let imageString = dict1["image"] as! String
        //print("imageString = \(imageString)")
        imageViewForAds.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
        
        
    }
    
    @objc func adsImageViewAction(sender: UITapGestureRecognizer!) {
        print("adsImageView clicked \((sender.view?.tag)!)")
        if (sender.view?.tag)! != -1 {
            let dict2 = advertisementArr2[(sender.view?.tag)!]
            self.ownerId1 = (dict2["owner_id"] as? String)!
            print("ownerId ======******\(ownerId1)")
            adClick(userid: userid, bannerid: dict2.value(forKey: "id")! as! String, ownerid: dict2.value(forKey: "owner_id")! as! String)
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func onBack(_ sender: Any) {
        stopTimerTest()
         self.dismiss(animated: false, completion: nil)
    }
    @IBAction func backOne(_ sender: Any) {
        stopTimerTest()
          self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func openList1(_ sender: Any) {
         if listTable1.isHidden {
            listTable1.isHidden = false
        } else {
            listTable1.isHidden = true
        }
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searching = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searching = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let params = "user_id=\(userid)&search_keyword=\(searchText)&value_match=0"
        print("params == \(params)")
        self.getSearch(keywordStr: params)
    }
    
    func adClick(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/banner_click", parms: pstring, completion: {(result , error) in
                 print("OK======",ApiResponse.onResponsePostPhp)
                print("pstring======",pstring)


                if(error == "")
                {
                    print("start result======",result)
                    print("finish======")


                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                self.addDetailTwo()
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    func adView(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/save_page_viewer", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    func showAdvertisement2(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let pstring = ""
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/banners", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    let status = result["status"]as! Bool
                   // let message = result["message"]as! String
                    print("ressassss = \(result)")
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            self.advertisementArr2 = result["data"] as! [NSDictionary]
                            print("self.advertisementArr1  == \(self.advertisementArr2)")
                            self.setupForAdsImages()
                            self.scrollingTimer2 = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: nil, repeats: true)
                            self.scrollingTimer2.fire()
//                            self.addCollectionViewTwo.reloadData()
                            
                            LoadingIndicatorView.hide()
                        }
                    }
                    else{
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                       // ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    func getSearch(keywordStr : String) {
        
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            ApiResponse.onResponsePostPhp(url: "Userajax/search_business_owner", parms: keywordStr, completion: {(result , error) in
                
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss == \(result)")
                    if(status == true){
                        OperationQueue.main.addOperation
                        {
                            self.newsArr = result["data"] as! [NSDictionary]
                            self.newBusinessTV.isHidden = false
                            self.newBusinessTV.reloadData()
                            LoadingIndicatorView.hide()
                            self.lblNoData.isHidden = false

                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation
                        {
                            self.newBusinessTV.isHidden = true
                            self.lblNoData.isHidden = false
                        }
                       // ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else
                {
                    if(error == Constant.Status_Not_200)
                    {
                        //ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else
                    {
                        ApiResponse.alert(title: "Request time out", message: "Please check internet Connection", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    func getLocal(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = ""
            
           // print("OKOKOKOKOK\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/new_business_owner", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                   
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                     print("local data == \(result)")
                    print("finish local data ")

                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            LoadingIndicatorView.hide()
                            self.newsArr = result["data"] as! [NSDictionary]
                            if self.newsArr.count != 0
                            {
                                self.newBusinessTV.reloadData()
                                 //self.businessLabel.isHidden = true
                                self.lblNoData.isHidden = true

                            }
                            else
                            {
                               self.newBusinessTV.isHidden = true
                              //  self.businessLabel.isHidden = false
                                self.lblNoData.isHidden = false

                            }
                        }
                    }
                    else{
                        LoadingIndicatorView.hide()
                      //  ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    

}

//class NewBusinessCVcell : UICollectionViewCell {
//
//    @IBOutlet weak var newsImage: UIImageView!
//    @IBOutlet weak var businessName: UILabel!
//    @IBOutlet weak var providerName: UILabel!
//    @IBOutlet weak var email: UILabel!
//    @IBOutlet weak var contact: UILabel!
//    @IBOutlet weak var address: UILabel!
//    @IBOutlet weak var website: UILabel!
//    @IBOutlet var starCollection1: [UIImageView]!
//    @IBOutlet weak var emailHt: NSLayoutConstraint!
//    @IBOutlet weak var addressHt: NSLayoutConstraint!
//    @IBOutlet weak var websiteHt: NSLayoutConstraint!
//    
//}

class AddCellTwo: UICollectionViewCell {
    
    @IBOutlet weak var addImg3: UIImageView!
}

extension  NewsFeedVC {//} : UICollectionViewDataSource , UICollectionViewDelegate {

//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        return advertisementArr2.count
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//            let cell : AddCellTwo = addCollectionViewTwo.dequeueReusableCell(withReuseIdentifier: "addcelltwo", for: indexPath) as! AddCellTwo
//
//            let dict1 = advertisementArr2[indexPath.item]
//            let imageString = dict1["image"] as! String
//            print("imageString = https://www.localneighborhoodapp.com/images/banner/\(imageString)")
//            cell.addImg3.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
//
//            var rowIndex = indexPath.item
//            let numberofRecords : Int = self.advertisementArr2.count - 1
//            if (rowIndex < numberofRecords) {
//                rowIndex = (rowIndex + 1)
//
//            }else {
//                rowIndex = 0
//
//            }
//            scrollingTimer2 = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(NewsFeedVC.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)
//            return cell
//
//
//             }

    
 
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
//    {
//
//            let dict2 = advertisementArr2[indexPath.row]
//            self.ownerId1 = (dict2["owner_id"] as? String)!
//            print("ownerId ======******\(ownerId1)")
//            self.addDetailTwo()
//
//    }
    
    
    func addDetailTwo(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = "owner_id=\(self.ownerId1)"
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/get_business_owner", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss = \(result)")
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            LoadingIndicatorView.hide()
                            let dict5 = result["data"] as! NSDictionary
                            
                            let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                            dictProvider.providerdetailDict = dict5
                            self.present(dictProvider, animated: true, completion: nil)
                           
                            // ApiResponse.alert(title: "OK", message: message, controller: self)
                        }
                        
                    }
                    else{
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    
    
}

class NewBusinessCell : UITableViewCell {
   
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var businessNameOne: UILabel!
   // @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var contact: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var website: UILabel!
    @IBOutlet var starCollection1: [UIImageView]!
    @IBOutlet var emailHt: NSLayoutConstraint!
    @IBOutlet var contactHt: NSLayoutConstraint!
    @IBOutlet var addressHt: NSLayoutConstraint!
    @IBOutlet var websiteHt: NSLayoutConstraint!
    
    
}

extension NewsFeedVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == newBusinessTV
        {
            return newsArr.count
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == newBusinessTV
        {
            return 1
        }
        else
        {
             return listArray1.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == newBusinessTV
        {
            let cell : NewBusinessCell = newBusinessTV.dequeueReusableCell(withIdentifier: "newbusinesscell") as! NewBusinessCell
            
            let newsDict = newsArr[indexPath.section]
            
            cell.businessNameOne.text! = "\((newsDict["businessname"] as? String)!)"
            
            let emailstr = newsDict["email"] as! String
            if (emailstr == "") {
                cell.emailHt.isActive = true
                cell.emailHt.constant = 0
            }
            else {
                cell.emailHt.isActive = false
                let ht = ApiResponse.calculateHeightForString(emailstr)
                if (emailstr.count > 33) {
                    //                   cell.emailHt.constant = ht + 20
                }
                cell.email.text! = "Email : \(emailstr)"
                cell.email.numberOfLines = 0
            }
            
            let phonestr = newsDict["phonenumber"] as! String
            if (phonestr == "") {
                cell.contactHt.isActive = true
                cell.contactHt.constant = 0
            }
            else {
                cell.contactHt.isActive = false
                //                cell.contactHt.constant = 15
                cell.contact.text! = "Contact : \(phonestr)"
            }
            
            let addr = Int((newsDict["address_status"] as? String)!)
            if (addressone == addr)
            {
                cell.addressHt.isActive = true
                cell.addressHt.constant = 0
            }
            else
            {
                cell.addressHt.isActive = false
                let ht = ApiResponse.calculateHeightForString((newsDict["address"] as! String))
                if ((newsDict["address"] as! String).count > 33) {
                    //                     cell.addressHt.constant = ht + 20
                }
                cell.address.text! = "Address : \((newsDict["address"] as? String)!)"
                cell.address.numberOfLines = 0
            }
            
            self.webLink = (newsDict["weblink"] as? String)!
            if self.webLink == ""
            {
                cell.websiteHt.isActive = true
                cell.websiteHt.constant = 0
            } else
            {
                cell.websiteHt.isActive = false
                let ht = ApiResponse.calculateHeightForString((newsDict["weblink"] as! String))
                
                if (self.webLink.count > 33) {
                    //                   cell.websiteHt.constant = ht + 20
                }
                cell.website.text! = "Website : \((newsDict["weblink"] as? String)!)"
                cell.website.numberOfLines = 0
            }
            
            cell.newsImage.layer.masksToBounds = false
            cell.newsImage.layer.cornerRadius = cell.newsImage.frame.size.width/2
            cell.newsImage.clipsToBounds = true
            cell.newsImage.layer.borderWidth = 1.0
            cell.newsImage.layer.borderColor = UIColor.black.cgColor
            var imageString = newsDict["image"] as! String
            imageString = imageString.replacingOccurrences(of: " ", with: "%20")
            cell.newsImage.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/\(imageString)"))
            
            ApiResponse.callRating(Double((newsDict["average_rating"] as? String)!)!, starRating: cell.starCollection1)
            return cell
        }
        else
        {
            let cell : UITableViewCell = listTable1.dequeueReusableCell(withIdentifier: "listcell1")!
            cell.textLabel?.text = listArray1[indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            let itemSize = CGSize(width : 15, height : 15);
            UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
            let imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
            cell.imageView?.image?.draw(in: imageRect)
            cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return cell
        }
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == newBusinessTV
//        {
//            var httotal : CGFloat = 0
//            var minusValue : CGFloat = 0
//            
//            let dict = newsArr[indexPath.section]
//            
//            let addr = dict["address_status"] as? String
//            if (addr == "0") {
//                minusValue = minusValue + 15
//            }
//            else {
//                if ((dict["address"] as! String).characters.count > 30) {
//                    let htt = ApiResponse.calculateHeightForString(dict["address"] as! String)
//                    httotal = httotal + htt + 20
//                }
//            }
//            
//            if ((dict["weblink"] as? String)! ==  "") {
//                minusValue = minusValue + 15
//            }
//            else {
//                if ((dict["weblink"] as! String).characters.count > 30) {
//                    let htt = ApiResponse.calculateHeightForString(dict["address"] as! String)
//                    httotal = httotal + htt + 20
//                }
//            }
//            
//            if ((dict["email"] as? String)! ==  "") {
//                minusValue = minusValue + 15
//            }
//            else {
//                if ((dict["email"] as! String).characters.count > 30) {
//                    let htt = ApiResponse.calculateHeightForString(dict["address"] as! String)
//                    httotal = httotal + htt + 20
//                }
//            }
//            
//            if ((dict["phonenumber"] as? String)! ==  "") {
//                minusValue = minusValue + 15
//            }
//            
//            if (httotal >= 40) {
//                return 140 + (httotal + 20)
//            }
//            else {
//                return 140 + httotal
//            }
//        }
//        else
//        {
//            return 31
//        }
//    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == newBusinessTV
        {
//            var httotal : CGFloat = 0
//            var minusValue : CGFloat = 0
//
//            let dict = newsArr[indexPath.section]
//
//            let addr = dict["address_status"] as? String
//            if (addr ==  "0") {
//               minusValue = minusValue + 15
//            }
//            else {
//                if ((dict["address"] as! String).count > 33) {
//                    let htt = ApiResponse.calculateHeightForString(dict["address"] as! String)
//                    httotal = httotal + htt + 20
//                }
//            }
//
//            if ((dict["weblink"] as? String)! ==  "") {
//               minusValue = minusValue + 15
//            }
//            else {
//                 if ((dict["weblink"] as! String).count > 33) {
//                    let htt = ApiResponse.calculateHeightForString(dict["address"] as! String)
//                    httotal = httotal + htt + 20
//                 }
//            }
//
//            if ((dict["email"] as? String)! ==  "") {
//                 minusValue = minusValue + 15
//            }
//            else {
//                if ((dict["email"] as! String).count > 33) {
//                    let htt = ApiResponse.calculateHeightForString(dict["address"] as! String)
//                    httotal = httotal + htt + 20
//                }
//            }
//
//            if ((dict["phonenumber"] as? String)! ==  "") {
//               minusValue = minusValue + 15
//            }
//
//            if (httotal >= 40) {
//                return 140 + (httotal) - minusValue
//            }
//            else {
//                return 160 + httotal - minusValue
//            }
            
            return UITableViewAutomaticDimension
        }
        else
        {
            return 31
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == newBusinessTV
        {
            let dict2 = newsArr[indexPath.section]
            let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
            dictProvider.providerdetailDict = dict2
            self.present(dictProvider, animated: true, completion: nil)
        }
        else {
            switch indexPath.row {
            case 0:
    
                let feed = self.storyboard?.instantiateViewController(withIdentifier: "newsfeed") as! NewsFeedVC
                feed.searchone = "search2"
                self.present(feed, animated: true, completion: nil)
                break
                
            case 1:

                let feed = self.storyboard?.instantiateViewController(withIdentifier: "reviewsviewcontroller") as! ReviewsViewController
                self.present(feed, animated: true, completion: nil)
                break
                
            case 2:
      
                let feed = self.storyboard?.instantiateViewController(withIdentifier: "reviewrequestvc") as! ReviewRequestVC
                self.present(feed, animated: true, completion: nil)
                break
            default :
                break
        }
        listTable1.isHidden = true
      }
    }
}
 

