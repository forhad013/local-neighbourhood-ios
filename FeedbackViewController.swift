//
//  FeedbackViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//
/*
import UIKit
import FirebaseMessaging
import ReachabilitySwift

class FeedbackViewController: UIViewController , UIImagePickerControllerDelegate ,  UINavigationControllerDelegate , UITextViewDelegate{

    
    @IBOutlet var starCollection: [UIButton]!
    @IBOutlet var titleText: UITextField!
    @IBOutlet var descriptionText: UITextView!
    @IBOutlet weak var imageUpload2CV: UICollectionView!
    @IBOutlet weak var submitBtn: KGHighLightedButton!
    
    var feedback_Id : String = ""
    var rating : Int = 0
    var picker2 : UIImagePickerController? = UIImagePickerController()
    var newFrame2 : CGRect!
    var imageArray2 : [UIImage] = []
    var feedBackDict : NSDictionary!
    var user_id : String = ""
    var userDef : UserDefaults!
    
    var editDict : NSDictionary!
    var editDictOne : NSDictionary!
    //var editStr : String = ""
    var editStr : String!

    var editimageArr : [String] = []
    var editimageArrOne : [String] = []
    var editimageArrTwo : [UIImage] = []
    var idStr : String = ""
    var owneridStr : String = ""
    var reviewidStr : String = ""
    var deleteImg : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
         if editStr == "editone" || editStr == "edit"
        {
            print(editDictOne)
            self.reviewidStr = editDictOne["id"] as! String
            self.owneridStr = editDictOne["owner_id"] as! String
            self.idStr = editDictOne["user_id"] as! String
            self.titleText.text! = editDictOne["title"] as! String
            self.descriptionText.text! = editDictOne["review"] as! String
            
            editimageArr = editDictOne["images"] as! [String]
            let rate : Int = Int((editDictOne["rating"] as? String)!)!
            if rate > 0
            {
                           for i in 0...rate-1
                            {
                                self.starCollection[i].setBackgroundImage(UIImage (named: "yellowStar.png"), for: UIControlState.normal)
                            }
            }
            else
            
            {
                
                for star in self.starCollection as [UIButton]
                {
                    star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
                }

            }
            
            for star in self.starCollection as [UIButton]
            {
                star.addTarget(self, action: #selector(FeedbackViewController.buttonClicked(button:)), for: .touchUpInside)
            }

//           for i in 0...rate-1
//            {
//                self.starCollection[i].setBackgroundImage(UIImage (named: "yellowStar.png"), for: UIControlState.normal)
//            }
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                // your code here
//                self.loadImage()
//            }
            DispatchQueue.main.async {
                // Update UI
                self.loadImage()
            }


//            editimageArrOne = editDictOne["images"] as! [String]
//            if(editimageArrOne.count > 0)
//            {
//            for im in editimageArrOne
//            {
//
//                DispatchQueue.main.async() {
//                    let data = try? Data.init(contentsOf: URL(string : "https://www.localneighborhoodapp.com/assets/rating_review/\(im)")!)
//
//                    print("https://www.localneighborhoodapp.com/assets/rating_review/\(im)")
//                                    let img = UIImage.sd_image(with: data)
////                    self.editimageArrTwo.append(img!)
//                }
//
////                let data = try? Data.init(contentsOf: URL(string : "https://www.localneighborhoodapp.com/assets/rating_review/\(im)")!)
////
////                print("https://www.localneighborhoodapp.com/assets/rating_review/\(im)")
//////                let img = UIImage.sd_image(with: data)
//////                editimageArrTwo.append(img!)
//             }
//            }
            submitBtn.setTitle("Save", for: UIControlState.normal)
            self.imageUpload2CV.reloadData()
        }
        else
         {
            descriptionText.delegate = self
            descriptionText.layer.borderWidth = 1.0
            descriptionText.layer.borderColor = UIColor.lightGray.cgColor
        
            for star in self.starCollection as [UIButton]
            {
                star.addTarget(self, action: #selector(FeedbackViewController.buttonClicked(button:)), for: .touchUpInside)
            }
        }
    }
    @objc func loadImage()
    {
        editimageArrOne = editDictOne["images"] as! [String]
        if(editimageArrOne.count > 0)
        {
            for im in editimageArrOne
            {
                
                DispatchQueue.main.async() {
                    let data = try? Data.init(contentsOf: URL(string : "https://www.localneighborhoodapp.com/assets/rating_review/\(im)")!)
                    
                    print("https://www.localneighborhoodapp.com/assets/rating_review/\(im)")
                    let img = UIImage.sd_image(with: data)
 
                    if(img == nil)
                    {
                        return
                    }

                    
                    self.editimageArrTwo.append(img!)
                }

    }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @objc func buttonClicked(button: UIButton)
    {
        for star in self.starCollection as [UIButton]
        {
            star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
        }

        for i in 0  ... (starCollection.index(of: button)!)
        {
            starCollection[i].setBackgroundImage(UIImage (named: "yellowStar.png"), for: UIControlState.normal)
            rating = starCollection.index(of: button)!
            rating = rating +  1
        }
    }
    
    @IBAction func galleryBtn(_ sender: Any)
    {
        picker2?.delegate = self
        self.openGallary()
    }

    @IBAction func cameraBtn(_ sender: Any)
    {
        picker2?.delegate = self
        self.openCamera()
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker2!.sourceType = UIImagePickerControllerSourceType.camera
            self.present(picker2!, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary))
        {
            picker2!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(picker2!, animated: true, completion: nil)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        var tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        tempImage = self.resize(tempImage!)
        if editStr == "editone" || editStr == "edit"
        {
           editimageArrTwo.append(tempImage!)
        }
        else
        {
           imageArray2.append(tempImage!)
        }
        
        picker.dismiss(animated: true, completion: nil)
        self.imageUpload2CV.reloadData()
    }

    func resize(_ image: UIImage) -> UIImage
    {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        //var compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth
        {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = UIImagePNGRepresentation(img)
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
    
    @IBAction func onSubmit(_ sender: Any)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
//        {
//            //self.PostComment()
//        }
        {
            LoadingIndicatorView.show()
            
            let url = NSURL(string: "https://www.localneighborhoodapp.com/Userajax/ratings_and_review")
            let request = NSMutableURLRequest(url: url! as URL)
            request.httpMethod = "POST"
            let boundary = generateBoundaryString()
            //define the multipart request type
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            var body = Data()
            let fname = "test.png"
            let mimetype = "image/png"
            var parameters : [String : String] = [:]
            var imgarr : [UIImage] = []
            
            if editStr == "editone" || editStr == "edit"
            {
                 parameters = ["owner_id" : "\(self.owneridStr)" , "review" : "\(descriptionText.text!)","title" : "\(titleText.text!)" , "user_id" : "\(self.idStr)" , "rating" : "\(rating)" , "review_id" : "\(self.reviewidStr)"]
                print(parameters)
                imgarr = editimageArrTwo
            }
            else
            {
                parameters = ["owner_id" : "\(self.feedback_Id)" , "review" : "\(descriptionText.text!)","title" : "\(titleText.text!)" , "user_id" : "\(self.user_id)" , "rating" : "\(rating)"]
                imgarr = imageArray2
            }
            
            
            for (key, value) in parameters
            {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
            
            var i : Int = 0
            for imgg in imgarr
            {
                let image_data = UIImagePNGRepresentation(imgg)
                if(image_data == nil)
                {
                    return
                }
                
                //define the data post parameter
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"test\(i)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append("hi\r\n".data(using: String.Encoding.utf8)!)
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"images[]\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(image_data!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
                
                i = i + 1
            }
            
            request.httpBody = body as Data
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest)
            {
                (
                data, response, error) in
                guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                    print("error")
                    return
                }
                // let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                
//                LoadingIndicatorView.hide()
//                for star in self.starCollection as [UIButton]
//                {
//                    star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
//                }
//                self.titleText.text = ""
//                self.descriptionText.text = ""
//                self.editimageArrTwo = []
                LoadingIndicatorView.hide()
                ApiResponse.alert(title: "", message: "Your review has been successfully submitted.", controller: self)
            }
            task.resume()
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
     
    func PostComment()
    {
        //LoadingIndicatorView.show()
        
        let url = NSURL(string: "https://www.localneighborhoodapp.com/Userajax/ratings_and_review")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        //define the multipart request type
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var body = Data()
        let fname = "test.png"
        let mimetype = "image/png"
        var parameters : [String : String] = [:]
        var imgarr : [UIImage] = []

        if editStr == "editone" || editStr == "edit"
        {
 
            

            
            
            parameters = ["owner_id" : "\(self.owneridStr)" , "review" : "\(descriptionText.text!)","title" : "\(titleText.text!)" , "user_id" : "\(self.idStr)" , "rating" : "\(rating)" , "review_id" : "\(self.reviewidStr)"]
            print(parameters)
            imgarr = editimageArrTwo
        }
        else
        {
             parameters = ["owner_id" : "\(self.feedback_Id)" , "review" : "\(descriptionText.text!)","title" : "\(titleText.text!)" , "user_id" : "\(self.user_id)" , "rating" : "\(rating)"]
            imgarr = imageArray2
        }
        
        
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        var i : Int = 0
        for imgg in imgarr
        {
            let image_data = UIImagePNGRepresentation(imgg)
            if(image_data == nil)
            {
                return
            }
            
            //define the data post parameter
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"test\(i)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("hi\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"images[]\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(image_data!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            i = i + 1
        }
        
        request.httpBody = body as Data
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest)
        {
            (
            data, response, error) in
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                print("error")
                return
            }
           // let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            LoadingIndicatorView.hide()
            for star in self.starCollection as [UIButton]
            {
                star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
            }
            self.titleText.text = ""
            self.descriptionText.text = ""
            self.editimageArrTwo = []
            LoadingIndicatorView.hide()
            ApiResponse.alert(title: "", message: "Your review has been successfully submitted.", controller: self)
        }
        task.resume()
    }
    
    
    
    
    
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }

}

class ImageOneCell: UICollectionViewCell
{
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var imgUploadOne: UIImageView!
}

extension FeedbackViewController : UICollectionViewDelegate , UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if editStr == "editone" || editStr == "edit"
        {
            return editimageArrTwo.count
        }
        else
        {
            return imageArray2.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if editStr == "editone" || editStr == "edit"
        {
            let cell : ImageOneCell = imageUpload2CV.dequeueReusableCell(withReuseIdentifier: "imageonecell", for: indexPath) as! ImageOneCell
            cell.imgUploadOne.image = editimageArrTwo[indexPath.item]
            cell.deleteBtn.addTarget(self, action: #selector(deleteReview(_:)), for: .touchUpInside)
            return cell
        }
       else
        {
            let cell : ImageOneCell = imageUpload2CV.dequeueReusableCell(withReuseIdentifier: "imageonecell", for: indexPath) as! ImageOneCell
            cell.imgUploadOne.image = imageArray2[indexPath.row]
            cell.deleteBtn.addTarget(self, action: #selector(deleteReview(_:)), for: .touchUpInside)
            return cell
        }
      }
    
    @objc func deleteReview(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:imageUpload2CV)
        let indexPath1 = imageUpload2CV.indexPathForItem(at: point)
        if editStr == "editone" || editStr == "edit"
        {
            editimageArrTwo.remove(at: (indexPath1?.item)!)
            deleteImg = editimageArrOne[(indexPath1?.item)!]
            self.DeleteReview()
        }
        else {
            imageArray2.remove(at: (indexPath1?.item)!)
            imageUpload2CV.reloadData()
        }
    }
    
    func DeleteReview()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let params = "review_id=\(self.reviewidStr)&owner_id=\(self.owneridStr)&user_id=\(self.idStr)&image=\(self.deleteImg)"
            
            ApiResponse.onResponsePostPhp(url: "Userajax/delete_review_image", parms: params, completion: { (result, error) in
                if (error == "")
                {
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    
                    if (status == true)
                    {
                        OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            self.imageUpload2CV.reloadData()
                        }
                    }
                    else
                    {
                        ApiResponse.alert(title: "", message: message, controller: self)
                    }
                }
                else
                {
                    if (error == Constant.Status_Not_200)
                    {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else
                    {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }

}
 */
//
//  FeedbackViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import FirebaseMessaging
import ReachabilitySwift

class FeedbackViewController: UIViewController , UIImagePickerControllerDelegate ,  UINavigationControllerDelegate , UITextViewDelegate{
    
    
    @IBOutlet var starCollection: [UIButton]!
    @IBOutlet var titleText: UITextField!
    @IBOutlet var descriptionText: UITextView!
    @IBOutlet weak var imageUpload2CV: UICollectionView!
    @IBOutlet weak var submitBtn: KGHighLightedButton!
    
    var feedback_Id : String = ""
    var rating : Int = 0
    var picker2 : UIImagePickerController? = UIImagePickerController()
    var newFrame2 : CGRect!
    var imageArray2 : [UIImage] = []
    var feedBackDict : NSDictionary!
    var user_id : String = ""
    var userDef : UserDefaults!
    
    var editDict : NSDictionary!
    var editDictOne : NSDictionary!
    var editStr : String = ""
    var editimageArr : [String] = []
    var editimageArrOne : [String] = []
    var editimageArrTwo : [UIImage] = []
    var idStr : String = ""
    var owneridStr : String = ""
    var reviewidStr : String = ""
    var deleteImg : String = ""
    var deleteID : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if editStr == "editone" || editStr == "edit"
        {
            self.reviewidStr = editDictOne["id"] as! String
            self.owneridStr = editDictOne["owner_id"] as! String
            self.idStr = editDictOne["user_id"] as! String
            self.titleText.text! = editDictOne["title"] as! String
            self.descriptionText.text! = editDictOne["review"] as! String
            
            editimageArr = editDictOne["images"] as! [String]
            let rate : Int = Int((editDictOne["rating"] as? String)!)!
            
            rating = rate
            if rate > 0
            {
                for i in 0...rate-1
                {
                    self.starCollection[i].setBackgroundImage(UIImage (named: "yellowStar.png"), for: UIControlState.normal)
                }
            }
            else
                
            {
                
                for star in self.starCollection as [UIButton]
                {
                    star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
                }
                
            }
            for star in self.starCollection as [UIButton]
            {
                star.addTarget(self, action: #selector(FeedbackViewController.buttonClicked(button:)), for: .touchUpInside)
            }

            editimageArrOne = editDictOne["images"] as! [String]
            if(editimageArrOne.count > 0)
            {
            for im in editimageArrOne
            {
                let data = try? Data.init(contentsOf: URL(string : "https://www.localneighborhoodapp.com/assets/rating_review/\(im)")!)
                let img = UIImage.sd_image(with: data)
                if(img == nil)
                {
                    return
                }
                else
                {
                     editimageArrTwo.append(img!)
                 }

            }
            }
            self.imageUpload2CV.reloadData()

//            DispatchQueue.main.async {
//                // Update UI
//                self.loadImage()
//            }
//            self.imageUpload2CV.reloadData()

            submitBtn.setTitle("Save", for: UIControlState.normal)
 
            
        }
        else
        {
            descriptionText.delegate = self
            descriptionText.layer.borderWidth = 1.0
            descriptionText.layer.borderColor = UIColor.lightGray.cgColor
            
            for star in self.starCollection as [UIButton]
            {
                star.addTarget(self, action: #selector(FeedbackViewController.buttonClicked(button:)), for: .touchUpInside)
            }
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        self.view.addGestureRecognizer(tapGesture)

    }
    @objc func tap(gesture: UITapGestureRecognizer) {
        descriptionText.resignFirstResponder()
        titleText.resignFirstResponder()

    }

    @objc func loadImage()
    {
        editimageArrOne = editDictOne["images"] as! [String]
        if(editimageArrOne.count > 0)
        {
            for im in editimageArrOne
            {
                
                DispatchQueue.main.async() {
                    let data = try? Data.init(contentsOf: URL(string : "https://www.localneighborhoodapp.com/assets/rating_review/\(im)")!)
                    
                    print("https://www.localneighborhoodapp.com/assets/rating_review/\(im)")
                    let img = UIImage.sd_image(with: data)
                    
                    if(img == nil)
                    {
                        return
                    }
                    
                    
                    self.editimageArrTwo.append(img!)
                }
                
            }
            self.imageUpload2CV.reloadData()

        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @objc func buttonClicked(button: UIButton)
    {
        for star in self.starCollection as [UIButton]
        {
            star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
        }
        
        for i in 0  ... (starCollection.index(of: button)!)
        {
            starCollection[i].setBackgroundImage(UIImage (named: "yellowStar.png"), for: UIControlState.normal)
            rating = starCollection.index(of: button)!
            rating = rating +  1
        }
    }
    
    @IBAction func galleryBtn(_ sender: Any)
    {
        picker2?.delegate = self
        self.openGallary()
    }
    
    @IBAction func cameraBtn(_ sender: Any)
    {
        picker2?.delegate = self
        self.openCamera()
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker2!.sourceType = UIImagePickerControllerSourceType.camera
            self.present(picker2!, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary))
        {
            picker2!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(picker2!, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        var tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        tempImage = self.resize(tempImage!)
        if editStr == "editone" || editStr == "edit"
        {
            editimageArrTwo.append(tempImage!)
        }
        else
        {
            imageArray2.append(tempImage!)
        }
        
        picker.dismiss(animated: true, completion: nil)
        self.imageUpload2CV.reloadData()
    }
    
    func resize(_ image: UIImage) -> UIImage
    {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        //var compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth
        {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = UIImagePNGRepresentation(img)
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
    
    @IBAction func onSubmit(_ sender: Any)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
          
            
            if(rating == 0){
                ApiResponse.alert(title: "Empty", message: "Please set rating", controller: self)
            }else if(titleText.text! ==  "" ){
                ApiResponse.alert(title: "Empty", message: "Please enter the 'Title'", controller: self)
                
            }else if(descriptionText.text! ==  ""){
                ApiResponse.alert(title: "Empty", message: "Please enter the 'Description'", controller: self)
            }else{
                  self.PostComment()
            }
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    func showDeleteAler(){
        let alert = UIAlertController(title: "Deleting the review.", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default,  handler: { action in
            
             self.DeleteReview()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel,  handler: { action in
            
            alert.dismiss(animated: true, completion: nil)
            
            
        }))
        
    }
    
    
    func PostComment()
    {
        LoadingIndicatorView.show()
          let newURL = Constant.BASE_URL + "Userajax/ratings_and_review"
        let url = NSURL(string: newURL)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        //define the multipart request type
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var body = Data()
        let fname = "test.png"
        let mimetype = "image/png"
        var parameters : [String : String] = [:]
        var imgarr : [UIImage] = []
        
        if editStr == "editone" || editStr == "edit"
        {
            parameters = ["owner_id" : "\(self.owneridStr)" , "review" : "\(descriptionText.text!)","title" : "\(titleText.text!)" , "user_id" : "\(self.idStr)" , "rating" : "\(rating)" , "review_id" : "\(self.reviewidStr)"]
            
            imgarr = editimageArrTwo
            
            print(parameters)
            
              print("parameters : \(parameters)" )
        }
        else
        {
            parameters = ["owner_id" : "\(self.feedback_Id)" , "review" : "\(descriptionText.text!)","title" : "\(titleText.text!)" , "user_id" : "\(self.user_id)" , "rating" : "\(rating)"]
            imgarr = imageArray2
        }
        
        
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        var i : Int = 0
        for imgg in imgarr
        {
            let image_data = UIImagePNGRepresentation(imgg)
            if(image_data == nil)
            {
                return
            }
            
            //define the data post parameter
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"test\(i)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("hi\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"images[]\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(image_data!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            i = i + 1
        }
        
        
        
        request.httpBody = body as Data
        
        print("body : \(body)" )
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest)
        {
            (
            data, response, error) in
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                print("error")
                return
            }
            // let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            LoadingIndicatorView.hide()
//            for star in self.starCollection as [UIButton]
//            {
//                star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
//            }
//            self.titleText.text = ""
//            self.descriptionText.text = ""
//            self.editimageArrTwo = []
//            LoadingIndicatorView.hide()
            ApiResponse.alert(title: "", message: "Your review has been successfully submitted.", controller: self)
            self.dismiss(animated: false, completion: nil)

        }
        task.resume()
    }
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
}

class ImageOneCell: UICollectionViewCell
{
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var imgUploadOne: UIImageView!
}

extension FeedbackViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if editStr == "editone" || editStr == "edit"
        {
            return editimageArrTwo.count
        }
        else
        {
            return imageArray2.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if editStr == "editone" || editStr == "edit"
        {
            let cell : ImageOneCell = imageUpload2CV.dequeueReusableCell(withReuseIdentifier: "imageonecell", for: indexPath) as! ImageOneCell
            cell.imgUploadOne.image = editimageArrTwo[indexPath.item]
            cell.deleteBtn.addTarget(self, action: #selector(deleteReview(_:)), for: .touchUpInside)
            cell.deleteBtn.tag = indexPath.item
            return cell
        }
        else
        {
            let cell : ImageOneCell = imageUpload2CV.dequeueReusableCell(withReuseIdentifier: "imageonecell", for: indexPath) as! ImageOneCell
            cell.imgUploadOne.image = imageArray2[indexPath.row]
            cell.deleteBtn.addTarget(self, action: #selector(deleteReview(_:)), for: .touchUpInside)
            cell.deleteBtn.tag = indexPath.row

            return cell
        }
    }
    
    @objc func deleteReview(_ sender: UIButton!)
    {
        deleteID = sender.tag
//        let point : CGPoint = sender.convert(CGPoint.zero, to:imageUpload2CV)
//        let indexPath1 = imageUpload2CV.indexPathForItem(at: point)
        if editStr == "editone" || editStr == "edit"
        {
            editimageArrTwo.remove(at: deleteID)
            deleteImg = editimageArrOne[deleteID]
//            editimageArrTwo.remove(at: (indexPath1?.item)!)
//            deleteImg = editimageArrOne[(indexPath1?.item)!]

           self.showDeleteAler()
        }
        else {
//            imageArray2.remove(at: (indexPath1?.item)!)
            imageArray2.remove(at: deleteID)

            imageUpload2CV.reloadData()
        }
    }
    
    func DeleteReview()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let params = "review_id=\(self.reviewidStr)&owner_id=\(self.owneridStr)&user_id=\(self.idStr)&image=\(self.deleteImg)"
            
            ApiResponse.onResponsePostPhp(url: "Userajax/delete_review_image", parms: params, completion: { (result, error) in
                if (error == "")
                {
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    
                    if (status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                self.imageUpload2CV.reloadData()
                        }
                    }
                    else
                    {
                        ApiResponse.alert(title: "", message: message, controller: self)
                    }
                }
                else
                {
                    if (error == Constant.Status_Not_200)
                    {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else
                    {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
            self.imageUpload2CV.reloadData()

        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
}

