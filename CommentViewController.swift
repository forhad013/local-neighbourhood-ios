//
//  CommentViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 8/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class CommentViewController: UIViewController {
    
    
    @IBOutlet weak var commentTableView: UITableView!
    
    var commentArr1 : [NSDictionary] = []
    var commentTemp : [NSDictionary] = []
    
    var ownerID1 : String = ""
    var backStr : String!
    var userDef : UserDefaults!
    var user_id : String = ""
    //var idStr1 : String = ""
    var idStr1 : Int = 0
    var currentItem : Int = 0
    var selectedItem : Int = -1
    
    
    var isExpanded: [Bool] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"
        
        self.commentTableView.reloadData()
 
        // self.ShowComment()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.async
            {
                self.ShowComment()
        }
    }
    
    
    @IBAction func backToEvent(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func ShowComment(){
        
        LoadingIndicatorView.show()
        let params = "owner_id=\(self.ownerID1)"
        print("params = \(params)")
        
        ApiResponse.onResponsePostPhp(url: "Userajax/get_rating_review", parms: params, completion: { (result, error) in
            
            print("show comment = \(result)")
            if (error == "") {
                
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                print("result = \(result)")
                
                if (status == true)
                {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            //  ApiResponse.alert(title: "Done", message: message, controller: self)
                            self.commentArr1  = result["data"] as! [NSDictionary]
                            self.isExpanded = [Bool](repeating: false, count: self.commentArr1.count)
                            print("commentArr1 ===== \(self.commentArr1)")
                            self.commentTemp = result["data"] as! [NSDictionary]
                            self.commentTableView.reloadData()
                    }
                }
                else {
                    OperationQueue.main.addOperation
                        {
                            
                    }
                    //   ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
        
    }
    
}

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var imageBtn: KGHighLightedButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var review: UILabel!
    @IBOutlet var starCollections4: [UIImageView]!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var BusinessNameLabel: UILabel!
    @IBOutlet weak var descriptionLabelHt: NSLayoutConstraint!
    @IBOutlet var starOneHeightConstraint: NSLayoutConstraint!
    
}

extension CommentViewController : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentArr1.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CommentCell = commentTableView.dequeueReusableCell(withIdentifier: "commentcell")as! CommentCell
        let commDict = commentArr1[indexPath.row]
        let userid = commDict["user_id"] as! String
        if (user_id == userid) {
            cell.editBtn.isHidden = false
            cell.deleteBtn.isHidden = false
            
            cell.deleteBtn.tag = Int((commDict["id"]  as? String)!)!
            cell.editBtn.tag = indexPath.row
            
            cell.deleteBtn.addTarget(self, action: #selector(deleteReview(_:)), for: .touchUpInside)
            cell.editBtn.addTarget(self, action: #selector(editDetail(_:)), for: .touchUpInside)
            
            //cell.editBtn.tag = Int((commDict["id"]  as? String)!)!
            
        
            //cell.editBtn.addTarget(self, action: #selector(editDetail), for: .touchUpInside)
            
            
            
        }
        else {
            cell.editBtn.isHidden = true
            cell.deleteBtn.isHidden = true
        }
        
        // idStr1 = commDict["id"] as! String
        
        if(commDict["user_name"] == nil){
            cell.nameLabel.text! = ""
        }
        else{
            cell.nameLabel.text! = (commDict["user_name"] as? String)!
            
        }
        cell.review.text!  = (commDict["title"] as? String)!
        cell.BusinessNameLabel.text!  = (commDict["businessname"] as? String)!
        ApiResponse.callRating(Double((commDict["rating"] as? String)!)!, starRating: cell.starCollections4)
        
        if Double((commDict["rating"]!) as! String) == 0 {
            cell.starCollections4[0].isHidden = true
            cell.starCollections4[1].isHidden = true
            cell.starCollections4[2].isHidden = true
            cell.starCollections4[3].isHidden = true
            cell.starCollections4[4].isHidden = true
            cell.starOneHeightConstraint.constant = 0
        }
        else {
            cell.starCollections4[0].isHidden = false
            cell.starCollections4[1].isHidden = false
            cell.starCollections4[2].isHidden = false
            cell.starCollections4[3].isHidden = false
            cell.starCollections4[4].isHidden = false
            cell.starOneHeightConstraint.constant = 16
        }
        
        cell.layer.borderColor =  UIColor.init(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1.0).cgColor
        cell.layer.borderWidth = 4
        cell.clipsToBounds = true
        let ht = ApiResponse.calculateHeightForString((commDict["review"] as! String))
        //cell.descriptionLabelHt.constant = ht
        //cell.descriptionLabel.text!  = (commDict["review"] as? String)!
        
        
        
        if self.isExpanded[indexPath.row] == true {
            cell.descriptionLabel.text!  = (commDict["review"] as? String)!
            
            cell.descriptionLabel.numberOfLines = 0
            
            print("TRUE")
            
        }
        else {
            cell.descriptionLabel.text!  = (commDict["review"] as? String)!
            cell.descriptionLabel.numberOfLines = 1
            
            
            
            
            
            
            let readmoreFontColor = UIColor.blue
            print(cell.descriptionLabel.text!)
            
            // if((cell.descriptionLabel.text?.count)! > 40)
            
            DispatchQueue.main.async {
                //                    cell.descriptionLabel1.addTrailing(with: "... ", moreText: "more", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
                
                if((cell.descriptionLabel.text?.count)! > 40)
                {
                    
                    cell.descriptionLabel.addTrailing1(with: "  ", moreText: "Read more", moreTextFont: cell.descriptionLabel.font!, moreTextColor: readmoreFontColor)
                }
                
                
            }
            
            
            //            if((cell.descriptionLabel.text?.count)! > 20)
            //
            //            {
            //                print("READMORE")
            //               // print(cell.descriptionLabel.text!)
            //
            //                cell.descriptionLabel.addTrailing1(with: "...", moreText: "Readmore", moreTextFont: cell.descriptionLabel.font!, moreTextColor: readmoreFontColor)
            //            }
            
        }
        let imagesArray = commDict["images"]
        if ((imagesArray as AnyObject).count)! <= 0 {
            cell.imageBtn.isHidden = true
        }
        else {
            cell.imageBtn.isHidden = false
        }
        
        cell.imageBtn.tag = indexPath.row
        
        
        cell.imageBtn.addTarget(self, action: #selector(imagedetail(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func deleteReview(_ sender: UIButton!)
    {
        
        // idStr1 = commDict["id"] as! String
        idStr1 = sender.tag
        
        //        let point : CGPoint = sender.convert(CGPoint.zero, to:commentTableView)
        //        let indexPath1 = commentTableView.indexPathForRow(at: point)
        //        let dictNoti1 = self.commentArr1[(indexPath1?.row)!]
       
        
        self.showDeleteAler()
        
    }
    
    
    func showDeleteAler(){
        let alert = UIAlertController(title: "Deleting the review.", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default,  handler: { action in
             self.DeleteReview()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel,  handler: { action in
            
            alert.dismiss(animated: true, completion: nil)
            
        }))
           self.present(alert, animated: true, completion: nil)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @objc func editDetail(_ sender: UIButton!)
    {
        currentItem = sender.tag
        let dictNoti = self.commentArr1[currentItem]
        // print(dictNoti)
        let edit1 = self.storyboard?.instantiateViewController(withIdentifier: "feedbackvw") as! FeedbackViewController
        edit1.editDictOne = dictNoti
        edit1.editStr = "editone"
        //edit1.editStr = "editone"
        
        self.present(edit1, animated: true, completion: nil)
    }
    
    func DeleteReview(){
        
        let params = "review_id=\(self.idStr1)"
        
        ApiResponse.onResponsePostPhp(url: "Userajax/delete_rating", parms: params, completion: { (result, error) in
            
            print("show comment = \(result)")
            if (error == "")
            {
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                print("result = \(result)")
                
                if (status == true) {
                    
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            self.ShowComment()
                            ApiResponse.alert(title: "Done", message: message, controller: self)
                            
                    }
                }
                else {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                    }
                    //   ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else
            {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
        
    }
    
    
    @objc func imagedetail(_ sender: UIButton!)
    {
        currentItem = sender.tag
        
        
        //        let point : CGPoint = sender.convert(CGPoint.zero, to:commentTableView)
        //        let indexPath1 = commentTableView.indexPathForRow(at: point)
        let dictNoti1 = self.commentArr1[currentItem]
        let showImage = self.storyboard?.instantiateViewController(withIdentifier: "imageviewcontroller") as! ImageViewController
        showImage.commentDict = dictNoti1
        showImage.imgStr = "commentStr"
        self.present(showImage, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let commDict = commentArr1[indexPath.row]
        let ht = ApiResponse.calculateHeightForString((commDict["review"] as! String))
        //                return ht + 123
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        
        let vw = UIView.init(frame: CGRect(x : 0, y : 0, width : commentTableView.frame.width, height : 10))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.isExpanded[indexPath.row] = !self.isExpanded[indexPath.row]
        self.commentTableView.reloadData()
        
        
    }
    
    func Deletereview(){
        
        //        event_id:2
        LoadingIndicatorView.show()
        
        let params = "review_id=\(self.idStr1)"
        
        ApiResponse.onResponsePostPhp(url: "Userajax/delete_rating", parms: params, completion: { (result, error) in
            
            print("show comment = \(result)")
            if (error == "")
            {
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                print("result = \(result)")
                
                if (status == true) {
                    
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            let data  = result["data"] as! NSDictionary
                            self.commentTableView.reloadData()
                            ApiResponse.alert(title: "Done", message: message, controller: self)
                    }
                }
                else {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                    }
                    //   ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
        
    }
    
}
extension UILabel {
    
    func addTrailing1(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText
        
        let lengthForVisibleString: Int = self.vissibleTextLength1
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        var answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedStringKey.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedStringKey.font: moreTextFont, NSAttributedStringKey.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        
        
        
        self.attributedText = answerAttributed
        
    }
    
    var vissibleTextLength1: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        
        let attributes: [AnyHashable: Any] = [NSAttributedStringKey.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedStringKey : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
        
        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedStringKey : Any], context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
    }
}


