
/*
//
//  ImageCard.swift
//  CardSlider
//
//  Created by Saoud Rizwan on 2/27/17.
//  Copyright © 2017 Saoud Rizwan. All rights reserved.
//

import UIKit

class ImageCard: CardView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // image
        
        let imageView = UIImageView(image: UIImage(named: "dummy_image"))
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor(red: 67/255, green: 79/255, blue: 182/255, alpha: 1.0)
        imageView.layer.cornerRadius = 5
        imageView.tag = 11
        imageView.layer.masksToBounds = true
        
        imageView.frame = CGRect(x: 12, y: 12, width: self.frame.width - 24, height: self.frame.height - 103)
        self.addSubview(imageView)
        
        // dummy text boxes
        
       // let textBox1 = UIView()
      //  textBox1.backgroundColor = UIColor(red: 67/255, green: 79/255, blue: 182/255, alpha: 1.0)
      //  textBox1.layer.cornerRadius = 12
      //  textBox1.layer.masksToBounds = true
        
      //  textBox1.frame = CGRect(x: 12, y: imageView.frame.maxY + 15, width: 200, height: 24)
      //  self.addSubview(textBox1)
        
      //  let textBox2 = UIView()
     //   textBox2.backgroundColor = UIColor(red: 67/255, green: 79/255, blue: 182/255, alpha: 1.0)
      //  textBox2.layer.cornerRadius = 12
      //  textBox2.layer.masksToBounds = true
        
      //  textBox2.frame = CGRect(x: 12, y: textBox1.frame.maxY + 10, width: 120, height: 24)
      //  self.addSubview(textBox2)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

*/

 //
 //  ImageCard.swift
 //  CardSlider
 //
 //  Created by Saoud Rizwan on 2/27/17.
 //  Copyright © 2017 Saoud Rizwan. All rights reserved.
 //
 
 import UIKit
 
 class ImageCard: CardView {
 
     override init(frame: CGRect) {
         let screenSize = UIScreen.main.bounds
         let screenWidth = screenSize.width
         let screenHeight = screenSize.height
         //let f = CGRect(x: 20, y: 30, width: screenWidth - 40, height: screenHeight - 150)
        //let f = CGRect(x: 20, y: 30, width: screenWidth - 40, height: screenHeight - 200)
        if UIDevice.isIphoneX {
            // is iPhoneX
            let f = CGRect(x: 20, y: 30, width: screenWidth - 40, height: screenHeight - 200)
            super.init(frame: f)

        } else {
            // is not iPhoneX
            let f = CGRect(x: 20, y: 20, width: screenWidth - 20, height: screenHeight - 150)
            super.init(frame: f)

        }

        
         // image
         self.alpha = 1
         self.backgroundColor = UIColor.white
        
         let imageView = UIImageView(image: UIImage(named: "dummy_image"))
         imageView.contentMode = .scaleToFill
         //imageView.backgroundColor = UIColor(red: 67/255, green: 79/255, blue: 182/255, alpha: 1.0)
        imageView.backgroundColor = UIColor.white

         imageView.layer.cornerRadius = 5
         imageView.tag = 11
         imageView.layer.masksToBounds = true
        
         imageView.frame = CGRect(x: 12, y: 12, width: self.frame.width - 24, height: screenHeight/2)
         self.addSubview(imageView)
        
        let titleTextLabel = UILabel()
        titleTextLabel.tag = 99999
        titleTextLabel.frame = CGRect(x: 12, y: imageView.frame.maxY + 5, width: self.frame.width - 24, height: 40)
        titleTextLabel.textAlignment = .natural
        self.addSubview(titleTextLabel)
//        titleTextLabel.lineBreakMode = .byWordWrapping
//        titleTextLabel.numberOfLines = 0;
        titleTextLabel.adjustsFontSizeToFitWidth = true


        titleTextLabel.backgroundColor = UIColor.clear
        titleTextLabel.font = UIFont(name: "Arial-BoldMT", size: 21)
        titleTextLabel.textColor = UIColor.darkGray
        titleTextLabel.text = "Dummy Header"
        
        let descriptionTextLabel = UILabel()
//        descriptionTextLabel.frame = CGRect(x: 12, y: titleTextLabel.frame.maxY + 5, width: self.frame.width - 24, height: (screenHeight/2) - 150 - 40)
        
//        descriptionTextLabel.frame = CGRect(x: 12, y: titleTextLabel.frame.maxY + 5, width: self.frame.width - 24, height: (screenHeight/2) - 150 - 40)
        
        
        if UIDevice.isIphoneX {
            // is iPhoneX
            descriptionTextLabel.frame = CGRect(x: 12, y: titleTextLabel.frame.origin.y + 50, width: self.frame.width - 24, height: (screenHeight/2) - 200 - 40)

        } else {
            // is not iPhoneX
//            descriptionTextLabel.frame = CGRect(x: 12, y: titleTextLabel.frame.maxY + 5, width: self.frame.width - 24, height: (screenHeight/2) - 150 - 20)
            descriptionTextLabel.frame = CGRect(x: 12, y: titleTextLabel.frame.maxY - 15, width: self.frame.width - 24, height: (screenHeight/2) - 150 - 40)
 
 
        }



        self.addSubview(descriptionTextLabel)
        descriptionTextLabel.tag = 999999
        descriptionTextLabel.numberOfLines = 0
        descriptionTextLabel.backgroundColor = UIColor.clear
        descriptionTextLabel.textColor = UIColor.darkGray
        descriptionTextLabel.font = UIFont(name: "arial", size: 17)
        descriptionTextLabel.text = "Here is the text here is the text Here is the text here is the text Here is the text here is the text "
        
        
         // dummy text boxes
        
         //        let textBox1 = UIView()
         //        textBox1.backgroundColor = UIColor(red: 67/255, green: 79/255, blue: 182/255, alpha: 1.0)
         //        textBox1.layer.cornerRadius = 12
         //        textBox1.layer.masksToBounds = true
         //
         //        textBox1.frame = CGRect(x: 12, y: imageView.frame.maxY + 15, width: 200, height: 24)
         //        self.addSubview(textBox1)
         //
         //        let textBox2 = UIView()
         //        textBox2.backgroundColor = UIColor(red: 67/255, green: 79/255, blue: 182/255, alpha: 1.0)
         //        textBox2.layer.cornerRadius = 12
         //        textBox2.layer.masksToBounds = true
         //
         //        textBox2.frame = CGRect(x: 12, y: textBox1.frame.maxY + 10, width: 120, height: 24)
         //        self.addSubview(textBox2)
     }
    
     required init?(coder aDecoder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
 
 }
extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}

