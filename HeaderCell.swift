//
//  HeaderCell.swift
//  FairField
//
//  Created by Prashant Shinde on 7/13/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
   
    @IBOutlet var toggleButton: UIButton!
    
}
