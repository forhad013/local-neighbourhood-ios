//
//  ShowSavedCouponViewController.swift
//  Local Neighbourhood App
//
//  Created by Danish Naeem on 12/3/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class SlideCouponViewController: UIViewController {
    @IBOutlet weak var bannerimageView: UIImageView!

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var couponView: UIView!


    var descriptionString: String? = nil
    var imageString: String? = nil
    var currentID: String? = nil

    var labelString: String? = nil
    var currentParent: String? = nil

    @IBOutlet weak var titleLab: UILabel!
    var allCouponArr : [NSDictionary] = []
     var currentItem : Int = 0

 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
     }
     @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right")
            if(currentItem > 0)
            {
                currentItem = currentItem - 1
                self.loadData()
                
                
            }

        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left")
            if(currentItem < allCouponArr.count - 1 )
            {
                currentItem = currentItem + 1
                self.loadData()
                
                
            }

        }
    }

    func loadData () {
        let newsDict = allCouponArr[currentItem]
 
        if currentParent == "banner" {
            

        imageString = newsDict["image"] as? String
        descriptionString = newsDict["description"] as? String
        labelString = newsDict["banner_name"] as? String
            
            bannerimageView.isHidden = false
            self.imageView.isHidden = true
            self.textView.isHidden = true
            self.titleLab.isHidden = true
            bannerView.isHidden = false
            couponView.isHidden = true

        if imageString != nil {
            self.bannerimageView.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString!)"))
        }
        if descriptionString != nil {
            self.textView.text = descriptionString!
        }
        if labelString != nil {
            self.titleLab.text = labelString!
        }
        }
        else
        {
            bannerView.isHidden = true
            couponView.isHidden = false

            bannerimageView.isHidden = true
            imageString = newsDict["image"] as? String
            descriptionString = newsDict["description"] as? String
            labelString = newsDict["coupon_name"] as? String
            if imageString != nil {
                self.imageView.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/coupon/\(imageString!)"))
            }
            if descriptionString != nil {
                self.textView.text = descriptionString!
            }
            if labelString != nil {
                self.titleLab.text = labelString!
            }

            
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

