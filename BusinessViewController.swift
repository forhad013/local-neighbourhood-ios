//
//  BusinessViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class BusinessViewController: UIViewController , UISearchBarDelegate {
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var businessTable: UITableView!
    @IBOutlet weak var searchBarHt: NSLayoutConstraint!
    
    var titleArray : [String] = []
    var filtered:[String] = []
    var data : [String] = []
    var idarray : [[String]] = []
    var userid: String = ""
    
    struct Section {
        var name: String!
        var items: [String]!
        var collapsed: Bool!
        
        init(name: String, items: [String], collapsed: Bool = true) {
            self.name = name
            self.items = items
            self.collapsed = collapsed
        }
    }
    var sections = [Section]()
    var sections12 = [Section]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dic = UserDefaults.standard.value(forKey: "userInfo") as! NSDictionary
        userid = dic.value(forKey: "user_id")! as! String
        
        searchBar.isHidden = true
        searchBarHt.constant = 0
        searchBar.layer.cornerRadius = 20.0
        searchBar.layer.borderColor = UIColor.lightGray.cgColor
        searchBar.layer.borderWidth = 1.0
        
        searchBar.delegate = self
        //  self.businessList()
        self.businessListAlamo()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = titleArray.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0) {
            sections = sections12
        } else {
            sections = []
            for section11 in sections12 {
                for filtr in filtered {
                    if (filtr == section11.name) {
                        sections.append(section11)
                    }
                }
            }
        }
        self.businessTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBack(_ sender: Any) {
         self.dismiss(animated: false, completion: nil)
 
        let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
        
        self.present(dashboard, animated: true, completion: nil)
    }
    
    
    
    func businessListAlamo(){
        
        let CATEGORY_LIST = Constant.BASE_URL + "Userajax/category_list";
        
        let params : [String : String] = ["" : "" ]
        
        
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            let pstring = ""
            
            Alamofire.request(CATEGORY_LIST,method:.post,parameters:params).responseJSON {
                
                response in
                if response.result.isSuccess{
                    let value = response.value as! NSDictionary
                      print(value)
                   // let message = value["message"].string
                    let status = value["status"] as! Bool
                    let message = value["message"] as! String
                   if(status == true)
                    {
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            let data1 = value["data"] as! [NSDictionary]
                             for dict in data1 {
                             let nameStr = dict["name"] as! String
                             self.titleArray.append(nameStr)
                             let subcatg = dict["subcategories"] as! [NSDictionary]
                             var subcatgArray : [String] = []
                             var idArray : [String] = []
                             for sub in subcatg {
                             subcatgArray.append(sub["name"] as! String)
                             idArray.append(sub["id"] as! String)
                             }
                             self.idarray.append(idArray)
                             self.sections.append(Section.init(name: nameStr, items: subcatgArray))
                             }
                            
                            self.sections12 = self.sections
                            self.businessTable.reloadData()
                        }
                        
                    }
                    else{
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                    }else{
                    print("Error \(String(describing: response.result.error))")
                    
                    
                    ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    
                    
                }
            }
        }else{
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
        
        
    }
    
    func jsonToString(json: AnyObject){
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            print(convertedString ?? "defaultvalue")
        } catch let myJSONError {
            print(myJSONError)
        }
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
     func businessList(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            let pstring = ""
            ApiResponse.onResponsePostPhp(url: "Userajax/category_list", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true)
                    {
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            
                            let data1 = result["data"] as! [NSDictionary]
                            for dict in data1 {
                                let nameStr = dict["name"] as! String
                                self.titleArray.append(nameStr)
                                let subcatg = dict["subcategories"] as! [NSDictionary]
                                var subcatgArray : [String] = []
                                var idArray : [String] = []
                                for sub in subcatg {
                                    subcatgArray.append(sub["name"] as! String)
                                    idArray.append(sub["id"] as! String)
                                }
                                self.idarray.append(idArray)
                                self.sections.append(Section.init(name: nameStr, items: subcatgArray))
                            }
                            self.sections12 = self.sections
                            self.businessTable.reloadData()
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
}

extension BusinessViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        
        // For section 1, the total count is items count plus the number of headers
        var count = sections.count
        
        for section in sections {
            count += section.items.count
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "title") as UITableViewCell!
            // cell?.backgroundColor = UIColor.white
            cell?.backgroundColor = UIColor.clear
            
            return cell!
        }
        
        // Calculate the real section index and row index
        let section = getSectionIndex(indexPath.row)
        let row = getRowIndex(indexPath.row)
        
        if row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderCell
            
            cell.titleLabel.text = sections[section].name
            
            cell.toggleButton.tag = section
            
            cell.toggleButton.setTitle(sections[section].collapsed! ? "▼" : "▲", for: UIControlState())
            cell.toggleButton.addTarget(self, action: #selector(BusinessViewController.toggleCollapse(_:)), for: .touchUpInside)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
            cell?.textLabel?.text = sections[section].items[row - 1]
            cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 0
        }
        
        // Calculate the real section index and row index
        let section = getSectionIndex(indexPath.row)
        let row = getRowIndex(indexPath.row)
        
        // Header has fixed height
        if row == 0 {
            return 49.0
        }
        return sections[section].collapsed! ? 0 : 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderCell
        
        let section = getSectionIndex(indexPath.row)
        let row = getRowIndex(indexPath.row)
        
        if row == 0 {
            self.toggleCollapse(index: section)
        }
        else {
            if(idarray.count != 0)  {
                
                let idStr = idarray[section][row - 1]
                let prov = self.storyboard?.instantiateViewController(withIdentifier: "provider") as! ProviderViewController
                prov.passId = idStr
                self.present(prov, animated: true, completion: nil)
            }
            else {
                print(" no data")
            }
        }
    }
    
    func toggleCollapse(index: Int) {
        let section = index
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed!
        
        let indices = getHeaderIndices()
        
        let start = indices[section]
        let end = start + sections[section].items.count
        
        businessTable.beginUpdates()
        for i in start ..< end + 1 {
            businessTable.reloadRows(at: [IndexPath(row: i, section: 1)], with: .automatic)
        }
        businessTable.endUpdates()
    }
    
    //
    // MARK: - Event Handlers
    //
    @objc func toggleCollapse(_ sender: UIButton) {
        let section = sender.tag
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed!
        
        let indices = getHeaderIndices()
        
        let start = indices[section]
        let end = start + sections[section].items.count
        
        businessTable.beginUpdates()
        for i in start ..< end + 1 {
            businessTable.reloadRows(at: [IndexPath(row: i, section: 1)], with: .automatic)
        }
        businessTable.endUpdates()
    }
    
    //
    // MARK: - Helper Functions
    //
    func getSectionIndex(_ row: NSInteger) -> Int {
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                return i
            }
        }
        
        return -1
    }
    
    func getRowIndex(_ row: NSInteger) -> Int {
        var index = row
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                index -= indices[i]
                break
            }
        }
        
        return index
    }
    
    func getHeaderIndices() -> [Int] {
        var index = 0
        var indices: [Int] = []
        
        for section in sections {
            indices.append(index)
            index += section.items.count + 1
        }
        
        return indices
    }
    
}

