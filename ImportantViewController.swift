//
//  ImportantViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/29/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class ImportantViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var importantTable: UITableView!
         
    @IBOutlet weak var navigationInfo: UINavigationBar!
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var webViewPage: UIWebView!
    @IBOutlet weak var imageViewForAds: UIImageView!
    
    var importantArr : [NSDictionary] = []
    var advertisementArr11 : [NSDictionary] = []
    var scrollingTimer11 = Timer()
    var ownerId11 : String = ""
    var htArray : [CGFloat] = []
    var backStr : String = ""
    var userid: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       viewOne.isHidden = true
        let dic = UserDefaults.standard.value(forKey: "userInfo") as! NSDictionary
        userid = dic.value(forKey: "user_id")! as! String

        
       self.getInfo()
       showAdvertisement3()
       addGestureToAdsImageView()

    }
    
    func addGestureToAdsImageView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(adsImageViewAction))
        self.imageViewForAds.isUserInteractionEnabled = true
        self.imageViewForAds.tag = -1
        self.imageViewForAds.addGestureRecognizer(tapGesture)
    }
    
    private var adsImageIndex = 0
    
    @objc func startTimer(theTimer : Timer){
        //        UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseOut, animations: {
        //            self.addCollectionViewThree.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int , section:0), at: .centeredHorizontally, animated: false)
        //        }, completion: nil)
        setupForAdsImages()
    }
    
   
    func stopTimerTest() {
        //if scrollingTimer11 != nil {
            scrollingTimer11.invalidate()
          //  scrollingTimer11 = nil
       // }
    }
    
    
    func setupForAdsImages() {
        if adsImageIndex >= advertisementArr11.count {
            adsImageIndex = 0
        }
        let dict1 = advertisementArr11[adsImageIndex]
        self.imageViewForAds.tag = adsImageIndex
        adView(userid: userid, bannerid: dict1.value(forKey: "id")! as! String , ownerid: dict1.value(forKey: "owner_id")! as! String)
        adsImageIndex = adsImageIndex + 1
        let imageString = dict1["image"] as! String
        //print("imageString = \(imageString)")
        imageViewForAds.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
        
        
    }
    
    @objc func adsImageViewAction(sender: UITapGestureRecognizer!) {
        print("adsImageView clicked \((sender.view?.tag)!)")
        if (sender.view?.tag)! != -1 {
            let dict2 = advertisementArr11[(sender.view?.tag)!]
            adClick(userid: userid, bannerid: dict2.value(forKey: "id")! as! String, ownerid: dict2.value(forKey: "owner_id")! as! String)
            self.ownerId11 = (dict2["owner_id"] as? String)!
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (backStr != "") {
            backStr = ""
            viewOne.isHidden = true
//            let parent = view.superview
//            view.removeFromSuperview()
//            view = nil
            navigationInfo.topItem?.title = "Important Info"

//            parent?.addSubview(view)
        }
        else {
            stopTimerTest()
             self.dismiss(animated: false, completion: nil)
        }
    }

    @IBAction func backAction(_ sender: Any)
    {
      // viewOne.isHidden = true
    }
    
    func getInfo()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            let pstring = "user_id="
            ApiResponse.onResponsePostPhp(url: "Userajax/importants", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                        {
                            self.importantArr = result["data"] as! [NSDictionary]
                            self.importantTable.reloadData()
                            LoadingIndicatorView.hide()
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    func adClick(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/banner_click", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                // self.importantArr = result["data"] as! [NSDictionary]
                                //self.importantTable.reloadData()
                                //  LoadingIndicatorView.hide()
                                self.addDetailFive()
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    func adView(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/save_page_viewer", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                               // self.importantArr = result["data"] as! [NSDictionary]
                               //self.importantTable.reloadData()
                              //  LoadingIndicatorView.hide()
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    func showAdvertisement3()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let pstring = ""
            ApiResponse.onResponsePostPhp(url: "Userajax/banners", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    
                    if(status == true){
                        OperationQueue.main.addOperation {
                            self.advertisementArr11 = result["data"] as! [NSDictionary]
                            if self.advertisementArr11.count != 0
                            {
//                                self.addCollectionViewFive.reloadData()
                                self.setupForAdsImages()
                                self.scrollingTimer11 = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: nil, repeats: true)
                                self.scrollingTimer11.fire()
                            } else {
//                                self.addCollectionViewFive.isHidden = true
                            }
                        }
                    }
                    else
                    {
                    }
                }
                else
                {
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else
                    {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
           ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
}

class Importantableviewcell : UITableViewCell {

    @IBOutlet weak var iImage: UIImageView!
    @IBOutlet weak var iDepartment: UILabel!
    @IBOutlet weak var iEmail: UIButton!
    @IBOutlet weak var iWebsite: UIButton!
    @IBOutlet weak var iFacebook: UIButton!
    @IBOutlet weak var iContact: UIButton!
    @IBOutlet weak var iEmailLabel: UILabel!
    @IBOutlet weak var iEmailLabelHt: NSLayoutConstraint!
    
    @IBOutlet weak var iEmailHt: NSLayoutConstraint!
    @IBOutlet weak var iWebsiteHt: NSLayoutConstraint!
    @IBOutlet weak var iFacebookHt: NSLayoutConstraint!
    @IBOutlet weak var iContactHt: NSLayoutConstraint!
    
}

extension ImportantViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return importantArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : Importantableviewcell = importantTable.dequeueReusableCell(withIdentifier: "importantableviewcell") as! Importantableviewcell
        let reviewDict  = importantArr[indexPath.item]
        cell.iDepartment.text! =  "\((reviewDict["department"] as? String)!)"
        
        let email = (reviewDict["email"] as? String)!
        
        if email != ""
        {
            cell.iEmail.setTitle("\(email)", for: .normal)
        }
        else
        {
            cell.iEmailHt.constant = 0
            cell.iEmail.isHidden = true
            cell.iEmailLabelHt.constant = 0
            cell.iEmailLabel.isHidden = true
        }
        
        cell.iWebsite.setTitle("\((reviewDict["website"] as? String)!)", for: .normal)
        cell.iFacebook.setTitle("\((reviewDict["facebook"] as? String)!)", for: .normal)
        cell.iContact.setTitle("\((reviewDict["contact_no"] as? String)!)", for: .normal)
        
        cell.iImage.layer.cornerRadius = cell.iImage.frame.size.width/2
        cell.iImage.clipsToBounds = true
        let imageString = reviewDict["logo"] as! String
        cell.iImage.sd_setImage(with: URL(string : "\(imageString)"))
        
        cell.layer.borderColor =  UIColor.init(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1.0).cgColor
        cell.layer.borderWidth = 4
        cell.clipsToBounds = true
        
        cell.iFacebook.addTarget(self, action: #selector(facebookView(_:)), for: .touchUpInside)
        cell.iContact.addTarget(self, action: #selector(contact(_:)), for: .touchUpInside)
        cell.iWebsite.addTarget(self, action: #selector(website(_:)), for: .touchUpInside)
        cell.iEmail.addTarget(self, action: #selector(gmail(_:)), for: .touchUpInside)
        
        return cell
    
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        
        let vw = UIView.init(frame: CGRect(x : 0, y : 0, width : importantTable.frame.width, height : 30))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        let cell = importantTable.cellForRow(at: indexPath) as! Importantableviewcell
//          let ht = htArray[indexPath.row]
//        cell.iEmail.frame.height + cell.iWebsite.frame.height + cell.iFacebook.frame.height + cell.iContact.frame.height + 25
////        let dict1 = importantArr[indexPath.row]
////        let ht1 = ApiResponse.calculateSizeForString((dict1["website"] as? String)!)
////        let ht2 = ApiResponse.calculateSizeForString((dict1["facebook"] as? String)!)
////        let ht3 = ApiResponse.calculateSizeForString((dict1["contact_no"] as? String)!)
////        let ht4 = ApiResponse.calculateSizeForString((dict1["email"] as? String)!)
//        
//        return 138 
//    }
    
    @objc func contact(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:importantTable)
        
        let indexPath1 = importantTable.indexPathForRow(at: point)
        let dictNoti = importantArr[(indexPath1?.row)!]
        var number1 =  dictNoti["contact_no"] as? String
        number1 = number1?.replacingOccurrences(of: " ", with: "")
        let url : NSURL = URL(string : "tel://\(number1!)" )! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
   
    }

    @objc func website(_ sender: UIButton!)
    {
        backStr = "web"
        let point : CGPoint = sender.convert(CGPoint.zero, to:importantTable)
        viewOne.isHidden = false
        webViewPage.delegate  = self
        let indexPath1 = importantTable.indexPathForRow(at: point)
        let dictNoti = importantArr[(indexPath1?.row)!]
        let url =  dictNoti["website"] as? String
        let url1 = URL(string: "\(String(describing: url!))")
        navigationInfo.topItem?.title = "WebSite"
        if UIApplication.shared.canOpenURL(url1!) {
            let request = NSURLRequest(url: url1!);
            webViewPage.loadRequest(request as URLRequest);
        }
    }
    
    /// ** WEB VIEW DELEGATE ** ///
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingIndicatorView.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingIndicatorView.hide()
    }
    
    @objc func facebookView(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:importantTable)
        viewOne.isHidden = true
        let indexPath1 = importantTable.indexPathForRow(at: point)
        _ = importantArr[(indexPath1?.row)!]
        let fbgroup = self.storyboard?.instantiateViewController(withIdentifier: "facebookvc") as! FaceBookVC
        self.present(fbgroup, animated: true, completion: nil)
    }
    
    @objc func gmail(_ sender: UIButton!)
    {
       // let point : CGPoint = sender.convert(CGPoint.zero, to:importantTable)
        let url : NSURL = URL(string : "https://accounts.google.com" )! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }

}

extension ImportantViewController {// : UICollectionViewDelegate , UICollectionViewDataSource {
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return self.advertisementArr11.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell : AddBannerCell = addCollectionViewFive.dequeueReusableCell(withReuseIdentifier: "addbannercell", for: indexPath) as! AddBannerCell
//
//        let dict1 = advertisementArr11[indexPath.item]
//        let imageString = dict1["image"] as! String
//        cell.bannerImageOne.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
//
//        var rowIndex = indexPath.row
//        let numberofRecords : Int = self.advertisementArr11.count - 1
//        if (rowIndex < numberofRecords) {
//            rowIndex = (rowIndex + 1)
//
//        }else {
//            rowIndex = 0
//
//        }
//
//
//        scrollingTimer11 = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(ImportantViewController.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)
//
//        return cell
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        let dict2 = advertisementArr11[indexPath.row]
//        self.ownerId11 = (dict2["owner_id"] as? String)!
//        self.addDetailFive()
//    }
    
    func addDetailFive(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let pstring = "owner_id=\(self.ownerId11)"
            ApiResponse.onResponsePostPhp(url: "Userajax/get_business_owner", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    if(status == true)
                    {
                        OperationQueue.main.addOperation
                        {
                            let dict5 = result["data"] as! NSDictionary
                            let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                            dictProvider.providerdetailDict = dict5
                            self.present(dictProvider, animated: true, completion: nil)
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
             ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
}

class AddBannerCell : UICollectionViewCell {
    @IBOutlet weak var bannerImageOne: UIImageView!

}
