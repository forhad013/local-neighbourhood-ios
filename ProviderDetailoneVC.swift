//
//  ProviderDetailoneVC.swift
//  FairField
//
//  Created by Prashant Shinde on 8/10/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import FirebaseMessaging
import ReachabilitySwift
import Alamofire

class ProviderDetailoneVC: UIViewController , UIImagePickerControllerDelegate ,  UINavigationControllerDelegate ,UITextFieldDelegate , UITextViewDelegate {
    
    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessAddress: UILabel!
    @IBOutlet var starCollection4: [UIImageView]!
    @IBOutlet weak var businessEmail: UILabel!
    @IBOutlet weak var businessDescription: UILabel!
    @IBOutlet weak var businessDescriptionHt: NSLayoutConstraint!
    @IBOutlet weak var businessDescriptionView: UIView!
    @IBOutlet weak var businessDescriptionViewHt: NSLayoutConstraint!
    @IBOutlet weak var setTitle: UITextField!
    @IBOutlet weak var setDescription: UITextView!
    @IBOutlet weak var phoneImg: UIImageView!
    @IBOutlet weak var webImage: UIImageView!
    @IBOutlet weak var mailImage: UIImageView!
    @IBOutlet var starsBtn: [UIButton]!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var couponLabel: UILabel!
    @IBOutlet weak var bannerLabel: UILabel!
    @IBOutlet weak var couponCollectionView: UICollectionView!
    @IBOutlet weak var couponHeight: NSLayoutConstraint!
    @IBOutlet weak var showCommentTableView: UITableView!
    @IBOutlet weak var imageUploadCV: UICollectionView!
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var commentHeight3: NSLayoutConstraint!
    @IBOutlet weak var reviewLAbelHt: NSLayoutConstraint!
    @IBOutlet weak var numberBtn: UIButton!
    @IBOutlet weak var webSiteBtn: UIButton!
    @IBOutlet weak var businessAddressHt: NSLayoutConstraint!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeLabelViewHt: NSLayoutConstraint!
    @IBOutlet weak var timeLabelHt: NSLayoutConstraint!
    @IBOutlet weak var timeImgHt: NSLayoutConstraint!
    
    
    @IBOutlet weak var contentVww: UIView!
    @IBOutlet weak var contentHtConstraint: NSLayoutConstraint!
    
    var timerForShowScrollIndicator: Timer?
    
    
    var idStr1 : Int = 0
    
    var providerdetailDict : NSDictionary = [:]
    var rating : Int = 0
    var bannerDict : [NSDictionary] = []
    var couponDict : [NSDictionary] = []
    var commentDict2 : [NSDictionary] = []
    var ownerID : String = ""
    var picker1 : UIImagePickerController? = UIImagePickerController()
    var newFrame1 : CGRect!
    var imageArray1 : [UIImage] = []
    var reviewRating : Int!
    var user_id : String = ""
    var userDef : UserDefaults!
    var newFrame : CGRect!
    var addresstwo : Int = 0
    var openTime : String = ""
    var closeTime : String = ""
    var time : String = ""
    var pass_ownerId : String = ""
    var passDict : NSDictionary = [:]
    var strOne : String = ""
    var currentItem : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setTitle.delegate = self
        setDescription.delegate = self
        
        webImage.layoutMargins = UIEdgeInsetsMake(4, 4, 4, 4)
        phoneImg.layoutMargins = UIEdgeInsetsMake(4, 4, 4, 4)
        mailImage.layoutMargins = UIEdgeInsetsMake(4, 4, 4, 4)
        
        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"
        
        phoneImg.layer.cornerRadius =  phoneImg.frame.size.width/2.0
        webImage.layer.cornerRadius =  webImage.frame.size.width/2.0
        mailImage.layer.cornerRadius =  mailImage.frame.size.width/2.0
        
           self.loadData()
        
        //   self.loadData()
        
    }
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        print("Coupon tap")
        print("couponDict == \(couponDict)")
        
        // Your action
        let commentDict1 = self.storyboard?.instantiateViewController(withIdentifier: "SlideCouponViewController") as! SlideCouponViewController
        commentDict1.allCouponArr = self.couponDict
        print("commentDict1.newsArr == \(couponDict)")
        print("currentItem.newsArr == \(currentItem)")
        
        self.present(commentDict1, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
  
    
    override func viewDidAppear(_ animated: Bool) {
     
      resetTheApi()
     
    }
    
    
    
    func loadData(){

        
        if (self.pass_ownerId != "")
        {
            self.ownerID = self.pass_ownerId
            self.getNotification()
        }
        else
        {
            self.ownerID = (providerdetailDict["owner_id"] as? String)!
          //  self.bannerDict = providerdetailDict["bannerdata"] as! [NSDictionary]
           // self.couponDict = providerdetailDict["coupondata"] as! [NSDictionary]
            passDict = providerdetailDict
            self.dataDict()
        }
        pageViewer()
    }
    
    
    
    func resetStar()
    {
       
     
        ApiResponse.callRating(Double((passDict["average_rating"] as! String))!, starRating: self.starCollection4)
        
        for star in self.starsBtn as [UIButton]
        {
            star.addTarget(self, action: #selector(ProviderDetailoneVC.buttonClicked(button:)), for: .touchUpInside)
        }
        self.showMoreComment()
    }
    
    
    func dataDict()
    {
        if(self.couponDict.count == 0)
        {
            couponLabel.isHidden = true
            couponHeight.constant = 0
            contentHtConstraint.constant = contentHtConstraint.constant - 99
        }
        else
        {
            couponLabel.isHidden = false
            couponHeight.constant = 99
            self.couponCollectionView.reloadData()
        }
        
        if(self.bannerDict.count == 0)
        {
            bannerLabel.isHidden = true
            bannerHeight.constant = 0
            contentHtConstraint.constant = contentHtConstraint.constant - 99
        }
        else{
            bannerLabel.isHidden = false
            bannerHeight.constant = 99
            self.bannerCollectionView.reloadData()
        }
        
        self.businessName.text = passDict["businessname"] as? String
        
        var imageString1 = passDict["image"] as! String
        imageString1 = imageString1.replacingOccurrences(of: " ", with: "%20")
        providerImage.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/\(imageString1)"))
        let titleString : NSMutableAttributedString = NSMutableAttributedString(string: "\((passDict["phonenumber"] as? String)!)")
        titleString.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedStringKey, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, (passDict["phonenumber"] as? String)!.utf16.count))
        self.numberBtn.setTitleColor(UIColor(rgb: 0x01A8FD), for: .normal)
        self.numberBtn.setAttributedTitle(titleString, for: .normal)
        
        
        // self.numberBtn.setTitle("\((passDict["phonenumber"] as? String)!)", for: .normal)
        self.webSiteBtn.setTitle("\((passDict["weblink"] as? String)!)", for: .normal)
        let addr = Int((passDict["address_status"] as? String)!)
        
        if (addresstwo == addr)
        {
            self.businessAddress.text = ""
        }
        else
        {
            self.businessAddress.text! = "\((passDict["address"] as? String)!)"
        }
        
        let ht1 = ApiResponse.calculateHeightForString((passDict["businessdescription"] as? String)!)

        
        
        
        self.businessDescriptionHt.constant = ht1 + 20
        self.businessDescription.text! = (passDict["businessdescription"] as? String)!
        
        self.businessDescriptionViewHt.constant = ht1 + 25
        self.businessDescription.numberOfLines = 0
        self.businessEmail.text! = (passDict["email"] as? String)!
        
        
        self.openTime = "\((passDict["open_time"] as! String))"
        self.closeTime = "\((passDict["close_time"] as! String))"
        
        self.time = "\(self.openTime) \(self.closeTime)"
        if self.time != " "
        {
            self.timeLabel.text! = "\(self.openTime) to \(self.closeTime)"
        }
        else
        {
            self.timeLabelViewHt.constant = 0
            self.timeLabelHt.constant = 0
            self.timeImgHt.constant = 0
        }
        ApiResponse.callRating(Double((passDict["average_rating"] as! String))!, starRating: self.starCollection4)
        
        for star in self.starsBtn as [UIButton]
        {
            star.addTarget(self, action: #selector(ProviderDetailoneVC.buttonClicked(button:)), for: .touchUpInside)
        }
        self.showMoreComment()
    }
    
    func showDeleteAler(){
        let alert = UIAlertController(title: "Deleting the review.", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default,  handler: { action in
            
            self.DeleteReview()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel,  handler: { action in
            
            alert.dismiss(animated: true, completion: nil)
            
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func contactBtn(_ sender: Any)
    {
        var number1 =  passDict["phonenumber"] as? String
        number1 = number1?.replacingOccurrences(of: " ", with: "")
        let url : NSURL = URL(string : "tel://\(number1!)" )! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func websiteBtn(_ sender: Any)
    {
        let urlstr = passDict["weblink"] as! String
        let web = self.storyboard?.instantiateViewController(withIdentifier: "webviewvc")as! WebViewVC
        web.webLink = urlstr
        web.linkOne = "one"
        self.present(web, animated: true, completion: nil)
    }
    
 
    @IBAction func onBackButton(_ sender: Any)
    {
        if (strOne == "one")
        {
            let dash = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
            self.present(dash, animated: false, completion: nil)
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func buttonClicked(button: UIButton)
    {
        for star in self.starsBtn as [UIButton]
        {
            star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
            
        }
        
        for i in 0  ... (starsBtn.index(of: button)!)
        {
            starsBtn[i].setBackgroundImage(UIImage (named: "yellowStar.png"), for: UIControlState.normal)
            
            rating = starsBtn.index(of: button)!
            
            rating = rating +  1
        }
    }
    
    func pageViewer()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            
            let URL = Constant.BASE_URL + "Userajax/page_viewer";
            
            let params : [String : String] = ["user_id" : "\(self.user_id)", "owner_id": "\(self.ownerID)"]
            
            Alamofire.request(URL,method:.post,parameters:params).responseJSON {
                
                response in
                if response.result.isSuccess{
                    let result = response.value as! NSDictionary
                    
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                                
                        }
                    }
                    else
                    {
                        ApiResponse.alert(title: "Oops", message: message, controller: self)
                    }
                }
                else {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
            }
        }
        else {
            ApiResponse.alert(title: "No internet connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    
    func resetTheApi()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            
            let URL = Constant.BASE_URL+"Userajax/get_business_owner";
           
            
            let params : [String : String] = ["owner_id": "\(self.ownerID)"]
            
            Alamofire.request(URL,method:.post,parameters:params).responseJSON {
                
                response in
                if response.result.isSuccess{
                    let result = response.value as! NSDictionary
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                let notiDict = result["data"] as! NSDictionary
                                self.passDict = notiDict
                                self.bannerDict = notiDict["bannerdata"] as! [NSDictionary]
                                self.couponDict = notiDict["coupondata"] as! [NSDictionary]
                                self.resetStar()
                        }
                    }
                    else
                    {
                        ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                    }
                }
                else {
                    
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    
                }
            }
        }
        else {
            ApiResponse.alert(title: "No internet connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    
    func getNotification()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            
            let URL = Constant.BASE_URL+"Userajax/get_business_owner";
            
            
            let params : [String : String] = ["owner_id": "\(self.ownerID)"]
            
            Alamofire.request(URL,method:.post,parameters:params).responseJSON {
                
                response in
                if response.result.isSuccess{
                    let result = response.value as! NSDictionary
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                let notiDict = result["data"] as! NSDictionary
                                self.passDict = notiDict
                                self.bannerDict = notiDict["bannerdata"] as! [NSDictionary]
                                self.couponDict = notiDict["coupondata"] as! [NSDictionary]
                                self.dataDict()
                        }
                    }
                    else
                    {
                        ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                    }
                }
                else {
                    
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    
                }
            }
        }
        else {
            ApiResponse.alert(title: "No internet connection", message: "Please check your internet", controller: self)
        }
    }
    
    func showMoreComment()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            
            let URL = Constant.BASE_URL+"Userajax/get_rating_review";
            
            let params : [String : String] = ["owner_id": "\(self.ownerID)"]
            
            Alamofire.request(URL,method:.post,parameters:params).responseJSON {
                
                response in
                if response.result.isSuccess{
                    let result = response.value as! NSDictionary
                    
                    
                    let status = result["status"] as! Bool
                    
                    if (status == true) {
                        
                        OperationQueue.main.addOperation
                            {
                                print("cccccccccccccc = \(result)")
                                LoadingIndicatorView.hide()
                                self.commentDict2  = result["data"] as! [NSDictionary]
                                if self.commentDict2.count != 0
                                {
                                    self.showCommentTableView.reloadData()
                                    self.commentHeight3.constant = 196
                                    
                                }
                                else
                                {
                                    self.commentHeight3.constant = 76
                                    self.contentHtConstraint.constant = self.contentHtConstraint.constant - 120
                                }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                self.commentHeight3.constant = 76
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
            }
        }
        else {
            ApiResponse.alert(title: "No internet connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    @IBAction func cameraButton(_ sender: Any)
    {
        picker1?.delegate = self
        self.openCamera()
    }
    
    @IBAction func galleryBtn(_ sender: Any)
    {
        picker1?.delegate = self
        self.openGallary()
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker1!.sourceType = UIImagePickerControllerSourceType.camera
            self.present(picker1!, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary))
        {
            picker1!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(picker1!, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        var tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        tempImage = self.resize(tempImage!)
        imageArray1.append(tempImage!)
        
        picker.dismiss(animated: true, completion: nil)
        self.imageUploadCV.reloadData()
    }
    
    func resize(_ image: UIImage) -> UIImage
    {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        //var compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth
        {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = UIImagePNGRepresentation(img)
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
    
    //-------------------------------
    @IBAction func submitBtn(_ sender: Any) {
        if self.rating != 0 {
            if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
            {
                
                if(rating == 0){
                    ApiResponse.alert(title: "Empty", message: "Please set rating", controller: self)
                }else if(setTitle.text! ==  "" ){
                    ApiResponse.alert(title: "Empty", message: "Please enter the 'Title'", controller: self)
                    
                }else if(setDescription.text! ==  ""){
                    ApiResponse.alert(title: "Empty", message: "Please enter the 'Description'", controller: self)
                }else{
                    self.postComment()
                }
            }
            else {
                ApiResponse.alert(title: "No internet connection", message: "Please check your internet", controller: self)
            }
        }
        else {
            ApiResponse.alert(title: "Alert", message: "Rating is mandatory", controller: self)
        }
    }
    
    
    
    func postComment(){
        
        LoadingIndicatorView.show()
        
            let url = Constant.BASE_URL+"Userajax/ratings_and_review"
        
        let parameters : [String : String]
        if(setTitle.text == ""){
            parameters = ["owner_id" : "\(self.ownerID)" , "review" : "\(setDescription.text!)","title" : "" , "user_id" : "\(self.user_id)" , "rating" : "\(rating)"]
        }
        else{
            parameters = ["owner_id" : "\(self.ownerID)" , "review" : "\(setDescription.text!)","title" : "\(setTitle.text!)" , "user_id" : "\(self.user_id)" , "rating" : "\(rating)"]
        }
        
     
            
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            var i = 0;
            
            for imgg in self.imageArray1 {
                
                i = i + 1;
                let imageData = UIImagePNGRepresentation(imgg)
                
                if(imageData == nil)
                {
                    return
                }
           
                if let data = imageData{
                    
                 
                    
                    let beginImage = CIImage(image:imgg)
                  
                    
                       let imageName = "\(Date().timeIntervalSince1970)"+"\(i)"
                    
                    multipartFormData.append(data, withName: "images[]", fileName: imageName as! String, mimeType: "image/png")
                }
            }
            
            
        
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    print(response)
                    let result = response.value as! NSDictionary
                    
                    let status = result["status"] as! Bool
                    print("Status :\(status)")
                    let message = result["message"] as! String
                    if (status == false) {
                        self.setTitle.text! = ""
                        self.setDescription.text! = ""
                        for star in self.starsBtn as [UIButton]
                        {
                            star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
                        }
                        self.imageArray1 = []
                        self.imageUploadCV.reloadData()
                        
                        
                          LoadingIndicatorView.hide()
                        if message == "You have already given review in last 24 hours to this owner!" {
                            ApiResponse.alert(title: "", message: "You have already given a review to this Business in the last 24 hours", controller: self)
                        }
                        else {
                            
                          
                            self.resetTheApi()
                            ApiResponse.alert(title: "", message: message, controller: self)
                        }
                    }
                    else {
                
                        print("show comment")
                        self.resetTheApi()
                  
                        ApiResponse.alert(title: "", message: "Your review has been successfully submitted." , controller: self)
                    }
                    
                    if let err = response.error{
                        
                        LoadingIndicatorView.hide()
                          ApiResponse.alert(title: "", message: "Something wrong!! Try again later", controller: self)
                        return
                    }
                 //   onCompletion?(nil)
                }
            case .failure(let error):
                LoadingIndicatorView.hide()
                print("Error in upload: \(error.localizedDescription)")
                        ApiResponse.alert(title: "", message: "Something wrong!! Try again later", controller: self)
            }
        }

            
        
        }
        
    
    
    
    func PostComment1()
    {
        
        
        LoadingIndicatorView.show()
        let newUrl = Constant.BASE_URL+"Userajax/ratings_and_review"
        let url = NSURL(string: newUrl)
        
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        
        //define the multipart request type
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var body = Data()
        let fname = "test.png"
        let mimetype = "image/png"
        print("SHOW")
        // print(setDescription.text!)
        
        
        //                        setDescription.text! = "Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Expand the review paragraph to be 1,000 characters. Test characters. Test characters. Test characters. Test OK"
        
        
        let parameters : [String : String]
        if(setTitle.text == ""){
            parameters = ["owner_id" : "\(self.ownerID)" , "review" : "\(setDescription.text!)","title" : "" , "user_id" : "\(self.user_id)" , "rating" : "\(rating)"]
        }
        else{
            parameters = ["owner_id" : "\(self.ownerID)" , "review" : "\(setDescription.text!)","title" : "\(setTitle.text!)" , "user_id" : "\(self.user_id)" , "rating" : "\(rating)"]
        }
        
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        var i : Int = 0
        for imgg in imageArray1 {
            let image_data = UIImagePNGRepresentation(imgg)
            
            if(image_data == nil)
            {
                return
            }
            
            //define the data post parameter
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"test\(i)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("hi\r\n".data(using: String.Encoding.utf8)!)
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"images[]\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(image_data!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            i = i + 1
        }
        
    
        request.httpBody = body as Data
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            OperationQueue.main.addOperation {
                guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                    print("error")
                    LoadingIndicatorView.hide()
                    //  self.showMoreComment()
                    ApiResponse.alert(title: "", message: "Your review has been successfully submitted" , controller: self)
                    return
                }
                let dd = try! JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                let status = dd["status"] as! Bool
                print("Status :\(status)")
                let message = dd["message"] as! String
                if (status == false) {
                    if message == "You have already given review in last 24 hours to this owner!" {
                        ApiResponse.alert(title: "", message: "You have already given a review to this Business in the last 24 hours", controller: self)
                    }
                    else {
                        self.loadData()
                        ApiResponse.alert(title: "", message: message, controller: self)
                    }
                }
                else {
                    LoadingIndicatorView.hide()
                    print("show comment")
                    self.loadData()
                    ApiResponse.alert(title: "", message: "Your review has been successfully submitted." , controller: self)
                }
                self.setTitle.text! = ""
                self.setDescription.text! = ""
                
                for star in self.starsBtn as [UIButton]
                {
                    star.setBackgroundImage(UIImage (named: "star.png"), for: UIControlState.normal)
                }
                self.imageArray1 = []
                self.imageUploadCV.reloadData()
            }
        }
        task.resume()
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
        
    }
    //    func textViewDidChange(_ textView: UITextView) {
    //        let fixedWidth = textView.frame.size.width
    //        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
    //        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
    //        var newFrame = textView.frame
    //        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
    //        textView.frame = newFrame
    //    }
    //
    
    var isRowExpanded = false
}

class UploadImageCell : UICollectionViewCell
{
    @IBOutlet weak var uploadImage: UIImageView!
}

class CouponCell : UICollectionViewCell
{
    @IBOutlet weak var couponImage: UIImageView!
    @IBOutlet weak var couponImg: UIButton!
    
}

class BannerCollectionViewCell : UICollectionViewCell
{
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var bannerImg: UIButton!
    
}

extension ProviderDetailoneVC : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (collectionView == couponCollectionView){
            return self.couponDict.count
        }else if(collectionView == imageUploadCV){
            return self.imageArray1.count
        }
        else {
            return self.bannerDict.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == couponCollectionView)
        {
            let cell : CouponCell = couponCollectionView.dequeueReusableCell(withReuseIdentifier: "couponcell", for: indexPath) as! CouponCell
            let newsDict = couponDict[indexPath.item]
            let imageString = newsDict["image"] as! String
            
            cell.couponImage.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/coupon/\(imageString)"))
            cell.couponImg.tag = indexPath.item
            cell.couponImg.addTarget(self, action: #selector(buttonTapAction(_:)), for: .touchUpInside)
            currentItem = indexPath.item
            return cell
            
        }
        else if(collectionView == imageUploadCV)
        {
            let cell : UploadImageCell = imageUploadCV.dequeueReusableCell(withReuseIdentifier: "uploadimagecell", for: indexPath) as! UploadImageCell
            cell.uploadImage.image = imageArray1[indexPath.row]
            
            return cell
        }
            
        else {
            let cell : BannerCollectionViewCell = bannerCollectionView.dequeueReusableCell(withReuseIdentifier: "bannercollectionviewcell", for: indexPath) as! BannerCollectionViewCell
            let newsDict = bannerDict[indexPath.item]
            let imageString = newsDict["image"] as! String
            cell.bannerImage.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
            
            cell.bannerImg.tag = indexPath.item
            cell.bannerImg.addTarget(self, action: #selector(BannerImgTapAction(_:)), for: .touchUpInside)
            currentItem = indexPath.item
            
            return cell
        }
    }
    @objc func buttonTapAction(_ sender: UIButton!)
    {
        let row : Int = sender.tag
        let selectedItem = couponDict[row]
        //let indexPath1 = couponCollectionView.indexPathForRow(at: point)
        print("couponDict == \(selectedItem)")
        
        
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        print("Coupon tap")
        print("couponDict == \(couponDict)")
        
        // Your action
        let commentDict1 = self.storyboard?.instantiateViewController(withIdentifier: "SlideCouponViewController") as! SlideCouponViewController
        commentDict1.allCouponArr = self.couponDict
        
        commentDict1.currentItem = row
        commentDict1.currentParent = "coupon"
        
        self.present(commentDict1, animated: true, completion: nil)
    }
    
    @objc func BannerImgTapAction(_ sender: UIButton!)
    {
        let row : Int = sender.tag
        let selectedItem = bannerDict[row]
        //let indexPath1 = couponCollectionView.indexPathForRow(at: point)
        print("couponDict == \(selectedItem)")
        
        
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        print("Coupon tap")
        print("couponDict == \(bannerDict)")
        
        // Your action
        let commentDict1 = self.storyboard?.instantiateViewController(withIdentifier: "SlideCouponViewController") as! SlideCouponViewController
        commentDict1.allCouponArr = self.bannerDict
        commentDict1.currentParent = "banner"
        commentDict1.currentItem = row
        
        self.present(commentDict1, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAtIndexPath")
        print("bannerDict == \(bannerDict)")
        
    }
    
    
    ////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //
    //
    ////        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
    ////        let numberOfItems = CGFloat(collectionView.numberOfItems(inSection: section))
    ////        let combinedItemWidth = (numberOfItems * flowLayout.itemSize.width) + ((numberOfItems - 1)  * flowLayout.minimumInteritemSpacing)
    ////        let padding = (collectionView.frame.width - combinedItemWidth) / 2
    ////        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    //
    //
    //     }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        
        if (collectionView == couponCollectionView){
            
            let totalCellWidth = 140 * self.couponDict.count
            let totalSpacingWidth = 5 * (self.couponDict.count - 1)
            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            print("rightInset collectionView.layer.frame.size.width == \(collectionView.layer.frame.size.width)")
            
            print("rightInset couponDict == \(rightInset)")
            print("leftInset couponDict== \(leftInset)")
            
            return UIEdgeInsetsMake(0, leftInset , 0, rightInset )
            
        }else if(collectionView == imageUploadCV){
            let totalCellWidth = 140 * collectionView.numberOfItems(inSection: 0)
            let totalSpacingWidth = 5 * (collectionView.numberOfItems(inSection: 0) - 1)
            //print("totalSpacingWidth == \(totalSpacingWidth)")
            //let totalSpacingWidth = 20
            
            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            print("rightInset == \(rightInset)")
            print("leftInset == \(leftInset)")
            
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }
        else {
            let totalCellWidth = 150 * self.bannerDict.count
            let totalSpacingWidth = 5 * (self.bannerDict.count - 1)
            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
            
        }
        
        
        
 
        
        
    }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemsPerRow:CGFloat = 2
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
 
        
        return CGSize(width: itemWidth - 20 , height: itemHeight)
    }
    
}

class Showcommentcell :  UITableViewCell{
    @IBOutlet weak var imgBtn1: UIButton!
    @IBOutlet var ratingCollections: [UIImageView]!
    @IBOutlet weak var titleLabel1: UILabel!
    @IBOutlet weak var descriptionLabel1: UILabel!
    @IBOutlet weak var nameLabel1: UILabel!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
}

class Morecommentbtn : UITableViewCell {
    
    @IBOutlet weak var reviewRequestBtn: KGHighLightedButton!
    @IBOutlet weak var showBtn1: UIButton!
    @IBOutlet weak var showBtn1Ht: NSLayoutConstraint!
    
}

extension ProviderDetailoneVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(section == 0)
        {
            if (commentDict2.count == 0)
            {
                return 0
            }
            else
            {
                return 1
            }
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0)
        {
            let cell : Showcommentcell = showCommentTableView.dequeueReusableCell(withIdentifier: "showcommentcell", for: indexPath) as! Showcommentcell
            
            let dict1 = commentDict2[indexPath.row]
            let userid = dict1["user_id"] as! String
            
            
            if (user_id == userid) {
                cell.editBtn.isHidden = false
                cell.deleteBtn.isHidden = false
                
                cell.deleteBtn.tag = Int((dict1["id"]  as? String)!)!
                cell.editBtn.tag = indexPath.row
                
                cell.deleteBtn.addTarget(self, action: #selector(deleteReview(_:)), for: .touchUpInside)
                cell.editBtn.addTarget(self, action: #selector(editDetail(_:)), for: .touchUpInside)
                
                //cell.editBtn.tag = Int((commDict["id"]  as? String)!)!
                
                
                //cell.editBtn.addTarget(self, action: #selector(editDetail), for: .touchUpInside)
                
                
                
            }
            else {
                cell.editBtn.isHidden = true
                cell.deleteBtn.isHidden = true
            }
            
            
            if (user_id == userid)
            {
                cell.titleLabel1.text! = (dict1["title"] as? String)!
                cell.nameLabel1.text! = (dict1["user_name"] as? String)!
                cell.descriptionLabel1.text! = (dict1["review"] as? String)!
                
                print("cell.descriptionLabel1.text")
 
                print(cell.descriptionLabel1.text!)
                
                
                let businessname = (dict1["businessname"] as? String)!
                // let array = businessname.components(separatedBy: CharacterSet.newlines)
                
                var lines: [String] = []
                businessname.enumerateLines { line, _ in
                    lines.append(line)
                }
                
                let dateNdtime = lines[1]
                
                
                cell.businessName.text! = lines[0] + "\r\n" + ApiResponse.convertDateFormater(dateNdtime)
                
                
                ApiResponse.callRating(Double((dict1["rating"] as? String)!)!, starRating: cell.ratingCollections)
                let imagesArray = dict1["images"]
                if ((imagesArray as AnyObject).count)! <= 0 {
                    cell.imgBtn1.isHidden = true
                }
                else {
                    cell.imgBtn1.isHidden = false
                }
                if isRowExpanded == true {
                    cell.descriptionLabel1.numberOfLines = 0
                }
                else {
                    cell.descriptionLabel1.numberOfLines = 2
                    let readmoreFont = UIFont(name: "Helvetica-Oblique", size: 11.0)
                    let readmoreFontColor = UIColor.blue
                    DispatchQueue.main.async {
                        if((cell.descriptionLabel1.text?.count)! > 30)
                        {
                            
                            cell.descriptionLabel1.addTrailingnew(with: "...", moreText: "Read more", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
                        }
                    }
                    
                }
                cell.imgBtn1.addTarget(self, action: #selector(imagedetail2(_:)), for: .touchUpInside)
                return cell
            }
            else
            {
                cell.titleLabel1.text! = (dict1["title"] as? String)!
                if((dict1["user_name"] as? String) != nil){
                    cell.nameLabel1.text! = (dict1["user_name"] as? String)!
                    print((dict1["user_name"] as? String)!)
                    
                }
                cell.descriptionLabel1.text! = (dict1["review"] as? String)!
 
                if isRowExpanded == false {
                    
                    let readmoreFontColor = UIColor.blue
                    DispatchQueue.main.async {
                        //                    cell.descriptionLabel1.addTrailing(with: "... ", moreText: "more", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
                        
                        if((cell.descriptionLabel1.text?.count)! > 30)
                        {
                            
                            cell.descriptionLabel1.addTrailingnew(with: "  ", moreText: "Read more", moreTextFont: cell.descriptionLabel1.font!, moreTextColor: readmoreFontColor)
                        }
                        
                        
                    }
       
                    self.stopTimerForShowScrollIndicator()
                    
                }
                else
                {
                    self.startTimerForShowScrollIndicator()
                    
                    
                }
                cell.businessName.text! = (dict1["businessname"] as? String)!
                print((dict1["businessname"] as? String)!)
                
                ApiResponse.callRating(Double((dict1["rating"] as? String)!)!, starRating: cell.ratingCollections)
                let imagesArray = dict1["images"]
                if ((imagesArray as AnyObject).count)! <= 0 {
                    cell.imgBtn1.isHidden = true
                }
                else {
                    cell.imgBtn1.isHidden = false
                }
                if isRowExpanded == true {
                    cell.descriptionLabel1.numberOfLines = 0
                }
                else {
                    cell.descriptionLabel1.numberOfLines = 1
                }
                cell.imgBtn1.addTarget(self, action: #selector(imagedetail2(_:)), for: .touchUpInside)
                return cell
            }
        }
        else {
            let cell : Morecommentbtn = showCommentTableView.dequeueReusableCell(withIdentifier: "morecommentbtn") as! Morecommentbtn
            
            if (commentDict2.count != 0) {
                
                if (commentDict2.count == 1)
                {
                    self.commentHeight3.constant = 160
                    cell.showBtn1Ht.constant = 0
                    cell.showBtn1.setTitle("", for: .normal)
                    cell.showBtn1.addTarget(self, action: #selector(showMore2(_:)), for: .touchUpInside)
                    cell.reviewRequestBtn.addTarget(self, action: #selector(reviewRequest(_:)), for: .touchUpInside)
                }
                else
                {
                    self.commentHeight3.constant = 196
                    // self.commentHeight3.constant = 300
                    
                    cell.showBtn1Ht.constant = 36
                    cell.showBtn1.setTitle("Show More", for: .normal)
                    cell.showBtn1.addTarget(self, action: #selector(showMore2(_:)), for: .touchUpInside)
                    cell.reviewRequestBtn.addTarget(self, action: #selector(reviewRequest(_:)), for: .touchUpInside)
                }
            }
            else
            {
                cell.reviewRequestBtn.addTarget(self, action: #selector(reviewRequest(_:)), for: .touchUpInside)
                cell.showBtn1.setTitle("", for: .normal)
                cell.showBtn1Ht.constant = 0
                self.commentHeight3.constant = 76
            }
            return cell
        }
    }
    
    
    @objc func deleteReview(_ sender: UIButton!)
    {
        
        // idStr1 = commDict["id"] as! String
        idStr1 = sender.tag
        
        //        let point : CGPoint = sender.convert(CGPoint.zero, to:commentTableView)
        //        let indexPath1 = commentTableView.indexPathForRow(at: point)
        //        let dictNoti1 = self.commentArr1[(indexPath1?.row)!]
        
        
        self.showDeleteAler()
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func editDetail(_ sender: UIButton!)
    {
        currentItem = sender.tag
        let dictNoti = self.commentDict2[currentItem]
        // print(dictNoti)
        let edit1 = self.storyboard?.instantiateViewController(withIdentifier: "feedbackvw") as! FeedbackViewController
        edit1.editDictOne = dictNoti
        edit1.editStr = "editone"
        //edit1.editStr = "editone"
        
        self.present(edit1, animated: true, completion: nil)
    }
    
    func DeleteReview(){
        LoadingIndicatorView.show()
        
        
        let URL = Constant.BASE_URL+"Userajax/delete_rating";
        
        let params : [String : String] = ["review_id" : "\(self.idStr1)" ]
        
        //let params = "review_id=\(self.idStr1)"
        
        Alamofire.request(URL,method:.post,parameters:params).responseJSON {
            
            response in
            if response.result.isSuccess{
                
                let result = response.value as! NSDictionary
                
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                print("result = \(result)")
                
                if (status == true) {
                    
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            self.resetTheApi()
                            ApiResponse.alert(title: "Done", message: message, controller: self)
                            
                    }
                }
                else {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                    }
                    //   ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else
            {
                LoadingIndicatorView.hide()
                
                ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                
            }
        }
        
    }
    
    /// Show always scroll indicator in table view
    @objc func showScrollIndicatorsInContacts() {
        UIView.animate(withDuration: 0) {
            self.showCommentTableView.flashScrollIndicators()
        }
    }
    
    /// Start timer for always show scroll indicator in table view
    func startTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.showScrollIndicatorsInContacts), userInfo: nil, repeats: true)
    }
    
    /// Stop timer for always show scroll indicator in table view
    func stopTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator?.invalidate()
        self.timerForShowScrollIndicator = nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            isRowExpanded = !isRowExpanded
            self.showCommentTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0)
        {
            if commentDict2.count != 0
            {
                print("commentDict2")
                
                print(commentDict2)
                
                return UITableViewAutomaticDimension
            }
            else
            {
                return 0
            }
        }
        else
        {
            if (commentDict2.count != 0) {
                return 89
            }
            else {
                return 50
            }
        }
        
    }
    
    
    @objc func showMore2(_ sender: UIButton!)
    {
        // let point : CGPoint = sender.convert(CGPoint.zero, to:showCommentTableView)
        let commentDict1 = self.storyboard?.instantiateViewController(withIdentifier: "commentviewcontroller") as! CommentViewController
        commentDict1.ownerID1 = ownerID
        self.present(commentDict1, animated: true, completion: nil)
    }
    
    @objc func reviewRequest(_ sender: UIButton!)
    {
        // let point : CGPoint = sender.convert(CGPoint.zero, to:showCommentTableView)
        self.ReviewRequest()
    }
    
    @objc func imagedetail2(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:showCommentTableView)
        let indexPath1 = showCommentTableView.indexPathForRow(at: point)
        let dictNoti1 = self.commentDict2[(indexPath1?.item)!]
        let showImage = self.storyboard?.instantiateViewController(withIdentifier: "imageviewcontroller") as! ImageViewController
        showImage.commentDict = dictNoti1
        showImage.imgStr = "commentStr1"
        self.present(showImage, animated: true, completion: nil)
    }
    
    func ReviewRequest()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            
            
            let URL = Constant.BASE_URL + "Userajax/review_request";
            
            
            let params : [String : String] = ["owner_id" : "\(self.ownerID)" ,
                        "user_id" : "\(self.user_id)"]
            
            Alamofire.request(URL,method:.post,parameters:params).responseJSON {
                
                response in
                if response.result.isSuccess{
                    let result = response.value as! NSDictionary
                    
                    let status = result["status"] as! Bool
                    _ = result["message"] as! String
                    
                    if (status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                _  = result["data"] as! NSDictionary
                                self.showCommentTableView.reloadData()
                                ApiResponse.alert(title: "Done", message: "Review Request successfully submitted", controller: self)
                        }
                    }
                    else {
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                        }
                    }
                }
                else {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    
                }
            }
        }
        else {
            ApiResponse.alert(title: "No internet connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count >= 1000 {
            let truncated = textView.text.substring(to: textView.text.index(before: textView.text.endIndex))
            textView.text = truncated
        }
    }
    
    
}
extension UILabel {
    
    func addTrailingnew(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText
        
        let lengthForVisibleString: Int = self.vissibleTextLength
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        var answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedStringKey.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedStringKey.font: moreTextFont, NSAttributedStringKey.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        
        
        
        self.attributedText = answerAttributed
        
    }
    
    var vissibleTextLength: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        
        let attributes: [AnyHashable: Any] = [NSAttributedStringKey.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedStringKey : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
        
        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedStringKey : Any], context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
    }
}





