//
//  RateFoodVC.swift
//  FairField
//
//  Created by Prashant Shinde on 7/7/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class RateFoodVC: UIViewController {

    
    @IBOutlet var card: UIView!
    @IBOutlet var disLike: UIImageView!
    
    var divisor : CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        divisor = (view.frame.width / 2) / 0.61
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func dashBoard(_ sender: Any) {
        
        let events = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
        self.present(events, animated: true, completion: nil)
        

    }
    @IBAction func panCard(_ sender: UIPanGestureRecognizer) {
        
        let card1 = sender.view!
        let point = sender.translation(in: view)
        let xFromCenter = card1.center.x - view.center.x
        
        card1.center = CGPoint(x : view.center.x + point.x , y : view.center.y + point.y )
        
        let scale = min(100/abs(xFromCenter) , 1)
        card1.transform = CGAffineTransform(rotationAngle: xFromCenter / divisor).scaledBy(x: scale, y: scale)
        
        if (xFromCenter > 0 ){
        disLike.image = #imageLiteral(resourceName: "Like blue.png")
            
        } else {
        disLike.image = #imageLiteral(resourceName: "DisLike Blue.png")
        }
        disLike.alpha = abs(xFromCenter) / view.center.x
        
        
        if(sender.state == UIGestureRecognizerState.ended)
        {
            if(card.center.x < 75){
            // move off to the left side
                UIView.animate(withDuration: 0.5, animations: {
                    card1.center = CGPoint(x : card1.center.x - 225, y : card1.center.y + 75)
                    card1.alpha = 0
                })
                return
            }
            else if card.center.x > (view.frame.width - 75){
            // move off to the right side
                UIView.animate(withDuration: 0.5, animations: {
                 card1.center = CGPoint(x : card1.center.x + 225, y : card1.center.y + 75)
                    card1.alpha = 0
                })
                return
            }
            
            
        card1.center = self.view.center
            self.disLike.alpha = 0
        
        }
    }
 
}
