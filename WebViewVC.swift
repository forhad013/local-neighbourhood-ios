//
//  WebViewVC.swift
//  FairField
//
//  Created by Prashant Shinde on 10/9/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController {

    @IBOutlet weak var webViewPage: UIWebView!
    @IBOutlet weak var navigationbar: UINavigationBar!
    
    var webLink : String = ""
    var linkOne : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if linkOne == "one" {
            
         navigationbar.topItem?.title = "WebSite"
            let url = URL(string: "\(webLink)")
            if UIApplication.shared.canOpenURL(url!)
            {
                let request = NSURLRequest(url: url!);
                webViewPage.loadRequest(request as URLRequest);
            }
        }
        else
        {
            let url1 = URL(string: "https://www.localneighborhoodapp.com/user/terms_condition")
            if UIApplication.shared.canOpenURL(url1!)
            {
                let request = NSURLRequest(url: url1!);
                webViewPage.loadRequest(request as URLRequest);
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
         self.dismiss(animated: false, completion: nil)
    }

    
}
