//
//  ReviewRequestVC.swift
//  FairField
//
//  Created by Prashant Shinde on 8/4/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}



class ReviewRequestVC: UIViewController {
    
    @IBOutlet weak var requestTableView: UITableView!
    @IBOutlet weak var listTableView3: UITableView!
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var imageViewForAds: UIImageView!
    
    let swiftColor = UIColor.init(red: 8, green: 143, blue: 198)
    
    var requestArr : [NSDictionary] = []
    var user_id : String = ""
    var userDef : UserDefaults!
    var listArray3 : [String] = []
    var advertisementArr2 : [NSDictionary] = []
    var scrollingTimer1 = Timer()
    var ownerId3 : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"
        
        self.showAdvertisement2()
        addGestureToAdsImageView()
        DispatchQueue.main.async
            {
            self.showRequest()
            self.listArray3 = ["New Businesses", "New Reviews",   "Requested Reviews"]
            self.listTableView3.isHidden = true
        }
      
         
    }
    
    func addGestureToAdsImageView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(adsImageViewAction))
        self.imageViewForAds.isUserInteractionEnabled = true
        self.imageViewForAds.tag = -1
        self.imageViewForAds.addGestureRecognizer(tapGesture)
    }
    
    private var adsImageIndex = 0
    
    @objc func startTimer(theTimer : Timer){
        //        UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseOut, animations: {
        //            self.addCollectionViewThree.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int , section:0), at: .centeredHorizontally, animated: false)
        //        }, completion: nil)
        setupForAdsImages()
    }
    
    func stopTimerTest() {
        
        scrollingTimer1.invalidate()
    }
    
    func setupForAdsImages() {
        if adsImageIndex >= advertisementArr2.count {
            adsImageIndex = 0
        }
        let dict1 = advertisementArr2[adsImageIndex]
        adView(userid: self.user_id, bannerid: dict1.value(forKey: "id")! as! String, ownerid: dict1.value(forKey: "owner_id")! as! String)
        self.imageViewForAds.tag = adsImageIndex
        adsImageIndex = adsImageIndex + 1
        let imageString = dict1["image"] as! String
        //print("imageString = \(imageString)")
        imageViewForAds.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
        
        
    }
    
    @objc func adsImageViewAction(sender: UITapGestureRecognizer!) {
        print("adsImageView clicked \((sender.view?.tag)!)")
        if (sender.view?.tag)! != -1 {
            let dict2 = advertisementArr2[(sender.view?.tag)!]
            self.ownerId3 = (dict2["owner_id"] as? String)!
            print("ownerId ======******\(ownerId3)")
            adClick(userid: self.user_id, bannerid: dict2.value(forKey: "id")! as! String, ownerid: dict2.value(forKey: "owner_id")! as! String)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func showList1(_ sender: Any) {
      
        if listTableView3.isHidden {
            listTableView3.isHidden = false
        } else {
            listTableView3.isHidden = true
        }
 
    }
    func showRequest(){
        LoadingIndicatorView.show()
        let params = "user_id=\(user_id) "
        ApiResponse.onResponsePostPhp(url: "Userajax/feedback_request", parms: params, completion: { (result, error) in
            
            if (error == "") {
                
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                if (status == true) {
                    
                    print("Reviews Result = \(result)")
                    
                    OperationQueue.main.addOperation {
                        LoadingIndicatorView.hide()
                        self.requestArr = result["data"] as! [NSDictionary]
                        if self.requestArr.count != 0
                        {
                            self.requestTableView.reloadData()
                            self.labelOne.isHidden = true
                        }
                        else{}
                    }
                }
                else {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            self.requestTableView.isHidden = true
                            self.labelOne.isHidden = false
                    }
                    
                }
            }
            else
            {
                if (error == Constant.Status_Not_200)
                {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else
                {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
    }

    
    @IBAction func onRequestReview(_ sender: Any) {
       self.dismiss(animated: true, completion: nil)
    }
    
    func adClick(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/banner_click", parms: pstring, completion: {(result , error) in
                print(ApiResponse.onResponsePostPhp)
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                self.addDetailThree()

                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    func adView(userid: String, bannerid: String, ownerid:String)
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            //LoadingIndicatorView.show()
            let pstring = "user_id=\(userid)&banner_id=\(bannerid)&owner_id=\(ownerid)"
            ApiResponse.onResponsePostPhp(url: "userajax/save_page_viewer", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                
                        }
                    }
                    else{
                        ApiResponse.alert(title: "Oops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    
    
    func showAdvertisement2(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = ""
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/banners", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss = \(result)")
                    if(status == true)
                    {
                        OperationQueue.main.addOperation
                            {
                            self.advertisementArr2 = result["data"] as! [NSDictionary]
                            if self.advertisementArr2.count != 0
                            {
//                                self.addCollevtionViewOne.reloadData()
                                self.setupForAdsImages()
                                self.scrollingTimer1 = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: nil, repeats: true)
                                self.scrollingTimer1.fire()
                                LoadingIndicatorView.hide()
                            }
                            else
                            {
//                            self.addCollevtionViewOne.isHidden = true
                            }
                            
                            
                        }
                    }
                    else{
                       // ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                    
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    

    var newsArr: [NSDictionary] = []
}



class ReqTableViewCell : UITableViewCell {

    @IBOutlet weak var businessName1: UILabel!
    @IBOutlet weak var businessName1Ht: NSLayoutConstraint!
   
}
extension  ReviewRequestVC : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return requestArr.count
        var count:Int?
        
        if tableView == self.requestTableView {
            count = requestArr.count
        }
        else {
            count =  listArray3.count
        }
         
        return count!

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.requestTableView {
        let cell : ReqTableViewCell = requestTableView.dequeueReusableCell(withIdentifier: "reqtableviewcell") as! ReqTableViewCell
            let reviewDict  = requestArr[indexPath.row]
            let text =  "A Review is requested for \(reviewDict["businessname"] as! String) please share your experiance!"
            let linkTextWithColor = (reviewDict["businessname"]as? String)!
     
            
            
            let rangeOfColoredString = (text as NSString).range(of: linkTextWithColor)
            
            // Create the attributedString.
            let attributedString = NSMutableAttributedString(string:text)
            attributedString.setAttributes([NSAttributedStringKey.foregroundColor: swiftColor],
                                           range: rangeOfColoredString)
            
            cell.businessName1.attributedText = attributedString
            cell.layer.borderColor =  UIColor.init(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1.0).cgColor
            cell.layer.borderWidth = 4
            cell.clipsToBounds = true
            
           
//            var stringText = "<!DOCTYPE html> <html> <body><b>A Review is requested for <font color='blue'>\(reviewDict["businessname"] as! String)</font> please share your experiance!</b></body> </html>"
//
//
//            cell.businessName1.attributedText = stringText.htmlToAttributedString

        
        return cell
        }
        else {
            let cell : UITableViewCell = listTableView3.dequeueReusableCell(withIdentifier: "listcell3")!
            
            cell.textLabel?.text = listArray3[indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            
            let itemSize = CGSize(width : 15, height : 15);
            UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
            let imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
            cell.imageView?.image?.draw(in: imageRect)
            cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return cell

        }
    }
    
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        
    if tableView == self.listTableView3
    {
    switch indexPath.row {
    case 0:
    print("Business Info")
    let feed = self.storyboard?.instantiateViewController(withIdentifier: "newsfeed") as! NewsFeedVC
    feed.searchone = "search3"
    stopTimerTest()
    self.present(feed, animated: true, completion: nil)
     break
    
    case 1:
    print("View Reviews")
    let feed = self.storyboard?.instantiateViewController(withIdentifier: "reviewsviewcontroller") as! ReviewsViewController
    stopTimerTest()
     self.present(feed, animated: true, completion: nil)
      break
    
    case 2:
    print("Review Request")
    let feed = self.storyboard?.instantiateViewController(withIdentifier: "reviewrequestvc") as! ReviewRequestVC
    stopTimerTest()
    self.present(feed, animated: true, completion: nil)
    break
    
     default :
    break
    }
    listTableView3.isHidden = true
    }
    else{
//       let reqDict = requestArr[indexPath.row]
//        print("reqDict = \(reqDict)")
//        let feedback = self.storyboard?.instantiateViewController(withIdentifier: "feedbackvw") as! FeedbackViewController
//        let requestId = reqDict["owner_id"] as! String
//        feedback.feedback_Id = requestId
//        self.present(feedback, animated: true, completion: nil)
        
        self.getLocal(index: indexPath.row)
       
        }
    }
    
    func getLocal(index: Int){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = ""
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/new_business_owner", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("local data == \(result)")
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            LoadingIndicatorView.hide()
                            self.newsArr = result["data"] as! [NSDictionary]
                            if self.newsArr.count > 0 {
                                print("Array is \(self.newsArr)")
                                //reviewArr[1]["owner_id"]!
                                
                                for dictionary in self.newsArr {
                                    if dictionary["owner_id"] as! String == self.requestArr[index]["owner_id"] as! String {
                                        let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                                        dictProvider.providerdetailDict = dictionary
                                        self.present(dictProvider, animated: true, completion: nil)
                                        break
                                    }
                                }
                                
                            }
                            else {
                                
                            }
                            
                        }
                    }
                    else{
                        LoadingIndicatorView.hide()
                        //  ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        
        let vw = UIView.init(frame: CGRect(x : 0, y : 0, width : requestTableView.frame.width, height : 10))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
}

extension ReviewRequestVC {//}: UICollectionViewDataSource , UICollectionViewDelegate {
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return self.advertisementArr2.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell : AddCellOne = addCollevtionViewOne.dequeueReusableCell(withReuseIdentifier: "addcellone", for: indexPath) as! AddCellOne
//
//            let dict1 = advertisementArr2[indexPath.item]
//            let imageString = dict1["image"] as! String
//            print("imageString = https://www.localneighborhoodapp.com/images/banner/\(imageString)")
//            cell.addImgone.sd_setImage(with: URL(string : "https://www.localneighborhoodapp.com/images/banner/\(imageString)"))
//
//            var rowIndex = indexPath.item
//            let numberofRecords : Int = self.advertisementArr2.count - 1
//            if (rowIndex < numberofRecords) {
//                rowIndex = (rowIndex + 1)
//
//            }else {
//                rowIndex = 0
//
//            }
//            scrollingTimer1 = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(ReviewRequestVC.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)
//            return cell
//
//    }
    
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        let dict2 = advertisementArr2[indexPath.row]
//        self.ownerId3 = (dict2["owner_id"] as? String)!
//        print("ownerId ======******\(ownerId3)")
//        self.addDetailThree()
//
//
//    }
    
    
    func addDetailThree(){
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show("Loading....")
            let pstring = "owner_id=\(self.ownerId3)"
            
            print("\(pstring)")
            ApiResponse.onResponsePostPhp(url: "Userajax/get_business_owner", parms: pstring, completion: {(result , error) in
                if(error == ""){
                    
                    
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss = \(result)")
                    if(status == true){
                        OperationQueue.main.addOperation {
                            
                            var dict5 = result["data"] as! NSDictionary
                            
                            let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                            dictProvider.providerdetailDict = dict5
                            self.present(dictProvider, animated: true, completion: nil)
                            
                            
                            LoadingIndicatorView.hide()
                            // ApiResponse.alert(title: "OK", message: message, controller: self)
                        }
                        
                    }
                    else{
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
    }
    
 
    
}



class AddCellOne: UICollectionViewCell {
    
    @IBOutlet weak var addImgone: UIImageView!
    
}
