//
//  ShowSavedCouponViewController.swift
//  Local Neighbourhood App
//
//  Created by Danish Naeem on 12/3/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class ShowSavedCouponViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var textView: UITextView!
    var descriptionString: String? = nil
    var imageString: String? = nil
    var currentID: String? = nil
    
    var labelString: String? = nil
    @IBOutlet weak var titleLab: UILabel!
    var newsArr : [NSDictionary] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadingIndicatorView.show()
        
        
        let dic = UserDefaults.standard.value(forKey: "userInfo") as! NSDictionary
        let userid = dic.value(forKey: "user_id")! as! String
        
        let params = "user_id=\(userid)&search_keyword=\("")&value_match=0"
        print("params == \(params)")
        
        self.getSearch(keywordStr: params)
        
        if imageString != nil {
            self.imageView.sd_setImage(with: URL(string : imageString!))
            
        }
        if descriptionString != nil {
            self.textView.text = descriptionString!
        }
        if labelString != nil {
            self.titleLab.text = labelString!
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func gotoDetail (_ sender: Any)
    {
 
        if self.newsArr.count > 0 {
            print("Array is \(self.newsArr)")
            //reviewArr[1]["owner_id"]!
            
            for dictionary in self.newsArr {
                if dictionary["owner_id"] as? String == self.currentID  {
                    // LoadingIndicatorView.hide()
                    
                    let dictProvider = self.storyboard?.instantiateViewController(withIdentifier: "providerdetailonevc") as! ProviderDetailoneVC
                    dictProvider.providerdetailDict = dictionary
                    self.present(dictProvider, animated: true, completion: nil)
                    break
                }
            }
            
        }
    }
    func getSearch(keywordStr : String) {
        
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            
            ApiResponse.onResponsePostPhp(url: "Userajax/search_business_owner", parms: keywordStr, completion: {(result , error) in
                
                if(error == "")
                {
                    let status = result["status"]as! Bool
                    let message = result["message"]as! String
                    print("ressassss == \(result)")
                    if(status == true){
                        OperationQueue.main.addOperation
                            {
                                self.newsArr = result["data"] as! [NSDictionary]
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation
                            {
                                
                        }
                        LoadingIndicatorView.hide()
                        
                        ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                    }
                }
                else
                {
                    LoadingIndicatorView.hide()
                    
                    if(error == Constant.Status_Not_200)
                    {
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else
                    {
                        ApiResponse.alert(title: "Request time out", message: "Please check internet Connection", controller: self)
                    }
                }
                
            })
            
        }
        else
        {
            //  ApiResponse.alert(title: Warning, message: messa, controller: <#T##UIViewController#>)
        }
        LoadingIndicatorView.hide()
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

