//
//  SignUpViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 6/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseMessaging

//import JVFloatLabeledTextFieldViewController.h
//import JVFloatLabeledTextField.h
//import JVFloatLabeledTextView.h

//const static CGFloat kJVFieldHeight = 44.0f
//const static CGFloat kJVFieldHMargin = 10.0f
//const static CGFloat kJVFieldFontSize = 16.0f
//const static CGFloat kJVFieldFloatingLabelFontSize = 11.0f


class SignUpViewController: UIViewController , FBSDKLoginButtonDelegate , UITextFieldDelegate {
    
    
    @IBOutlet var fullName: UITextField!
    @IBOutlet var emailText: UITextField!
    @IBOutlet var passwordText: UITextField!
    @IBOutlet var confirmPassword: UITextField!
    @IBOutlet weak var fbBtn: FBSDKLoginButton!
    @IBOutlet weak var termsBtn: UIButton!
    @IBOutlet var popView: UIView!
    @IBOutlet var transparentView: UIView!


    var userDef : UserDefaults!
    var newFrame : CGRect!
    var checkOne : Bool = false
    var BoxOff = UIImage(named: "check-box-empty.png") as UIImage?
    var BoxOn = UIImage(named: "checkBoxFill.png")as UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.isUserInteractionEnabled = false
//        self.popView.isUserInteractionEnabled = true

        
 
        confirmPassword.delegate = self
        
        
        let textfield : [UITextField] = [fullName, emailText, passwordText, confirmPassword]
        
        let placeholder = ["Full Name", "Email", "Password", "Confirm Password"]
        var i = 0
        
        for tf in textfield
        {
            tf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 5, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:placeholder[i],
                                                          attributes:[kCTForegroundColorAttributeName as NSAttributedStringKey: UIColor.white])
            i += 1
            
        }
        let titleText = NSAttributedString(string: "Login with Facebook")
        fbBtn.titleLabel?.font = UIFont(name: "System-Medium", size: 10)
        fbBtn.setAttributedTitle(titleText, for: UIControlState.normal)
        fbBtn.titleLabel?.textColor = UIColor.white

      
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
          tokenRefreshNotification()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func checkBtn(_ sender: Any) {
//            termsBtn.setBackgroundImage(BoxOn , for: .normal)
//            termsBtn.setBackgroundImage( BoxOff , for: .normal)
        if (checkOne == false)
        {
            termsBtn.setBackgroundImage(UIImage(named : "CheckBoxFill.png"), for: .normal)
            checkOne = true
        }
        else {
            
            termsBtn.setBackgroundImage(UIImage(named : "check-box-empty.png"), for: .normal)
            checkOne = false
        }
    }
    @IBAction func btnConfirm(_ sender: Any) {
        if (checkOne == true)
        {
        UIView.animate(withDuration: 0.5, animations: {
            self.transparentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.popView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.transparentView.alpha = 0.0;
            self.popView.alpha = 0.0;

         }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.popView.removeFromSuperview()
                self.transparentView.removeFromSuperview()

            }
        });
        }
        else
        {
            ApiResponse.alert(title: "", message: "Please agree terms and conditions.", controller: self)

            
        }

    }
    @IBAction func btnCancel(_ sender: Any) {
        let signIn = self.storyboard?.instantiateViewController(withIdentifier: "signin") as! SignInViewController
        self.present(signIn, animated: true, completion: nil)

    }


    @IBAction func termsAneconditionBtm(_ sender: Any) {
        let web = self.storyboard?.instantiateViewController(withIdentifier: "webviewvc")as! WebViewVC
        self.present(web, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
//        self.newFrame = self.view.frame
//
//        newFrame.origin.y = self.view.bounds.origin.y - 110
//        newFrame.size.height = self.view.bounds.size.height
//
//        UIView.animate(withDuration: 0.25, animations:
//            {() -> Void in
//
//                self.view.frame = self.newFrame
//        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
//        newFrame.origin.y = 0
//        newFrame.size.height = self.view.bounds.size.height
//        
//        UIView.animate(withDuration: 0.25, animations:
//            {() -> Void in
//                self.view.frame = self.newFrame
//        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
    
    @IBAction func onSignin(_ sender: Any) {
        
        let signIn = self.storyboard?.instantiateViewController(withIdentifier: "signin") as! SignInViewController
        self.present(signIn, animated: true, completion: nil)
        
    }
    
    
    @IBAction func onSignup(_ sender: Any) {
        
        if (fullName.text == "") || (emailText.text == "") || (passwordText.text == "") || (confirmPassword.text == "")   {
            ApiResponse.alert(title: "Empty", message: "Please fill all the fields", controller: self)
        }
        else if (ApiResponse.validateEmail(emailText.text!) == false) {
            ApiResponse.alert(title: "Invalid Email", message: "Please enter valid email id", controller: self)
        }
        else if ((passwordText.text?.characters.count)! < 6) {
            ApiResponse.alert(title: "Ooops", message: "Password should be 6 character", controller: self)
        }
        else if (passwordText.text! != confirmPassword.text!) {
            ApiResponse.alert(title: "Password Mismatch", message: "Please enter correct password", controller: self)
        }
//        else if(checkOne == false)
//        {
//            ApiResponse.alert(title: "Ooops", message: "Please agree terms and conditions.", controller: self)
//        }
        else
        {
            self.getRegister()
        }
    }
    
    func getRegister()
    {
        let deviceid = UIDevice.current.identifierForVendor!.uuidString
        LoadingIndicatorView.show("Loading...")
        
        let params = "first_name=\(fullName.text!)&email=\(emailText.text!)&password=\(passwordText.text!)&cpassword=\(confirmPassword.text!)&terms_condition=1&from_where=0&device_type=ios&device_id=\(deviceid)&device_token=123456"
    
        ApiResponse.onResponsePostPhp(url: "Userajax/userregister", parms: params, completion: { (result, error) in
            
            if (error == "") {
                LoadingIndicatorView.hide()
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                
                if (status == true) {
                    
                    OperationQueue.main.addOperation {
                        LoadingIndicatorView.hide()
                        // to store data in userDefault ----------------
                        //                            let data = result["data"] as! NSDictionary
                        //                            let userDef = UserDefaults.standard
                        //                            userDef.set(data, forKey: "userInfo")
                        //                            userDef.set("login", forKey: "session")
                        // to store data in userDefault ----------------
                        let verifyMail = self.storyboard?.instantiateViewController(withIdentifier: "verifyemailvc") as! VerifyEmailVC
                        
                        verifyMail.emailId = self.emailText.text!
                        self.present(verifyMail, animated: true, completion: nil)
                    }
                }
                else {
                    ApiResponse.alert(title: "", message: message, controller: self)
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    LoadingIndicatorView.hide()
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    LoadingIndicatorView.hide()
                }
            }
        })
    }

    @IBAction func signupWithFb(_ sender: Any) {
        
        fbBtn.readPermissions = ["public_profile", "email", "user_friends"]
        fbBtn.delegate = self
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logout")
    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        print("User Logged In")
        
        if ((error) != nil)
        {
            // Process error
            print("process error\(error)")
        }
        else if result.isCancelled
        {
            // Handle cancellations
        }
        else
        {
            if result.grantedPermissions.contains("email")
            {
                print("do wrok")
                self.returnUserData()
            }
        }
    }
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"first_name, last_name , email, picture.type(large) , gender  " ])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(String(describing: error))")
            }
            else
            {
                print("fetched user: \(String(describing: result ))")
                //let userdef = UserDefaults.standard
                //let token =  userdef.value(forKey: "devicetoken")
                let result1 = result as AnyObject
                
                // print("tokennnn = \(String(describing: token))")
                
                
                let deviceid = UIDevice.current.identifierForVendor!.uuidString
                
                let fullName = "\(result1["first_name"] as! String) \(result1["last_name"] as! String)"
                
                let postString123 = "first_name=\(fullName)&email=\(result1["email"] as! String)&device_token=device123456token&device_type=IOS&device_id=\(deviceid)&from_where=1&social_id=\(result1["id"] as! String)&contact="
                // &contact=\(self.result1["contact"] as! String)
                
                
              
                
                print("postString123 \(postString123)" )
                self.fbjson(pstring: postString123)
            }
        })
    }
    
    func tokenRefreshNotification()
    {
        if let refreshedToken = Messaging.messaging().fcmToken{
            print("Sign up InstanceID token: \(refreshedToken)")
            // self.checkNotificationPermission()
        }
        
        
        
    }
    func fbjson( pstring : String)
    {
        ApiResponse.onResponsePostPhp(url: "Userajax/facebook_login", parms: pstring, completion: {(result , error) in
            
            if(error == ""){
                
                print("fb login rezsult = \(result)")
                let status = result["status"]as! Bool
                let message = result["message"]as! String
                
                if(status == true){
                    OperationQueue.main.addOperation {
                        
                        let datadict = result["data"] as! NSDictionary
                         let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
                        self.present(dashboard, animated: true, completion: nil)
                    }
                }
                else{
                    ApiResponse.alert(title: "Ooops!", message: message, controller: self)
                }
                
            }
            else{
                if(error == Constant.Status_Not_200){
                    ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                }
            }
        })
    }


    
}
