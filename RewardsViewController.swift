//
//  RewardsViewController.swift
//  FairField
//
//  Created by Prashant Shinde on 8/3/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import Alamofire

struct reward_struct {
    
    var reward_id : Int?
    var title : String?
    var imageUrl : String?
    var description : String?
    var can_comment : Int?
    var can_enter : Int?
    var show_title : Int?
}

class RewardsViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var reward_items : [reward_struct] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        getRewardsItems()
    }
    
    private func getRewardsItems() {
        
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            
            LoadingIndicatorView.show()
            let pstring = ""
            
            ApiResponse.onResponsePostRewardPhp(url: "userajax/active_rewards", parms: pstring, completion: {(result , error) in
                if(error == "")
                {
                    OperationQueue.main.addOperation {
                        LoadingIndicatorView.hide()
                        
                        self.reward_items = []
                        for temp in result {
                            
                            var dict = temp as! [String: Any]
                            let activation = Int(dict["active"] as! String)
                            if activation != 1 {
                                continue
                            }
                            var reward : reward_struct? = reward_struct()
                            reward?.reward_id = Int(dict["id"] as! String)
                            reward?.title = dict["title"] as? String
                            reward?.description = dict["description"] as? String
                            reward?.can_comment = Int(dict["can_comment"] as! String)
                            reward?.can_enter = Int(dict["can_enter"] as! String)
                            reward?.imageUrl = dict["image"] as? String
                            reward?.show_title = Int(dict["show_title"] as? String ?? "0")
                            self.reward_items.append(reward!)
                        }
                        
                        self.tableView.reloadData()
                        print(result.count)
                    }
                }
                else{
                    if(error == Constant.Status_Not_200){
                        ApiResponse.alert(title: "Oops", message: "Error", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Oops", message: "something went wrong", controller: self)
                    }
                }
            })
        } else {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }
    
    @IBAction func onDashboard(_ sender: Any)
    {
        let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
        
        self.present(dashboard, animated: true, completion: nil)
    }
    
}

extension RewardsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RewardCell") as! RewardCell
        cell.selectionStyle = .none
        
        if reward_items[indexPath.row].show_title == 1 {
            cell.titleLbl.isHidden = false
        } else {
            cell.titleLbl.isHidden = true
            
            cell.rewardImg.translatesAutoresizingMaskIntoConstraints = false
            cell.rewardImg.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor).isActive = true
            cell.rewardImg.topAnchor.constraint(equalTo: cell.contentView.topAnchor).isActive = true
            cell.rewardImg.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor).isActive = true
            cell.rewardImg.rightAnchor.constraint(equalTo: cell.contentView.rightAnchor).isActive = true
            
        }
        cell.titleLbl.text = reward_items[indexPath.row].title ?? ""
        
        let imageString : String = reward_items[indexPath.row].imageUrl!
        cell.rewardImg.sd_setImage(with: URL(string : "\(Constant.IMAGE_URL_NEW)rewards/\(String(describing: imageString))"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reward_items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "comment") as! RewardCommentVC
        dashboard.reward_id = reward_items[indexPath.row].reward_id!
        self.present(dashboard, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200.0
    }
}
