//
//  ChangePasswordVC.swift
//  FairField
//
//  Created by Prashant Shinde on 6/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController , UITextFieldDelegate{

    @IBOutlet var oldPassword: UITextField!
    @IBOutlet var newPassword: UITextField!
    @IBOutlet var confirmPassword: UITextField!
    
    var user_id : String = ""
    var userDef : UserDefaults!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"

        
        let textfield : [UITextField] = [oldPassword, newPassword, confirmPassword]
        
        let placeholder = ["Old Password", "New Password", "Confirm Password"]
        var i = 0
        
        for tf in textfield
        {
            tf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 5, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:placeholder[i],
                                                          attributes:[kCTForegroundColorAttributeName as NSAttributedStringKey: UIColor.white])
            i += 1
            
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        
        if(oldPassword.text == "" || newPassword.text == "" || confirmPassword.text == "")
        {
            
            ApiResponse.alert(title: "Empty Fields", message: "Please fill All the Fields", controller: self)
        }
        else if ((newPassword.text?.characters.count)! < 6) {
            ApiResponse.alert(title: "Ooops", message: "Password should be 6 character", controller: self)
        }
        else if(newPassword.text! != confirmPassword.text!)
        {
            ApiResponse.alert(title: "Incorrect Password", message: "New password and confirm password does not match", controller: self)
            
        }
        else {
            
            
            LoadingIndicatorView.show()
            
            let params = "user_id=\(user_id)&old_password=\(oldPassword.text!)&new_password=\(newPassword.text!)&confirm_password=\(confirmPassword.text!)"
            
            print("params = \(params)")
            
            ApiResponse.onResponsePostPhp(url: "Userajax/change_password", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    print("result = \(result)")
                    
                    if (status == true) {
                        
                        
                        OperationQueue.main.addOperation
                            {
                                LoadingIndicatorView.hide()
                                
                                ApiResponse.alert(title: "Done", message: message, controller: self)
                                self.oldPassword.text = ""
                                self.newPassword.text = ""
                                self.confirmPassword.text = ""
                                
                                                               
                        }
                        
                        
                    }
                    else {
                        ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
            
            
            
        }
        
        
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
