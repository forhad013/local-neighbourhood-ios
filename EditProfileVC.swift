//
//  EditProfileVC.swift
//  FairField
//
//  Created by Prashant Shinde on 6/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController , UITextFieldDelegate {

    @IBOutlet var fullName: UITextField!
    @IBOutlet var emailText: UITextField!
    @IBOutlet var updateProfile: KGHighLightedButton!
    
    var user_id : String = ""
    var userDef : UserDefaults!
    var newFrame : CGRect!
    var textfield : [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fullName.isUserInteractionEnabled = false
        emailText.isUserInteractionEnabled = false
        
        self.userDef = UserDefaults.standard
        let data1 = self.userDef.object(forKey: "userInfo") as! NSDictionary
        self.user_id = "\(data1["user_id"] as! String)"
        
        fullName.text = data1["first_name"] as? String
        emailText.text = data1["email"] as? String

        textfield = [fullName, emailText]
        let placeholder = ["Full Name", "Email"]
        var i = 0
        
        for tf in textfield
        {
            tf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 5, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:placeholder[i],
                                                          attributes:[kCTForegroundColorAttributeName as NSAttributedStringKey: UIColor.white])
            i += 1
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.newFrame = self.view.frame
        
        newFrame.origin.y = self.view.bounds.origin.y - 60
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                
                self.view.frame = self.newFrame
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        newFrame.origin.y = 0
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                self.view.frame = self.newFrame
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onUpdateProfile(_ sender: Any) {
        
        if (updateProfile.titleLabel?.text == "UPDATE") {
            
            for tf in textfield
            {
                tf.delegate = self
                tf.isUserInteractionEnabled = true
            }
            updateProfile.setTitle("SAVE" , for: UIControlState.normal)
            emailText.isUserInteractionEnabled = false
        }
        else {
            
            if(fullName.text == "") || (emailText.text == "") {
                ApiResponse.alert(title: "Empty", message: "Please fill all the fields", controller: self)
            }
            else {
                for tf in textfield
                {
                    tf.delegate = self
                    tf.isUserInteractionEnabled = false
                }
                updateProfile.setTitle("Edit Profile" , for: UIControlState.normal)
                self.getUpdate()
            }
        }
    }
    
    func getUpdate()
    {
        if  reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            LoadingIndicatorView.show()
            let params = "user_id=\(user_id)&first_name=\(fullName.text!)"
            
            ApiResponse.onResponsePostPhp(url: "Userajax/update_profile", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true) {
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            
                            let alert = UIAlertController(title: "Done", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                action in
                                
                                let userDef = UserDefaults.standard
                                let data = result["data"] as! NSDictionary
                                userDef.set(data, forKey: "userInfo")
                                userDef.synchronize()
                                self.viewDidLoad()
                                
                                let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
                                self.present(dashboard, animated: true, completion: nil)
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else
        {
            ApiResponse.alert(title: "No Internet Connection", message: "Please check your internet", controller: self)
        }
    }

    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
